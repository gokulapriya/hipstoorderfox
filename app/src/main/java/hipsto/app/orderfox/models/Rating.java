package hipsto.app.orderfox.models;

/**
 * Created by Gokulapriya on 7/20/2019.
 */

public class Rating {

    private String uId;
    private String uIdEncode;
    private String newsId;
    private Integer rating;

    public Rating(String uId, String uIdEncode, String newsId, Integer rating) {
        this.uId = uId;
        this.uIdEncode = uIdEncode;
        this.newsId = newsId;
        this.rating = rating;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getuIdEncode() {
        return uIdEncode;
    }

    public void setuIdEncode(String uIdEncode) {
        this.uIdEncode = uIdEncode;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
