package hipsto.app.orderfox.models;

import java.util.List;

/**
 * Created by Gokulapriya on 7/7/2019.
 */

public class NewsDetailResponse {
    private String topic_code;
    private List<String> moderation_details;
    private String data_video;
    private String data_content_pretty;
    private String mid;
    private String data_content_full;
    private Source source;
    private String data_title;
    private String data_teaser_clean;
    private String source_favicon;
    private SourceCreatedAt source_created_at;
    private String hipsto_own_views;
    private String topic_name;
    private String data_teaser;
    private String data_author;
    private String data_image_main;
    private String source_name;
    private int hipsto_own_rating;
    private String hipsto_own_ratings;
    private String hipsto_own_bookmark_id;
    private String data_image_preview;
    private Timestamp timestamp;
    private String source_lang;
    private String time_read;
    private String moderation_reason;
    private String data_published_parsed;
    private String data_content;
    private String data_published;
    private ModerationDate moderation_date;
    private String item_url;
    private String moderation_status;
    private Id id;
    private String source_code;
    private String words_count;

    public void setTopicCode(String topicCode) {
        this.topic_code = topicCode;
    }

    public String getTopicCode() {
        return topic_code;
    }

    public void setModerationDetails(List<String> moderationDetails) {
        this.moderation_details = moderationDetails;
    }

    public List<String> getModerationDetails() {
        return moderation_details;
    }

    public void setDataVideo(String dataVideo) {
        this.data_video = dataVideo;
    }

    public String getDataVideo() {
        return data_video;
    }

    public void setDataContentPretty(String dataContentPretty) {
        this.data_content_pretty = dataContentPretty;
    }

    public String getDataContentPretty() {
        return data_content_pretty;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getMid() {
        return mid;
    }

    public void setDataContentFull(String dataContentFull) {
        this.data_content_full = dataContentFull;
    }

    public String getDataContentFull() {
        return data_content_full;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public Source getSource() {
        return source;
    }

    public void setDataTitle(String dataTitle) {
        this.data_title = dataTitle;
    }

    public String getDataTitle() {
        return data_title;
    }

    public void setDataTeaserClean(String dataTeaserClean) {
        this.data_teaser_clean = dataTeaserClean;
    }

    public String getDataTeaserClean() {
        return data_teaser_clean;
    }

    public void setSourceFavicon(String sourceFavicon) {
        this.source_favicon = sourceFavicon;
    }

    public String getSourceFavicon() {
        return source_favicon;
    }

    public void setSourceCreatedAt(SourceCreatedAt sourceCreatedAt) {
        this.source_created_at = sourceCreatedAt;
    }

    public SourceCreatedAt getSourceCreatedAt() {
        return source_created_at;
    }

    public void setHipstoOwnViews(String hipstoOwnViews) {
        this.hipsto_own_views = hipstoOwnViews;
    }

    public String getHipstoOwnViews() {
        return hipsto_own_views;
    }

    public void setTopicName(String topicName) {
        this.topic_name = topicName;
    }

    public String getTopicName() {
        return topic_name;
    }

    public void setDataTeaser(String dataTeaser) {
        this.data_teaser = dataTeaser;
    }

    public String getDataTeaser() {
        return data_teaser;
    }

    public void setDataAuthor(String dataAuthor) {
        this.data_author = dataAuthor;
    }

    public String getDataAuthor() {
        return data_author;
    }

    public void setDataImageMain(String dataImageMain) {
        this.data_image_main = dataImageMain;
    }

    public String getDataImageMain() {
        return data_image_main;
    }

    public void setSourceName(String sourceName) {
        this.source_name = sourceName;
    }

    public String getSourceName() {
        return source_name;
    }

    public void setHipstoOwnRating(int hipstoOwnRating) {
        this.hipsto_own_rating = hipstoOwnRating;
    }

    public String getHipsto_own_bookmark_id() {
        return hipsto_own_bookmark_id;
    }

    public void setHipsto_own_bookmark_id(String hipsto_own_bookmark_id) {
        this.hipsto_own_bookmark_id = hipsto_own_bookmark_id;
    }
    public String getHipsto_own_ratings() {
        return hipsto_own_ratings;
    }

    public void setHipsto_own_ratings(String hipsto_own_ratings) {
        this.hipsto_own_ratings = hipsto_own_ratings;
    }
    public int getHipstoOwnRating() {
        return hipsto_own_rating;
    }

    public void setDataImagePreview(String dataImagePreview) {
        this.data_image_preview = dataImagePreview;
    }

    public String getDataImagePreview() {
        return data_image_preview;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setSourceLang(String sourceLang) {
        this.source_lang = sourceLang;
    }

    public String getSourceLang() {
        return source_lang;
    }

    public void setTimeRead(String timeRead) {
        this.time_read = timeRead;
    }

    public String getTimeRead() {
        return time_read;
    }

    public void setModerationReason(String moderationReason) {
        this.moderation_reason = moderationReason;
    }

    public String getModerationReason() {
        return moderation_reason;
    }

    public void setDataPublishedParsed(String dataPublishedParsed) {
        this.data_published_parsed = dataPublishedParsed;
    }

    public String getDataPublishedParsed() {
        return data_published_parsed;
    }

    public void setDataContent(String dataContent) {
        this.data_content = dataContent;
    }

    public String getDataContent() {
        return data_content;
    }

    public void setDataPublished(String dataPublished) {
        this.data_published = dataPublished;
    }

    public String getDataPublished() {
        return data_published;
    }

    public void setModerationDate(ModerationDate moderationDate) {
        this.moderation_date = moderationDate;
    }

    public ModerationDate getModerationDate() {
        return moderation_date;
    }

    public void setItemUrl(String itemUrl) {
        this.item_url = itemUrl;
    }

    public String getItemUrl() {
        return item_url;
    }

    public void setModerationStatus(String moderationStatus) {
        this.moderation_status = moderationStatus;
    }

    public String getModerationStatus() {
        return moderation_status;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Id getId() {
        return id;
    }

    public void setSourceCode(String sourceCode) {
        this.source_code = sourceCode;
    }

    public String getSourceCode() {
        return source_code;
    }

    public void setWordsCount(String wordsCount) {
        this.words_count = wordsCount;
    }

    public String getWordsCount() {
        return words_count;
    }

    public class Id {
        private String oid;

        public void setOid(String oid) {
            this.oid = oid;
        }

        public String getOid() {
            return oid;
        }
    }

    public class Date {
        private String $numberLong;

        public void setNumberLong(String numberLong) {
            this.$numberLong = numberLong;
        }

        public String getNumberLong() {
            return $numberLong;
        }
    }

    public class ModerationDate {
        private Date $date;

        public void setDate(Date date) {
            this.$date = date;
        }

        public Date getDate() {
            return $date;
        }
    }

    public class Source {
        private String domain;
        private String url;

        public void setDomain(String domain) {
            this.domain = domain;
        }

        public String getDomain() {
            return domain;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }
    }

    public class SourceCreatedAt {
        private Date date;

        public void setDate(Date date) {
            this.date = date;
        }

        public Date getDate() {
            return date;
        }
    }

    public class Timestamp {
        private Date $date;

        public void setDate(Date date) {
            this.$date = date;
        }

        public Date getDate() {
            return $date;
        }
    }

}