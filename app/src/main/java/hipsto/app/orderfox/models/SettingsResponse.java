package hipsto.app.orderfox.models;

import java.util.List;

/**
 * Created by Gokulapriya on 7/3/2019.
 */

public class SettingsResponse {
    private String topicCode;
    private String dataCategory;
    private String primaryColor;
    private boolean enableCrawler;
    private TimeUpdated timeUpdated;
    private List<String> data_lang;
    private Object topicInfluencers;
    private String dataCollection;
    private List<String> topicKeywords;
    private String apiKey;
    private String topicName;
    private String dataSubcategory;
    private TimeCreated timeCreated;
    private String secondaryColor;
    private Id id;
    private boolean enableModerator;
    private String appId;

    public void setTopicCode(String topicCode) {
        this.topicCode = topicCode;
    }

    public String getTopicCode() {
        return topicCode;
    }

    public void setDataCategory(String dataCategory) {
        this.dataCategory = dataCategory;
    }

    public String getDataCategory() {
        return dataCategory;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public void setEnableCrawler(boolean enableCrawler) {
        this.enableCrawler = enableCrawler;
    }

    public boolean isEnableCrawler() {
        return enableCrawler;
    }

    public void setTimeUpdated(TimeUpdated timeUpdated) {
        this.timeUpdated = timeUpdated;
    }

    public TimeUpdated getTimeUpdated() {
        return timeUpdated;
    }

    public void setDataLang(List<String> dataLang) {
        this.data_lang = dataLang;
    }

    public List<String> getDataLang() {
        return data_lang;
    }

    public void setTopicInfluencers(Object topicInfluencers) {
        this.topicInfluencers = topicInfluencers;
    }

    public Object getTopicInfluencers() {
        return topicInfluencers;
    }

    public void setDataCollection(String dataCollection) {
        this.dataCollection = dataCollection;
    }

    public String getDataCollection() {
        return dataCollection;
    }

    public void setTopicKeywords(List<String> topicKeywords) {
        this.topicKeywords = topicKeywords;
    }

    public List<String> getTopicKeywords() {
        return topicKeywords;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setDataSubcategory(String dataSubcategory) {
        this.dataSubcategory = dataSubcategory;
    }

    public String getDataSubcategory() {
        return dataSubcategory;
    }

    public void setTimeCreated(TimeCreated timeCreated) {
        this.timeCreated = timeCreated;
    }

    public TimeCreated getTimeCreated() {
        return timeCreated;
    }

    public void setSecondaryColor(String secondaryColor) {
        this.secondaryColor = secondaryColor;
    }

    public String getSecondaryColor() {
        return secondaryColor;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Id getId() {
        return id;
    }

    public void setEnableModerator(boolean enableModerator) {
        this.enableModerator = enableModerator;
    }

    public boolean isEnableModerator() {
        return enableModerator;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppId() {
        return appId;
    }

    public class Id {
        private String $oid;

        public void setOid(String oid) {
            this.$oid = oid;
        }

        public String getOid() {
            return $oid;
        }
    }

    public class TimeCreated {
        private Date date;

        public void setDate(Date date) {
            this.date = date;
        }

        public Date getDate() {
            return date;
        }

    }

    public class TimeUpdated {
        private Date date;

        public void setDate(Date date) {
            this.date = date;
        }

        public Date getDate() {
            return date;
        }
    }

    public class Date {
        private String numberLong;

        public void setNumberLong(String numberLong) {
            this.numberLong = numberLong;
        }

        public String getNumberLong() {
            return numberLong;
        }
    }
}