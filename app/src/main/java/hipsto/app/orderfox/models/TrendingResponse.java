package hipsto.app.orderfox.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Gokulapriya on 7/8/2019.
 */

public class TrendingResponse implements Serializable, Parcelable {
	private int accShares;
	private int viewsVideo;
	private String lastmeasuredTwitter;
	private int maxratePins;
	private int accScoreReddit;
	private int maxrateLikes;
	private int likesFirst;
	private int accVotesVideo;
	private int commentsReddit;
	private Object sharesFirst;
	private int upsReddit;
	private int id;
	private int maxrateDownsReddit;
	private int rateLikes;
	private int rateDownsReddit;
	private int rateViewsVideo;
	private int rateVotesVideo;
	private int accDownsReddit;
	private int accLikes;
	private String domain;
	private String hotnessTweets;
	private int rateScoreReddit;
	private String lastmeasured;
	private int favorite;
	private String hash;
	private int accViewsVideo;
	private Object firstmeasuredReddit;
	private String hotnessViewsVideo;
	private String hotnessPins;
	private int pins;
	private int commentsVideo;
	private Object commentsRedditFirst;
	private int shares;
	private int rateCommentsReddit;
	private String found;
	private int accComments;
	private String firstmeasuredTwitter;
	private int isTweet;
	private int accUpsReddit;
	private Object firstmeasuredVideo;
	private int maxrateUpsReddit;
	private Object votesVideoFirst;
	private String url;
	private Object upsRedditFirst;
	private int tweetsFirst;
	private Object lastmeasuredReddit;
	private int rateUpsReddit;
	private int maxrateCommentsReddit;
	private int maxrateComments;
	private Object lastmeasuredVideo;
	private int maxrateTweets;
	private int isGallery;
	private String hotnessVotesVideo;
	private String language;
	private String hotnessDownsReddit;
	private int tweets;
	private String firstmeasuredPintrest;
	private int ratePins;
	private int likes;
	private String image;
	private String hotnessLikes;
	private String author;
	private Object downsRedditFirst;
	private String hotnessCommentsReddit;
	private int maxrateScoreReddit;
	private int rateShares;
	private Object published;
	private int accCommentsReddit;
	private int isVideo;
	private int rateComments;
	private Object scoreRedditFirst;
	private int scoreReddit;
	private int downsReddit;
	private Object maxrateShares;
	private int rateTweets;
	private List<SourcesItem> sources;
	private int accPins;
	private String firstmeasured;
	private String description;
	private String title;
	private String hotnessShares;
	private String hotnessComments;
	private int votesVideo;
	private String hotnessScoreReddit;
	private String hotnessCommentsVideo;
	private int isImage;
	private int accTweets;
	private Object maxrateCommentsVideo;
	private int comments;
	private int pinsFirst;
	private int accCommentsVideo;
	private int isFacebook;
	private String hotnessUpsReddit;
	private Object maxrateVotesVideo;
	private Object viewsVideoFirst;
	private String lastmeasuredPintrest;
	private Object maxrateViewsVideo;
	private int commentsFirst;
	private Object commentsVideoFirst;
	private int rateCommentsVideo;

	protected TrendingResponse(Parcel in) {
		accShares = in.readInt();
		viewsVideo = in.readInt();
		lastmeasuredTwitter = in.readString();
		maxratePins = in.readInt();
		accScoreReddit = in.readInt();
		maxrateLikes = in.readInt();
		likesFirst = in.readInt();
		accVotesVideo = in.readInt();
		commentsReddit = in.readInt();
		upsReddit = in.readInt();
		id = in.readInt();
		maxrateDownsReddit = in.readInt();
		rateLikes = in.readInt();
		rateDownsReddit = in.readInt();
		rateViewsVideo = in.readInt();
		rateVotesVideo = in.readInt();
		accDownsReddit = in.readInt();
		accLikes = in.readInt();
		domain = in.readString();
		hotnessTweets = in.readString();
		rateScoreReddit = in.readInt();
		lastmeasured = in.readString();
		favorite = in.readInt();
		hash = in.readString();
		accViewsVideo = in.readInt();
		hotnessViewsVideo = in.readString();
		hotnessPins = in.readString();
		pins = in.readInt();
		commentsVideo = in.readInt();
		shares = in.readInt();
		rateCommentsReddit = in.readInt();
		found = in.readString();
		accComments = in.readInt();
		firstmeasuredTwitter = in.readString();
		isTweet = in.readInt();
		accUpsReddit = in.readInt();
		maxrateUpsReddit = in.readInt();
		url = in.readString();
		tweetsFirst = in.readInt();
		rateUpsReddit = in.readInt();
		maxrateCommentsReddit = in.readInt();
		maxrateComments = in.readInt();
		maxrateTweets = in.readInt();
		isGallery = in.readInt();
		hotnessVotesVideo = in.readString();
		language = in.readString();
		hotnessDownsReddit = in.readString();
		tweets = in.readInt();
		firstmeasuredPintrest = in.readString();
		ratePins = in.readInt();
		likes = in.readInt();
		image = in.readString();
		hotnessLikes = in.readString();
		author = in.readString();
		hotnessCommentsReddit = in.readString();
		maxrateScoreReddit = in.readInt();
		rateShares = in.readInt();
		accCommentsReddit = in.readInt();
		isVideo = in.readInt();
		rateComments = in.readInt();
		scoreReddit = in.readInt();
		downsReddit = in.readInt();
		rateTweets = in.readInt();
		accPins = in.readInt();
		firstmeasured = in.readString();
		description = in.readString();
		title = in.readString();
		hotnessShares = in.readString();
		hotnessComments = in.readString();
		votesVideo = in.readInt();
		hotnessScoreReddit = in.readString();
		hotnessCommentsVideo = in.readString();
		isImage = in.readInt();
		accTweets = in.readInt();
		comments = in.readInt();
		pinsFirst = in.readInt();
		accCommentsVideo = in.readInt();
		isFacebook = in.readInt();
		hotnessUpsReddit = in.readString();
		lastmeasuredPintrest = in.readString();
		commentsFirst = in.readInt();
		rateCommentsVideo = in.readInt();
	}

	public static final Creator<TrendingResponse> CREATOR = new Creator<TrendingResponse>() {
		@Override
		public TrendingResponse createFromParcel(Parcel in) {
			return new TrendingResponse(in);
		}

		@Override
		public TrendingResponse[] newArray(int size) {
			return new TrendingResponse[size];
		}
	};

	public void setAccShares(int accShares){
		this.accShares = accShares;
	}

	public int getAccShares(){
		return accShares;
	}

	public void setViewsVideo(int viewsVideo){
		this.viewsVideo = viewsVideo;
	}

	public int getViewsVideo(){
		return viewsVideo;
	}

	public void setLastmeasuredTwitter(String lastmeasuredTwitter){
		this.lastmeasuredTwitter = lastmeasuredTwitter;
	}

	public String getLastmeasuredTwitter(){
		return lastmeasuredTwitter;
	}

	public void setMaxratePins(int maxratePins){
		this.maxratePins = maxratePins;
	}

	public int getMaxratePins(){
		return maxratePins;
	}

	public void setAccScoreReddit(int accScoreReddit){
		this.accScoreReddit = accScoreReddit;
	}

	public int getAccScoreReddit(){
		return accScoreReddit;
	}

	public void setMaxrateLikes(int maxrateLikes){
		this.maxrateLikes = maxrateLikes;
	}

	public int getMaxrateLikes(){
		return maxrateLikes;
	}

	public void setLikesFirst(int likesFirst){
		this.likesFirst = likesFirst;
	}

	public int getLikesFirst(){
		return likesFirst;
	}

	public void setAccVotesVideo(int accVotesVideo){
		this.accVotesVideo = accVotesVideo;
	}

	public int getAccVotesVideo(){
		return accVotesVideo;
	}

	public void setCommentsReddit(int commentsReddit){
		this.commentsReddit = commentsReddit;
	}

	public int getCommentsReddit(){
		return commentsReddit;
	}

	public void setSharesFirst(Object sharesFirst){
		this.sharesFirst = sharesFirst;
	}

	public Object getSharesFirst(){
		return sharesFirst;
	}

	public void setUpsReddit(int upsReddit){
		this.upsReddit = upsReddit;
	}

	public int getUpsReddit(){
		return upsReddit;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setMaxrateDownsReddit(int maxrateDownsReddit){
		this.maxrateDownsReddit = maxrateDownsReddit;
	}

	public int getMaxrateDownsReddit(){
		return maxrateDownsReddit;
	}

	public void setRateLikes(int rateLikes){
		this.rateLikes = rateLikes;
	}

	public int getRateLikes(){
		return rateLikes;
	}

	public void setRateDownsReddit(int rateDownsReddit){
		this.rateDownsReddit = rateDownsReddit;
	}

	public int getRateDownsReddit(){
		return rateDownsReddit;
	}

	public void setRateViewsVideo(int rateViewsVideo){
		this.rateViewsVideo = rateViewsVideo;
	}

	public int getRateViewsVideo(){
		return rateViewsVideo;
	}

	public void setRateVotesVideo(int rateVotesVideo){
		this.rateVotesVideo = rateVotesVideo;
	}

	public int getRateVotesVideo(){
		return rateVotesVideo;
	}

	public void setAccDownsReddit(int accDownsReddit){
		this.accDownsReddit = accDownsReddit;
	}

	public int getAccDownsReddit(){
		return accDownsReddit;
	}

	public void setAccLikes(int accLikes){
		this.accLikes = accLikes;
	}

	public int getAccLikes(){
		return accLikes;
	}

	public void setDomain(String domain){
		this.domain = domain;
	}

	public String getDomain(){
		return domain;
	}

	public void setHotnessTweets(String hotnessTweets){
		this.hotnessTweets = hotnessTweets;
	}

	public String getHotnessTweets(){
		return hotnessTweets;
	}

	public void setRateScoreReddit(int rateScoreReddit){
		this.rateScoreReddit = rateScoreReddit;
	}

	public int getRateScoreReddit(){
		return rateScoreReddit;
	}

	public void setLastmeasured(String lastmeasured){
		this.lastmeasured = lastmeasured;
	}

	public String getLastmeasured(){
		return lastmeasured;
	}

	public void setFavorite(int favorite){
		this.favorite = favorite;
	}

	public int getFavorite(){
		return favorite;
	}

	public void setHash(String hash){
		this.hash = hash;
	}

	public String getHash(){
		return hash;
	}

	public void setAccViewsVideo(int accViewsVideo){
		this.accViewsVideo = accViewsVideo;
	}

	public int getAccViewsVideo(){
		return accViewsVideo;
	}

	public void setFirstmeasuredReddit(Object firstmeasuredReddit){
		this.firstmeasuredReddit = firstmeasuredReddit;
	}

	public Object getFirstmeasuredReddit(){
		return firstmeasuredReddit;
	}

	public void setHotnessViewsVideo(String hotnessViewsVideo){
		this.hotnessViewsVideo = hotnessViewsVideo;
	}

	public String getHotnessViewsVideo(){
		return hotnessViewsVideo;
	}

	public void setHotnessPins(String hotnessPins){
		this.hotnessPins = hotnessPins;
	}

	public String getHotnessPins(){
		return hotnessPins;
	}

	public void setPins(int pins){
		this.pins = pins;
	}

	public int getPins(){
		return pins;
	}

	public void setCommentsVideo(int commentsVideo){
		this.commentsVideo = commentsVideo;
	}

	public int getCommentsVideo(){
		return commentsVideo;
	}

	public void setCommentsRedditFirst(Object commentsRedditFirst){
		this.commentsRedditFirst = commentsRedditFirst;
	}

	public Object getCommentsRedditFirst(){
		return commentsRedditFirst;
	}

	public void setShares(int shares){
		this.shares = shares;
	}

	public int getShares(){
		return shares;
	}

	public void setRateCommentsReddit(int rateCommentsReddit){
		this.rateCommentsReddit = rateCommentsReddit;
	}

	public int getRateCommentsReddit(){
		return rateCommentsReddit;
	}

	public void setFound(String found){
		this.found = found;
	}

	public String getFound(){
		return found;
	}

	public void setAccComments(int accComments){
		this.accComments = accComments;
	}

	public int getAccComments(){
		return accComments;
	}

	public void setFirstmeasuredTwitter(String firstmeasuredTwitter){
		this.firstmeasuredTwitter = firstmeasuredTwitter;
	}

	public String getFirstmeasuredTwitter(){
		return firstmeasuredTwitter;
	}

	public void setIsTweet(int isTweet){
		this.isTweet = isTweet;
	}

	public int getIsTweet(){
		return isTweet;
	}

	public void setAccUpsReddit(int accUpsReddit){
		this.accUpsReddit = accUpsReddit;
	}

	public int getAccUpsReddit(){
		return accUpsReddit;
	}

	public void setFirstmeasuredVideo(Object firstmeasuredVideo){
		this.firstmeasuredVideo = firstmeasuredVideo;
	}

	public Object getFirstmeasuredVideo(){
		return firstmeasuredVideo;
	}

	public void setMaxrateUpsReddit(int maxrateUpsReddit){
		this.maxrateUpsReddit = maxrateUpsReddit;
	}

	public int getMaxrateUpsReddit(){
		return maxrateUpsReddit;
	}

	public void setVotesVideoFirst(Object votesVideoFirst){
		this.votesVideoFirst = votesVideoFirst;
	}

	public Object getVotesVideoFirst(){
		return votesVideoFirst;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setUpsRedditFirst(Object upsRedditFirst){
		this.upsRedditFirst = upsRedditFirst;
	}

	public Object getUpsRedditFirst(){
		return upsRedditFirst;
	}

	public void setTweetsFirst(int tweetsFirst){
		this.tweetsFirst = tweetsFirst;
	}

	public int getTweetsFirst(){
		return tweetsFirst;
	}

	public void setLastmeasuredReddit(Object lastmeasuredReddit){
		this.lastmeasuredReddit = lastmeasuredReddit;
	}

	public Object getLastmeasuredReddit(){
		return lastmeasuredReddit;
	}

	public void setRateUpsReddit(int rateUpsReddit){
		this.rateUpsReddit = rateUpsReddit;
	}

	public int getRateUpsReddit(){
		return rateUpsReddit;
	}

	public void setMaxrateCommentsReddit(int maxrateCommentsReddit){
		this.maxrateCommentsReddit = maxrateCommentsReddit;
	}

	public int getMaxrateCommentsReddit(){
		return maxrateCommentsReddit;
	}

	public void setMaxrateComments(int maxrateComments){
		this.maxrateComments = maxrateComments;
	}

	public int getMaxrateComments(){
		return maxrateComments;
	}

	public void setLastmeasuredVideo(Object lastmeasuredVideo){
		this.lastmeasuredVideo = lastmeasuredVideo;
	}

	public Object getLastmeasuredVideo(){
		return lastmeasuredVideo;
	}

	public void setMaxrateTweets(int maxrateTweets){
		this.maxrateTweets = maxrateTweets;
	}

	public int getMaxrateTweets(){
		return maxrateTweets;
	}

	public void setIsGallery(int isGallery){
		this.isGallery = isGallery;
	}

	public int getIsGallery(){
		return isGallery;
	}

	public void setHotnessVotesVideo(String hotnessVotesVideo){
		this.hotnessVotesVideo = hotnessVotesVideo;
	}

	public String getHotnessVotesVideo(){
		return hotnessVotesVideo;
	}

	public void setLanguage(String language){
		this.language = language;
	}

	public String getLanguage(){
		return language;
	}

	public void setHotnessDownsReddit(String hotnessDownsReddit){
		this.hotnessDownsReddit = hotnessDownsReddit;
	}

	public String getHotnessDownsReddit(){
		return hotnessDownsReddit;
	}

	public void setTweets(int tweets){
		this.tweets = tweets;
	}

	public int getTweets(){
		return tweets;
	}

	public void setFirstmeasuredPintrest(String firstmeasuredPintrest){
		this.firstmeasuredPintrest = firstmeasuredPintrest;
	}

	public String getFirstmeasuredPintrest(){
		return firstmeasuredPintrest;
	}

	public void setRatePins(int ratePins){
		this.ratePins = ratePins;
	}

	public int getRatePins(){
		return ratePins;
	}

	public void setLikes(int likes){
		this.likes = likes;
	}

	public int getLikes(){
		return likes;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setHotnessLikes(String hotnessLikes){
		this.hotnessLikes = hotnessLikes;
	}

	public String getHotnessLikes(){
		return hotnessLikes;
	}

	public void setAuthor(String author){
		this.author = author;
	}

	public String getAuthor(){
		return author;
	}

	public void setDownsRedditFirst(Object downsRedditFirst){
		this.downsRedditFirst = downsRedditFirst;
	}

	public Object getDownsRedditFirst(){
		return downsRedditFirst;
	}

	public void setHotnessCommentsReddit(String hotnessCommentsReddit){
		this.hotnessCommentsReddit = hotnessCommentsReddit;
	}

	public String getHotnessCommentsReddit(){
		return hotnessCommentsReddit;
	}

	public void setMaxrateScoreReddit(int maxrateScoreReddit){
		this.maxrateScoreReddit = maxrateScoreReddit;
	}

	public int getMaxrateScoreReddit(){
		return maxrateScoreReddit;
	}

	public void setRateShares(int rateShares){
		this.rateShares = rateShares;
	}

	public int getRateShares(){
		return rateShares;
	}

	public void setPublished(Object published){
		this.published = published;
	}

	public Object getPublished(){
		return published;
	}

	public void setAccCommentsReddit(int accCommentsReddit){
		this.accCommentsReddit = accCommentsReddit;
	}

	public int getAccCommentsReddit(){
		return accCommentsReddit;
	}

	public void setIsVideo(int isVideo){
		this.isVideo = isVideo;
	}

	public int getIsVideo(){
		return isVideo;
	}

	public void setRateComments(int rateComments){
		this.rateComments = rateComments;
	}

	public int getRateComments(){
		return rateComments;
	}

	public void setScoreRedditFirst(Object scoreRedditFirst){
		this.scoreRedditFirst = scoreRedditFirst;
	}

	public Object getScoreRedditFirst(){
		return scoreRedditFirst;
	}

	public void setScoreReddit(int scoreReddit){
		this.scoreReddit = scoreReddit;
	}

	public int getScoreReddit(){
		return scoreReddit;
	}

	public void setDownsReddit(int downsReddit){
		this.downsReddit = downsReddit;
	}

	public int getDownsReddit(){
		return downsReddit;
	}

	public void setMaxrateShares(Object maxrateShares){
		this.maxrateShares = maxrateShares;
	}

	public Object getMaxrateShares(){
		return maxrateShares;
	}

	public void setRateTweets(int rateTweets){
		this.rateTweets = rateTweets;
	}

	public int getRateTweets(){
		return rateTweets;
	}

	public void setSources(List<SourcesItem> sources){
		this.sources = sources;
	}

	public List<SourcesItem> getSources(){
		return sources;
	}

	public void setAccPins(int accPins){
		this.accPins = accPins;
	}

	public int getAccPins(){
		return accPins;
	}

	public void setFirstmeasured(String firstmeasured){
		this.firstmeasured = firstmeasured;
	}

	public String getFirstmeasured(){
		return firstmeasured;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setHotnessShares(String hotnessShares){
		this.hotnessShares = hotnessShares;
	}

	public String getHotnessShares(){
		return hotnessShares;
	}

	public void setHotnessComments(String hotnessComments){
		this.hotnessComments = hotnessComments;
	}

	public String getHotnessComments(){
		return hotnessComments;
	}

	public void setVotesVideo(int votesVideo){
		this.votesVideo = votesVideo;
	}

	public int getVotesVideo(){
		return votesVideo;
	}

	public void setHotnessScoreReddit(String hotnessScoreReddit){
		this.hotnessScoreReddit = hotnessScoreReddit;
	}

	public String getHotnessScoreReddit(){
		return hotnessScoreReddit;
	}

	public void setHotnessCommentsVideo(String hotnessCommentsVideo){
		this.hotnessCommentsVideo = hotnessCommentsVideo;
	}

	public String getHotnessCommentsVideo(){
		return hotnessCommentsVideo;
	}

	public void setIsImage(int isImage){
		this.isImage = isImage;
	}

	public int getIsImage(){
		return isImage;
	}

	public void setAccTweets(int accTweets){
		this.accTweets = accTweets;
	}

	public int getAccTweets(){
		return accTweets;
	}

	public void setMaxrateCommentsVideo(Object maxrateCommentsVideo){
		this.maxrateCommentsVideo = maxrateCommentsVideo;
	}

	public Object getMaxrateCommentsVideo(){
		return maxrateCommentsVideo;
	}

	public void setComments(int comments){
		this.comments = comments;
	}

	public int getComments(){
		return comments;
	}

	public void setPinsFirst(int pinsFirst){
		this.pinsFirst = pinsFirst;
	}

	public int getPinsFirst(){
		return pinsFirst;
	}

	public void setAccCommentsVideo(int accCommentsVideo){
		this.accCommentsVideo = accCommentsVideo;
	}

	public int getAccCommentsVideo(){
		return accCommentsVideo;
	}

	public void setIsFacebook(int isFacebook){
		this.isFacebook = isFacebook;
	}

	public int getIsFacebook(){
		return isFacebook;
	}

	public void setHotnessUpsReddit(String hotnessUpsReddit){
		this.hotnessUpsReddit = hotnessUpsReddit;
	}

	public String getHotnessUpsReddit(){
		return hotnessUpsReddit;
	}

	public void setMaxrateVotesVideo(Object maxrateVotesVideo){
		this.maxrateVotesVideo = maxrateVotesVideo;
	}

	public Object getMaxrateVotesVideo(){
		return maxrateVotesVideo;
	}

	public void setViewsVideoFirst(Object viewsVideoFirst){
		this.viewsVideoFirst = viewsVideoFirst;
	}

	public Object getViewsVideoFirst(){
		return viewsVideoFirst;
	}

	public void setLastmeasuredPintrest(String lastmeasuredPintrest){
		this.lastmeasuredPintrest = lastmeasuredPintrest;
	}

	public String getLastmeasuredPintrest(){
		return lastmeasuredPintrest;
	}

	public void setMaxrateViewsVideo(Object maxrateViewsVideo){
		this.maxrateViewsVideo = maxrateViewsVideo;
	}

	public Object getMaxrateViewsVideo(){
		return maxrateViewsVideo;
	}

	public void setCommentsFirst(int commentsFirst){
		this.commentsFirst = commentsFirst;
	}

	public int getCommentsFirst(){
		return commentsFirst;
	}

	public void setCommentsVideoFirst(Object commentsVideoFirst){
		this.commentsVideoFirst = commentsVideoFirst;
	}

	public Object getCommentsVideoFirst(){
		return commentsVideoFirst;
	}

	public void setRateCommentsVideo(int rateCommentsVideo){
		this.rateCommentsVideo = rateCommentsVideo;
	}

	public int getRateCommentsVideo(){
		return rateCommentsVideo;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(accShares);
		dest.writeInt(viewsVideo);
		dest.writeString(lastmeasuredTwitter);
		dest.writeInt(maxratePins);
		dest.writeInt(accScoreReddit);
		dest.writeInt(maxrateLikes);
		dest.writeInt(likesFirst);
		dest.writeInt(accVotesVideo);
		dest.writeInt(commentsReddit);
		dest.writeInt(upsReddit);
		dest.writeInt(id);
		dest.writeInt(maxrateDownsReddit);
		dest.writeInt(rateLikes);
		dest.writeInt(rateDownsReddit);
		dest.writeInt(rateViewsVideo);
		dest.writeInt(rateVotesVideo);
		dest.writeInt(accDownsReddit);
		dest.writeInt(accLikes);
		dest.writeString(domain);
		dest.writeString(hotnessTweets);
		dest.writeInt(rateScoreReddit);
		dest.writeString(lastmeasured);
		dest.writeInt(favorite);
		dest.writeString(hash);
		dest.writeInt(accViewsVideo);
		dest.writeString(hotnessViewsVideo);
		dest.writeString(hotnessPins);
		dest.writeInt(pins);
		dest.writeInt(commentsVideo);
		dest.writeInt(shares);
		dest.writeInt(rateCommentsReddit);
		dest.writeString(found);
		dest.writeInt(accComments);
		dest.writeString(firstmeasuredTwitter);
		dest.writeInt(isTweet);
		dest.writeInt(accUpsReddit);
		dest.writeInt(maxrateUpsReddit);
		dest.writeString(url);
		dest.writeInt(tweetsFirst);
		dest.writeInt(rateUpsReddit);
		dest.writeInt(maxrateCommentsReddit);
		dest.writeInt(maxrateComments);
		dest.writeInt(maxrateTweets);
		dest.writeInt(isGallery);
		dest.writeString(hotnessVotesVideo);
		dest.writeString(language);
		dest.writeString(hotnessDownsReddit);
		dest.writeInt(tweets);
		dest.writeString(firstmeasuredPintrest);
		dest.writeInt(ratePins);
		dest.writeInt(likes);
		dest.writeString(image);
		dest.writeString(hotnessLikes);
		dest.writeString(author);
		dest.writeString(hotnessCommentsReddit);
		dest.writeInt(maxrateScoreReddit);
		dest.writeInt(rateShares);
		dest.writeInt(accCommentsReddit);
		dest.writeInt(isVideo);
		dest.writeInt(rateComments);
		dest.writeInt(scoreReddit);
		dest.writeInt(downsReddit);
		dest.writeInt(rateTweets);
		dest.writeInt(accPins);
		dest.writeString(firstmeasured);
		dest.writeString(description);
		dest.writeString(title);
		dest.writeString(hotnessShares);
		dest.writeString(hotnessComments);
		dest.writeInt(votesVideo);
		dest.writeString(hotnessScoreReddit);
		dest.writeString(hotnessCommentsVideo);
		dest.writeInt(isImage);
		dest.writeInt(accTweets);
		dest.writeInt(comments);
		dest.writeInt(pinsFirst);
		dest.writeInt(accCommentsVideo);
		dest.writeInt(isFacebook);
		dest.writeString(hotnessUpsReddit);
		dest.writeString(lastmeasuredPintrest);
		dest.writeInt(commentsFirst);
		dest.writeInt(rateCommentsVideo);
	}


	public class SourcesItem{
		private String name;
		private String url;

		public void setName(String name){
			this.name = name;
		}

		public String getName(){
			return name;
		}

		public void setUrl(String url){
			this.url = url;
		}

		public String getUrl(){
			return url;
		}
	}

}