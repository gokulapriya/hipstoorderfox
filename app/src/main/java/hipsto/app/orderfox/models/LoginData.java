package hipsto.app.orderfox.models;

/**
 * Created by Gokulapriya on 9/16/2019.
 */

public class LoginData {

    private Integer login_id;
    private String pref_token;
    private String pref_expry;
    private Integer islogin;
    private String cookies;
    private String user_id;
    private String user_id_encode;


    public LoginData(Integer login_id, String pref_token, String pref_expry, Integer islogin, String cookies, String user_id, String user_id_encode) {
        this.login_id = login_id;
        this.pref_token = pref_token;
        this.pref_expry = pref_expry;
        this.islogin = islogin;
        this.cookies = cookies;
        this.user_id = user_id;
        this.user_id_encode = user_id_encode;
    }

    public Integer getLogin_id() {
        return login_id;
    }

    public void setLogin_id(Integer login_id) {
        this.login_id = login_id;
    }

    public String getPref_token() {
        return pref_token;
    }

    public void setPref_token(String pref_token) {
        this.pref_token = pref_token;
    }

    public String getPref_expry() {
        return pref_expry;
    }

    public void setPref_expry(String pref_expry) {
        this.pref_expry = pref_expry;
    }

    public Integer getIslogin() {
        return islogin;
    }

    public void setIslogin(Integer islogin) {
        this.islogin = islogin;
    }

    public String getCookies() {
        return cookies;
    }

    public void setCookies(String cookies) {
        this.cookies = cookies;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id_encode() {
        return user_id_encode;
    }

    public void setUser_id_encode(String user_id_encode) {
        this.user_id_encode = user_id_encode;
    }
}
