package hipsto.app.orderfox.models;

/**
 * Created by Gokulapriya on 7/7/2019.
 */

public class Bookmark {

    private String topicCode;
    private String mid;
    private String data_image_main;
    private String item_url;
    private String data_title;
    private String dataContent;
    private String data_content_full;
    private String domain;
    private String url;
    private String data_published;
    private String source_name;
    private String source_lang;
    private Integer hipsto_own_rating;
    private Integer hipsto_own_views;
    private String hipsto_own_bookmark_id;
    private String time_read;

    public Bookmark(String topicCode, String mid, String data_image_main, String item_url, String data_title, String dataContent, String data_content_full, String domain, String url, String data_published, String source_name, String source_lang, Integer hipsto_own_rating, Integer hipsto_own_views, String hipsto_own_bookmark_id, String time_read) {
        this.topicCode = topicCode;
        this.mid = mid;
        this.data_image_main = data_image_main;
        this.item_url = item_url;
        this.data_title = data_title;
        this.dataContent = dataContent;
        this.data_content_full = data_content_full;
        this.domain = domain;
        this.url = url;
        this.data_published = data_published;
        this.source_name = source_name;
        this.source_lang = source_lang;
        this.hipsto_own_rating = hipsto_own_rating;
        this.hipsto_own_views = hipsto_own_views;
        this.hipsto_own_bookmark_id = hipsto_own_bookmark_id;
        this.time_read = time_read;
    }


    public String getTopicCode() {
        return topicCode;
    }

    public void setTopicCode(String topicCode) {
        this.topicCode = topicCode;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getData_image_main() {
        return data_image_main;
    }

    public void setData_image_main(String data_image_main) {
        this.data_image_main = data_image_main;
    }

    public String getItem_url() {
        return item_url;
    }

    public void setItem_url(String item_url) {
        this.item_url = item_url;
    }

    public String getData_title() {
        return data_title;
    }

    public void setData_title(String data_title) {
        this.data_title = data_title;
    }

    public String getDataContent() {
        return dataContent;
    }

    public void setDataContent(String dataContent) {
        this.dataContent = dataContent;
    }

    public String getData_content_full() {
        return data_content_full;
    }

    public void setData_content_full(String data_content_full) {
        this.data_content_full = data_content_full;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getData_published() {
        return data_published;
    }

    public void setData_published(String data_published) {
        this.data_published = data_published;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public String getSource_lang() {
        return source_lang;
    }

    public void setSource_lang(String source_lang) {
        this.source_lang = source_lang;
    }

    public Integer getHipsto_own_rating() {
        return hipsto_own_rating;
    }

    public void setHipsto_own_rating(Integer hipsto_own_rating) {
        this.hipsto_own_rating = hipsto_own_rating;
    }

    public Integer getHipsto_own_views() {
        return hipsto_own_views;
    }

    public void setHipsto_own_views(Integer hipsto_own_views) {
        this.hipsto_own_views = hipsto_own_views;
    }

    public String getHipsto_own_bookmark_id() {
        return hipsto_own_bookmark_id;
    }

    public void setHipsto_own_bookmark_id(String hipsto_own_bookmark_id) {
        this.hipsto_own_bookmark_id = hipsto_own_bookmark_id;
    }

    public String getTime_read() {
        return time_read;
    }

    public void setTime_read(String time_read) {
        this.time_read = time_read;
    }
}
