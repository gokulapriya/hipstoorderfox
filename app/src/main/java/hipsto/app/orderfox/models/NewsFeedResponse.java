package hipsto.app.orderfox.models;

import java.util.List;

/**
 * Created by Gokulapriya on 7/18/2019.
 */

public class NewsFeedResponse{
	private boolean success;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private String message;
	private List<String> identifiers;
	private int totalCount;
	private int limit;
	private int page;
	private int countPages;
	private List<ItemsItem> items;

	public void setIdentifiers(List<String> identifiers){
		this.identifiers = identifiers;
	}

	public List<String> getIdentifiers(){
		return identifiers;
	}

	public void setTotalCount(int totalCount){
		this.totalCount = totalCount;
	}

	public int getTotalCount(){
		return totalCount;
	}

	public void setLimit(int limit){
		this.limit = limit;
	}

	public int getLimit(){
		return limit;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setCountPages(int countPages){
		this.countPages = countPages;
	}

	public int getCountPages(){
		return countPages;
	}

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

	public List<ItemsItem> getItems(){
		return items;
	}

	public class Id{
		private String oid;

		public void setOid(String oid){
			this.oid = oid;
		}

		public String getOid(){
			return oid;
		}
	}
	public class Date{
		private String numberLong;

		public void setNumberLong(String numberLong){
			this.numberLong = numberLong;
		}

		public String getNumberLong(){
			return numberLong;
		}
	}
	public class ItemsItem {
		private String topicCode;
		private List<String> moderationDetails;
		private String dataVideo;
		private String dataContentPretty;
		private String mid;
		private String data_content_full;
		private Source source;
		private String data_title;
		private String dataTeaserClean;
		private String sourceFavicon;
		private SourceCreatedAt sourceCreatedAt;
		private int hipstoOwnViews;
		private String topicName;
		private String dataTeaser;
		private String dataAuthor;
		private String data_image_main;
		private String source_name;
		private int hipsto_own_rating;
		private String hipsto_own_bookmark_id;

		public String getHipsto_own_reports() {
			return hipsto_own_reports;
		}

		public void setHipsto_own_reports(String hipsto_own_reports) {
			this.hipsto_own_reports = hipsto_own_reports;
		}

		private String hipsto_own_reports;
		private String dataImagePreview;
		private Timestamp timestamp;
		private String sourceLang;
		private String time_read;
		private int moderationReason;
		private String dataPublishedParsed;
		private String dataContent;
		private String data_published;
		private ModerationDate moderationDate;
		private String itemUrl;
		private int moderationStatus;
		private Id id;
		private String sourceCode;
		private int wordsCount;
		private String hipstoOwnImage;

		public void setTopicCode(String topicCode) {
			this.topicCode = topicCode;
		}

		public String getTopicCode() {
			return topicCode;
		}

		public void setModerationDetails(List<String> moderationDetails) {
			this.moderationDetails = moderationDetails;
		}

		public List<String> getModerationDetails() {
			return moderationDetails;
		}

		public void setDataVideo(String dataVideo) {
			this.dataVideo = dataVideo;
		}

		public String getDataVideo() {
			return dataVideo;
		}

		public void setDataContentPretty(String dataContentPretty) {
			this.dataContentPretty = dataContentPretty;
		}

		public String getDataContentPretty() {
			return dataContentPretty;
		}

		public void setMid(String mid) {
			this.mid = mid;
		}

		public String getMid() {
			return mid;
		}

		public void setDataContentFull(String dataContentFull) {
			this.data_content_full = dataContentFull;
		}

		public String getDataContentFull() {
			return data_content_full;
		}

		public void setSource(Source source) {
			this.source = source;
		}

		public Source getSource() {
			return source;
		}

		public void setDataTitle(String dataTitle) {
			this.data_title = dataTitle;
		}

		public String getDataTitle() {
			return data_title;
		}

		public void setDataTeaserClean(String dataTeaserClean) {
			this.dataTeaserClean = dataTeaserClean;
		}

		public String getDataTeaserClean() {
			return dataTeaserClean;
		}

		public void setSourceFavicon(String sourceFavicon) {
			this.sourceFavicon = sourceFavicon;
		}

		public String getSourceFavicon() {
			return sourceFavicon;
		}

		public void setSourceCreatedAt(SourceCreatedAt sourceCreatedAt) {
			this.sourceCreatedAt = sourceCreatedAt;
		}

		public SourceCreatedAt getSourceCreatedAt() {
			return sourceCreatedAt;
		}

		public void setHipstoOwnViews(int hipstoOwnViews) {
			this.hipstoOwnViews = hipstoOwnViews;
		}

		public int getHipstoOwnViews() {
			return hipstoOwnViews;
		}

		public void setTopicName(String topicName) {
			this.topicName = topicName;
		}

		public String getTopicName() {
			return topicName;
		}

		public void setDataTeaser(String dataTeaser) {
			this.dataTeaser = dataTeaser;
		}

		public String getDataTeaser() {
			return dataTeaser;
		}

		public void setDataAuthor(String dataAuthor) {
			this.dataAuthor = dataAuthor;
		}

		public String getDataAuthor() {
			return dataAuthor;
		}

		public void setDataImageMain(String dataImageMain) {
			this.data_image_main = dataImageMain;
		}

		public String getDataImageMain() {
			return data_image_main;
		}

		public void setSourceName(String sourceName) {
			this.source_name = sourceName;
		}

		public String getSourceName() {
			return source_name;
		}

		public void setHipstoOwnRating(int hipstoOwnRating) {
			this.hipsto_own_rating = hipstoOwnRating;
		}

		public int getHipstoOwnRating() {
			return hipsto_own_rating;
		}

		public String getHipsto_own_bookmark_id() {
			return hipsto_own_bookmark_id;
		}

		public void setHipsto_own_bookmark_id(String hipsto_own_bookmark_id) {
			this.hipsto_own_bookmark_id = hipsto_own_bookmark_id;
		}

		public void setDataImagePreview(String dataImagePreview) {
			this.dataImagePreview = dataImagePreview;
		}

		public String getDataImagePreview() {
			return dataImagePreview;
		}

		public void setTimestamp(Timestamp timestamp) {
			this.timestamp = timestamp;
		}

		public Timestamp getTimestamp() {
			return timestamp;
		}

		public void setSourceLang(String sourceLang) {
			this.sourceLang = sourceLang;
		}

		public String getSourceLang() {
			return sourceLang;
		}

		public void setTimeRead(String timeRead) {
			this.time_read = timeRead;
		}

		public String getTimeRead() {
			return time_read;
		}

		public void setModerationReason(int moderationReason) {
			this.moderationReason = moderationReason;
		}

		public int getModerationReason() {
			return moderationReason;
		}

		public void setDataPublishedParsed(String dataPublishedParsed) {
			this.dataPublishedParsed = dataPublishedParsed;
		}

		public String getDataPublishedParsed() {
			return dataPublishedParsed;
		}

		public void setDataContent(String dataContent) {
			this.dataContent = dataContent;
		}

		public String getDataContent() {
			return dataContent;
		}

		public void setDataPublished(String dataPublished) {
			this.data_published = dataPublished;
		}

		public String getDataPublished() {
			return data_published;
		}

		public void setModerationDate(ModerationDate moderationDate) {
			this.moderationDate = moderationDate;
		}

		public ModerationDate getModerationDate() {
			return moderationDate;
		}

		public void setItemUrl(String itemUrl) {
			this.itemUrl = itemUrl;
		}

		public String getItemUrl() {
			return itemUrl;
		}

		public void setModerationStatus(int moderationStatus) {
			this.moderationStatus = moderationStatus;
		}

		public int getModerationStatus() {
			return moderationStatus;
		}

		public void setId(Id id) {
			this.id = id;
		}

		public Id getId() {
			return id;
		}

		public void setSourceCode(String sourceCode) {
			this.sourceCode = sourceCode;
		}

		public String getSourceCode() {
			return sourceCode;
		}

		public void setWordsCount(int wordsCount) {
			this.wordsCount = wordsCount;
		}

		public int getWordsCount() {
			return wordsCount;
		}

		public void setHipstoOwnImage(String hipstoOwnImage) {
			this.hipstoOwnImage = hipstoOwnImage;
		}

		public String getHipstoOwnImage() {
			return hipstoOwnImage;
		}


		public class Source {
			private String faviconUrl;
			private String domain;
			private String url;

			public void setFaviconUrl(String faviconUrl) {
				this.faviconUrl = faviconUrl;
			}

			public String getFaviconUrl() {
				return faviconUrl;
			}

			public void setDomain(String domain) {
				this.domain = domain;
			}

			public String getDomain() {
				return domain;
			}

			public void setUrl(String url) {
				this.url = url;
			}

			public String getUrl() {
				return url;
			}
		}

		public class SourceCreatedAt {
			private Date date;

			public void setDate(Date date) {
				this.date = date;
			}

			public Date getDate() {
				return date;
			}
		}

		public class Timestamp {
			private Date date;

			public void setDate(Date date) {
				this.date = date;
			}

			public Date getDate() {
				return date;
			}
		}

		public class ModerationDate {
			private Date date;

			public void setDate(Date date) {
				this.date = date;
			}

			public Date getDate() {
				return date;
			}
		}
	}

}
