package hipsto.app.orderfox.models;

public class NewsPostResponse{
	private boolean success;
	private int count;

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}
}
