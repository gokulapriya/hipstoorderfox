package hipsto.app.orderfox.models;

/**
 * Created by Gokulapriya on 7/20/2019.
 */

public class TopicLangResponse {
    private String code;

    public TopicLangResponse(String code, String name, String aNative) {
        this.code = code;
        this.name = name;
        this.Native = aNative;
    }


    public String getNative() {
        return Native;
    }

    public void setNative(String aNative) {
        Native = aNative;
    }

    private String Native;
    private String name;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
