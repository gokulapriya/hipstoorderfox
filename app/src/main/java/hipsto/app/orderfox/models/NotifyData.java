package hipsto.app.orderfox.models;

/**
 * Created by Gokulapriya on 9/16/2019.
 */

public class NotifyData {

    private Integer notify_id;
    private String pref_device_token;
    private String pref_fcm_token;
    private String news_id;

    public NotifyData(Integer notify_id, String pref_device_token, String pref_fcm_token, String news_id) {
        this.notify_id = notify_id;
        this.pref_device_token = pref_device_token;
        this.pref_fcm_token = pref_fcm_token;
        this.news_id = news_id;
    }

    public Integer getNotify_id() {
        return notify_id;
    }

    public void setNotify_id(Integer notify_id) {
        this.notify_id = notify_id;
    }

    public String getPref_device_token() {
        return pref_device_token;
    }

    public void setPref_device_token(String pref_device_token) {
        this.pref_device_token = pref_device_token;
    }

    public String getPref_fcm_token() {
        return pref_fcm_token;
    }

    public void setPref_fcm_token(String pref_fcm_token) {
        this.pref_fcm_token = pref_fcm_token;
    }

    public String getNews_id() {
        return news_id;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }
}
