package hipsto.app.orderfox.Sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hipsto.app.orderfox.models.Bookmark;
import hipsto.app.orderfox.models.LoginData;
import hipsto.app.orderfox.models.NotifyData;
import hipsto.app.orderfox.models.Rating;
import hipsto.app.orderfox.models.TopicLangResponse;

/**
 * Created by Gokulapriya on 6/25/2019.
 */

public class DBQueries {

    private Context context;
    private SQLiteDatabase database;
    private DBHelper dbHelper;

    public DBQueries(Context context) {
        this.context = context;
    }

    public DBQueries open(Context context) throws SQLException {
        dbHelper = new DBHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    /*DELETE FUNCTIONS*/

    public void deleteAllData()
    {
        // If whereClause is null, it will delete all rows.
        SQLiteDatabase db = dbHelper.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(DBConstants.LOGIN_TABLE, null, null);
        if(isTableExists(DBConstants.NOTIFY_TABLE)) {
            db.delete(DBConstants.NOTIFY_TABLE, null, null);
        } else {
            Log.d("","NOTIFY_TABLE");
        }
        db.delete(DBConstants.BOOKMARK_TABLE, null, null);
        db.delete(DBConstants.SETTINGLANG_TABLE, null, null);
        db.delete(DBConstants.SETTINGSTREAM_TABLE, null, null);
        db.delete(DBConstants.RATING_TABLE, null, null);
        db.delete(DBConstants.RATING_TABLE_ID, null, null);
        db.delete(DBConstants.BOOKMARK_TABLE_ID, null, null);
        db.delete(DBConstants.NEWSFEEDID_TABLE, null, null);
        db.delete(DBConstants.REPORTID_TABLE, null, null);
        db.delete(DBConstants.SHAREID_TABLE, null, null);
    }

    public boolean isTableExists(String tableName) {
        boolean isExist = false;
        Cursor cursor = database.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'",null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                isExist = true;
            }
            cursor.close();
        }
        return isExist;
    }
    /*LOGIN_TABLE INSERT, READ, UPDATE FUNCTIONS*/

    public boolean insertLogin(LoginData loginData) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.LOGINID, loginData.getLogin_id());
        values.put(DBConstants.PREFTOKEN, loginData.getPref_token());
        values.put(DBConstants.PREFEXPRY, loginData.getPref_expry());
        values.put(DBConstants.PREFISLOGIN, loginData.getIslogin());
        values.put(DBConstants.PREFCOOKIE, loginData.getCookies());
        values.put(DBConstants.PREFUSERID, loginData.getUser_id());
        values.put(DBConstants.PREFUSERIDENCODE, loginData.getUser_id_encode());

        return database.insert(DBConstants.LOGIN_TABLE, null, values) > -1;
    }

    public boolean updateLogin(Integer loginId,String token,String expiry,Integer is_login) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.PREFTOKEN,token);
        values.put(DBConstants.PREFEXPRY, expiry);
        values.put(DBConstants.PREFISLOGIN, is_login);

        return database.update(DBConstants.LOGIN_TABLE, values, "LOGINID ="+loginId, null)> 0;
    }

    public boolean updateCookie(Integer loginId,String cookie) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.PREFCOOKIE, cookie);

        return database.update(DBConstants.LOGIN_TABLE, values, "LOGINID ="+loginId, null)> 0;
    }

    public ArrayList<LoginData> readLogin() {
        ArrayList<LoginData> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery(DBConstants.SELECT_QUERY_LOGIN_TABLE, null);
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        Integer login_id = cursor.getInt(cursor.getColumnIndex(DBConstants.LOGINID));
                        String token = cursor.getString(cursor.getColumnIndex(DBConstants.PREFTOKEN));
                        String expiry = cursor.getString(cursor.getColumnIndex(DBConstants.PREFEXPRY));
                        Integer isLogin = cursor.getInt(cursor.getColumnIndex(DBConstants.PREFISLOGIN));
                        String cookie = cursor.getString(cursor.getColumnIndex(DBConstants.PREFCOOKIE));
                        String user_id = cursor.getString(cursor.getColumnIndex(DBConstants.PREFUSERID));
                        String user_id_encode = cursor.getString(cursor.getColumnIndex(DBConstants.PREFUSERIDENCODE));

                        list.add(new LoginData(login_id,token,expiry,isLogin,cookie,user_id,user_id_encode));
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return list;
    }

    public LoginData readLoginID(Integer id) {
        LoginData loginData = null;

        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery("select * from LOGIN_TABLE where LOGINID='"+id+"'", null);
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        Integer login_id = cursor.getInt(cursor.getColumnIndex(DBConstants.LOGINID));
                        String token = cursor.getString(cursor.getColumnIndex(DBConstants.PREFTOKEN));
                        String expiry = cursor.getString(cursor.getColumnIndex(DBConstants.PREFEXPRY));
                        Integer isLogin = cursor.getInt(cursor.getColumnIndex(DBConstants.PREFISLOGIN));
                        String cookie = cursor.getString(cursor.getColumnIndex(DBConstants.PREFCOOKIE));
                        String user_id = cursor.getString(cursor.getColumnIndex(DBConstants.PREFUSERID));
                        String user_id_encode = cursor.getString(cursor.getColumnIndex(DBConstants.PREFUSERIDENCODE));

                        loginData = new LoginData(login_id,token,expiry,isLogin,cookie,user_id,user_id_encode);

                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return loginData;

    }

    /*NOTIFY_TABLE INSERT, READ, UPDATE FUNCTIONS*/

    public boolean insertNotify(NotifyData notifyData) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.NOTIFYID, notifyData.getNotify_id());
        values.put(DBConstants.PREFDEVICETOKEN, notifyData.getPref_device_token());
        values.put(DBConstants.PREFFCMTOKEN, notifyData.getPref_fcm_token());
        values.put(DBConstants.PREFNEWSID, notifyData.getNews_id());

        return database.insert(DBConstants.NOTIFY_TABLE, null, values) > -1;
    }

    public boolean updateNewsID(Integer notifyid,String newsid) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.PREFNEWSID, newsid);

        return database.update(DBConstants.NOTIFY_TABLE, values, "NOTIFYID ="+notifyid, null)> 0;
    }

    public NotifyData readNotifyID(Integer id) {
        NotifyData notifyData = null;

        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery("select * from NOTIFY_TABLE where NOTIFYID='"+id+"'", null);
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        Integer notify_id = cursor.getInt(cursor.getColumnIndex(DBConstants.NOTIFYID));
                        String device_token = cursor.getString(cursor.getColumnIndex(DBConstants.PREFDEVICETOKEN));
                        String fcm_token = cursor.getString(cursor.getColumnIndex(DBConstants.PREFFCMTOKEN));
                        String news_id = cursor.getString(cursor.getColumnIndex(DBConstants.PREFNEWSID));

                        notifyData = new NotifyData(notify_id,device_token,fcm_token,news_id);

                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return notifyData;

    }

    /*BOOKMARK_TABLE INSERT, READ, DELETE FUNCTIONS*/

    public boolean insertBookmark(Bookmark bookmark) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.TOPIC_CODE, bookmark.getTopicCode());
        values.put(DBConstants.MID, bookmark.getMid());
        values.put(DBConstants.DATA_IMAGE_MAIN, bookmark.getData_image_main());
        values.put(DBConstants.ITEM_URL, bookmark.getItem_url());
        values.put(DBConstants.DATA_TITLE, bookmark.getData_title());
        values.put(DBConstants.DATA_CONTENT, bookmark.getDataContent());
        values.put(DBConstants.DATA_CONTENT_FULL, bookmark.getData_content_full());
        values.put(DBConstants.DOMAIN, bookmark.getDomain());
        values.put(DBConstants.DOMAIN_URL, bookmark.getUrl());
        values.put(DBConstants.DATA_PUBLISHED, bookmark.getData_published());
        values.put(DBConstants.SOURCE_NAME, bookmark.getSource_name());
        values.put(DBConstants.SOURCE_LANG, bookmark.getSource_lang());
        values.put(DBConstants.HIPSTO_OWN_RATING, bookmark.getHipsto_own_rating().toString());
        values.put(DBConstants.HIPSTO_OWN_VIEWS, bookmark.getHipsto_own_views().toString());
        values.put(DBConstants.HIPSTO_OWN_BOOKMARK_ID, bookmark.getHipsto_own_bookmark_id());
        values.put(DBConstants.TIME_READ, bookmark.getTime_read());
        return database.insert(DBConstants.BOOKMARK_TABLE, null, values) > -1;

    }


    public ArrayList<Bookmark> readBookmark() {
        ArrayList<Bookmark> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery(DBConstants.SELECT_QUERY_FOR_LOG, null);
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        String topicCode = cursor.getString(cursor.getColumnIndex(DBConstants.TOPIC_CODE));
                        String mid = cursor.getString(cursor.getColumnIndex(DBConstants.MID));
                        String data_image_main = cursor.getString(cursor.getColumnIndex(DBConstants.DATA_IMAGE_MAIN));
                        String item_url = cursor.getString(cursor.getColumnIndex(DBConstants.ITEM_URL));
                        String data_title = cursor.getString(cursor.getColumnIndex(DBConstants.DATA_TITLE));
                        String dataContent = cursor.getString(cursor.getColumnIndex(DBConstants.DATA_CONTENT));
                        String data_content_full = cursor.getString(cursor.getColumnIndex(DBConstants.DATA_CONTENT_FULL));
                        String domain = cursor.getString(cursor.getColumnIndex(DBConstants.DOMAIN));
                        String domainUrl = cursor.getString(cursor.getColumnIndex(DBConstants.DOMAIN_URL));
                        String data_published = cursor.getString(cursor.getColumnIndex(DBConstants.DATA_PUBLISHED));
                        String source_name = cursor.getString(cursor.getColumnIndex(DBConstants.SOURCE_NAME));
                        String source_lang = cursor.getString(cursor.getColumnIndex(DBConstants.SOURCE_LANG));
                        Integer hipsto_own_rating = Integer.valueOf(cursor.getString(cursor.getColumnIndex(DBConstants.HIPSTO_OWN_RATING)));
                        Integer hipsto_own_views = Integer.valueOf(cursor.getString(cursor.getColumnIndex(DBConstants.HIPSTO_OWN_VIEWS)));
                        String hipsto_own_bookmark_id = cursor.getString(cursor.getColumnIndex(DBConstants.HIPSTO_OWN_BOOKMARK_ID));
                        String time_read = cursor.getString(cursor.getColumnIndex(DBConstants.TIME_READ));

                        Bookmark bookmark = new Bookmark(topicCode,
                                mid,
                                data_image_main,
                                item_url,
                                data_title,
                                dataContent,
                                data_content_full,
                                domain,
                                domainUrl,
                                data_published,
                                source_name,
                                source_lang,
                                hipsto_own_rating,
                                hipsto_own_views,
                                hipsto_own_bookmark_id,
                                time_read);

                        list.add(bookmark);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return list;
    }

    public boolean deleteBookmark(Bookmark bookmark) {
        String whereClause = "MID=?";
        String whereArgs[] = {bookmark.getMid().toString()};
        return database.delete(DBConstants.BOOKMARK_TABLE, whereClause, whereArgs) > 0;

    }

    /*BOOKMARK_TABLE_ID INSERT, READ, DELETE FUNCTIONS*/

    public boolean insertBookmarkId(String bookmarkId) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.BOOKMARK_ID, bookmarkId);

        return database.insert(DBConstants.BOOKMARK_TABLE_ID, null, values) > -1;
    }

    public ArrayList<String> readBookmarkId() {
        ArrayList<String> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery(DBConstants.SELECT_QUERY_FOR_ID, null);
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        String bookmarkId = cursor.getString(cursor.getColumnIndex(DBConstants.BOOKMARK_ID));

                        list.add(bookmarkId);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return list;
    }

    public boolean deleteBookmarkId(String bookmarkId) {
        String whereClause = "BOOKMARK_ID=?";
        String whereArgs[] = {bookmarkId};
        return database.delete(DBConstants.BOOKMARK_TABLE_ID, whereClause, whereArgs) > 0;
    }

    /*SETTINGLANG_TABLE INSERT, READ, DELETE FUNCTIONS*/


    public boolean insertSettingLang(TopicLangResponse topicLangResponse) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.LANGCODE, topicLangResponse.getCode());
        values.put(DBConstants.LANGNAME, topicLangResponse.getName());
        values.put(DBConstants.LANGNATIVE, topicLangResponse.getNative());

        return database.insert(DBConstants.SETTINGLANG_TABLE, null, values) > -1;
    }

    public boolean deleteSettingsLang(TopicLangResponse topicLangResponse) {
        String whereClause = "LANGCODE=?";
        String whereArgs[] = {topicLangResponse.getCode().toString()};
        return database.delete(DBConstants.SETTINGLANG_TABLE, whereClause, whereArgs) > 0;

    }

    public List<TopicLangResponse> readSettingLang() {
        ArrayList<TopicLangResponse> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery(DBConstants.SELECT_QUERY_SETTINGLANG, null);
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        String name = cursor.getString(cursor.getColumnIndex(DBConstants.LANGNAME));
                        String code = cursor.getString(cursor.getColumnIndex(DBConstants.LANGCODE));
                        String Native = cursor.getString(cursor.getColumnIndex(DBConstants.LANGNATIVE));

                        TopicLangResponse dataContent = new TopicLangResponse(code,
                                name,
                                Native
                        );

                        list.add(dataContent);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return list;

    }


    /*SETTINGLANG_TABLE INSERT, READ, DELETE FUNCTIONS*/

    public boolean insertSettingStream(String streamName) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.STREAMNAME, streamName);

        return database.insert(DBConstants.SETTINGSTREAM_TABLE, null, values) > -1;
    }


    public boolean deleteSettingStream(String streamName) {
        String whereClause = "STREAMNAME=?";
        String whereArgs[] = {streamName};
        return database.delete(DBConstants.SETTINGSTREAM_TABLE, whereClause, whereArgs) > 0;
    }


    public List<String> readSettingStream() {
        ArrayList<String> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery(DBConstants.SELECT_QUERY_SETTINGSTREAM, null);
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        String name = cursor.getString(cursor.getColumnIndex(DBConstants.STREAMNAME));


                        list.add(name);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return list;

    }

    /*NEWSFEEDID_TABLE INSERT, READ FUNCTIONS*/

    public boolean insertNewsFeedId(String newsfeedId) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.NEWSFEEDID, newsfeedId);

        return database.insert(DBConstants.NEWSFEEDID_TABLE, null, values) > -1;
    }

    public ArrayList<String> readNewsFeedId() {
        ArrayList<String> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery(DBConstants.SELECT_QUERY_NEWSFEEDID, null);
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        String newsfeedId = cursor.getString(cursor.getColumnIndex(DBConstants.NEWSFEEDID));
                        list.add(newsfeedId);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return list;
    }

    /*RATING_TABLE INSERT, READ FUNCTIONS*/

    public boolean insertRating(Rating rating) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.UID, rating.getuId());
        values.put(DBConstants.UIDENCODE, rating.getuIdEncode());
        values.put(DBConstants.NEWSID, rating.getNewsId());
        values.put(DBConstants.RATING, rating.getRating());


        return database.insert(DBConstants.RATING_TABLE, null, values) > -1;
    }

    public List<Rating> readRating() {
        ArrayList<Rating> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery(DBConstants.SELECT_QUERY_RATING_TABLE, null);
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        String uId = cursor.getString(cursor.getColumnIndex(DBConstants.UID));
                        String uIdEncode = cursor.getString(cursor.getColumnIndex(DBConstants.UIDENCODE));
                        String newsId = cursor.getString(cursor.getColumnIndex(DBConstants.NEWSID));
                        Integer rating = cursor.getInt(cursor.getColumnIndex(DBConstants.RATING));

                        Rating rating1 = new Rating(uId, uIdEncode, newsId, rating);

                        list.add(rating1);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return list;
    }

    public boolean deleteRating(Rating rating) {
        String whereClause = "NEWSID=?";
        String whereArgs[] = {rating.getNewsId().toString()};
        return database.delete(DBConstants.RATING_TABLE, whereClause, whereArgs) > 0;

    }



    /*BOOKMARK_TABLE_ID INSERT, READ, DELETE FUNCTIONS*/

    public boolean insertRatingId(String ratingId) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.RATING_ID, ratingId);

        return database.insert(DBConstants.RATING_TABLE_ID, null, values) > -1;
    }

    public ArrayList<String> readRatingId() {
        ArrayList<String> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery(DBConstants.SELECT_QUERY_FOR_RATING_ID, null);
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        String ratingId = cursor.getString(cursor.getColumnIndex(DBConstants.RATING_ID));

                        list.add(ratingId);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return list;
    }

    public boolean deleteRatingId(String ratingId) {
        String whereClause = "RATING_ID=?";
        String whereArgs[] = {ratingId};
        return database.delete(DBConstants.RATING_TABLE_ID, whereClause, whereArgs) > 0;
    }


    /*REPORTID_TABLE INSERT, READ, DELETE FUNCTIONS*/

    public boolean insertReportId(String reportId) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.REPORT_ID, reportId);

        return database.insert(DBConstants.REPORTID_TABLE, null, values) > -1;
    }

    public ArrayList<String> readReportId() {
        ArrayList<String> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery(DBConstants.SELECT_QUERY_FOR_REPORT_ID, null);
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        String reportId = cursor.getString(cursor.getColumnIndex(DBConstants.REPORT_ID));

                        list.add(reportId);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return list;
    }

    public boolean deleteReportId(String reportId) {
        String whereClause = "REPORT_ID=?";
        String whereArgs[] = {reportId};
        return database.delete(DBConstants.REPORTID_TABLE, whereClause, whereArgs) > 0;
    }

    /*SHAREID_TABLE INSERT, READ FUNCTIONS*/

    public boolean insertShareId(String shareId) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.SHARE_ID, shareId);

        return database.insert(DBConstants.SHAREID_TABLE, null, values) > -1;
    }

    public ArrayList<String> readShareId() {
        ArrayList<String> list = new ArrayList<>();
        try {
            Cursor cursor;
            database = dbHelper.getReadableDatabase();
            cursor = database.rawQuery(DBConstants.SELECT_QUERY_SHARE_TABLE, null);
            list.clear();
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        String shareId = cursor.getString(cursor.getColumnIndex(DBConstants.SHARE_ID));

                        list.add(shareId);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.v("Exception", e.getMessage());
        }
        return list;
    }
}
