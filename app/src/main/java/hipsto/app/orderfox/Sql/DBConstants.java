package hipsto.app.orderfox.Sql;

/**
 * Created by Gokulapriya on 6/25/2019.
 */

public class DBConstants {

    //Table Name
    public static final String LOGIN_TABLE = "LOGIN_TABLE";
    public static final String NOTIFY_TABLE = "NOTIFY_TABLE";
    public static final String BOOKMARK_TABLE = "BOOKMARK_TABLE";
    public static final String BOOKMARK_TABLE_ID = "BOOKMARK_TABLE_ID";

    public static final String SETTINGLANG_TABLE = "SETTINGLANG_TABLE";
    public static final String SETTINGSTREAM_TABLE = "SETTINGSTREAM_TABLE";

    public static final String RATING_TABLE = "RATING_TABLE";
    public static final String RATING_TABLE_ID = "RATING_TABLE_ID";

    public static final String NEWSFEEDID_TABLE = "NEWSFEEDID_TABLE";

    public static final String REPORTID_TABLE = "REPORTID_TABLE";

    public static final String SHAREID_TABLE = "SHAREID_TABLE";

    //Table Attributes Name

    /*LOGIN_TABLE*/
    static final String LOGINID = "LOGINID";
    static final String PREFTOKEN = "PREFTOKEN";
    static final String PREFEXPRY = "PREFEXPRY";
    static final String PREFISLOGIN = "PREFISLOGIN";
    static final String PREFCOOKIE = "PREFCOOKIE";
    static final String PREFUSERID = "PREFUSERID";
    static final String PREFUSERIDENCODE = "PREFUSERIDENCODE";

    /*NOTIFY_TABLE*/
    static final String NOTIFYID = "NOTIFYID";
    static final String PREFDEVICETOKEN = "PREFDEVICETOKEN";
    static final String PREFFCMTOKEN = "PREFFCMTOKEN";
    static final String PREFNEWSID = "PREFNEWSID";

    /*BOOKMARK_TABLE*/
    static final String TOPIC_CODE = "TOPIC_CODE";
    static final String MID = "MID";
    static final String DATA_IMAGE_MAIN = "DATA_IMAGE_MAIN";
    static final String ITEM_URL = "ITEM_URL";
    static final String DATA_TITLE = "DATA_TITLE";
    static final String DATA_CONTENT = "DATA_CONTENT";
    static final String DATA_CONTENT_FULL = "DATA_CONTENT_FULL";
    static final String DOMAIN = "DOMAIN";
    static final String DOMAIN_URL = "DOMAIN_URL";
    static final String DATA_PUBLISHED = "DATA_PUBLISHED";
    static final String SOURCE_NAME = "SOURCE_NAME";
    static final String SOURCE_LANG = "SOURCE_LANG";
    static final String HIPSTO_OWN_RATING = "HIPSTO_OWN_RATING";
    static final String HIPSTO_OWN_VIEWS = "HIPSTO_OWN_VIEWS";
    static final String HIPSTO_OWN_BOOKMARK_ID = "HIPSTO_OWN_BOOKMARK_ID";
    static final String TIME_READ = "TIME_READ";

    /*BOOKMARK_TABLE_ID*/
    static final String BOOKMARK_ID = "BOOKMARK_ID";

    /*RATING_TABLE_ID*/
    static final String RATING_ID = "RATING_ID";

    /*SETTINGSTREAM_TABLE*/
    static final String STREAMNAME = "STREAMNAME";

    /*REPORTID_TABLE*/
    static final String REPORT_ID = "REPORT_ID";

    /*SHAREID_TABLE*/
    static final String SHARE_ID = "SHARE_ID";

    /*SETTINGLANG_TABLE*/
    static final String LANGNAME = "LANGNAME";
    static final String LANGCODE = "LANGCODE";
    static final String LANGNATIVE = "LANGNATIVE";

    /*RATING_TABLE*/
    static final String UID = "UID";
    static final String UIDENCODE = "UIDENCODE";
    static final String NEWSID = "NEWSID";
    static final String RATING = "RATING";

    /*NEWSFEEDID_TABLE*/
    static final String NEWSFEEDID = "NEWSFEEDID";

    //LOGIN_TABLE CREATE QUERY
    static final String CREATE_LOGIN_TABLE = "CREATE TABLE IF NOT EXISTS " + LOGIN_TABLE + " ("
            + LOGINID + " INTEGER,"
            + PREFTOKEN + " TEXT,"
            + PREFEXPRY + " TEXT,"
            + PREFISLOGIN + " INTEGER,"
            + PREFCOOKIE + " TEXT,"
            + PREFUSERID + " TEXT,"
            + PREFUSERIDENCODE + " TEXT,"
            + PREFDEVICETOKEN + " TEXT,"
            + PREFFCMTOKEN + " TEXT,"
            + PREFNEWSID + " TEXT)";


    //LOGIN_TABLE READ QUERY
    static final String SELECT_QUERY_LOGIN_TABLE = "SELECT * FROM " + LOGIN_TABLE;

    //NOTIFY_TABLE CREATE QUERY
    static final String CREATE_NOTIFY_TABLE = "CREATE TABLE IF NOT EXISTS " + NOTIFY_TABLE + " ("
            + NOTIFYID + " INTEGER,"
            + PREFDEVICETOKEN + " TEXT,"
            + PREFFCMTOKEN + " TEXT,"
            + PREFNEWSID + " TEXT)";


    //NOTIFY_TABLE READ QUERY
    static final String SELECT_QUERY_NOTIFY_TABLE = "SELECT * FROM " + NOTIFY_TABLE;


    //BOOKMARK_TABLE CREATE QUERY
    static final String CREATE_BOOKMARK_TABLE = "CREATE TABLE IF NOT EXISTS " + BOOKMARK_TABLE + " ("

            + TOPIC_CODE + " TEXT,"
            + MID + " TEXT,"
            + DATA_IMAGE_MAIN + " TEXT,"
            + ITEM_URL + " TEXT,"
            + DATA_TITLE + " TEXT,"
            + DATA_CONTENT + " TEXT,"
            + DATA_CONTENT_FULL + " TEXT,"
            + DOMAIN + " TEXT,"
            + DOMAIN_URL + " TEXT,"
            + DATA_PUBLISHED + " TEXT,"
            + SOURCE_NAME + " TEXT,"
            + SOURCE_LANG + " TEXT,"
            + HIPSTO_OWN_RATING + " TEXT,"
            + HIPSTO_OWN_VIEWS + " TEXT,"
            + HIPSTO_OWN_BOOKMARK_ID + " TEXT,"
            + TIME_READ + " TEXT)";

    //BOOKMARK_TABLE READ QUERY
    static final String SELECT_QUERY_FOR_LOG = "SELECT * FROM " + BOOKMARK_TABLE;

    //BOOKMARK_TABLE_ID CREATE QUERY
    static final String CREATE_BOOKMARK_TABLE_ID = "CREATE TABLE IF NOT EXISTS " + BOOKMARK_TABLE_ID + " ("
            + BOOKMARK_ID + " TEXT)";

    //BOOKMARK_TABLE_ID READ QUERY
    static final String SELECT_QUERY_FOR_ID = "SELECT * FROM " + BOOKMARK_TABLE_ID;

    //SETTINGLANG_TABLE CREATE QUERY
    static final String CREATE_SETTINGLANG_TABLE = "CREATE TABLE IF NOT EXISTS " + SETTINGLANG_TABLE + " ("
            + LANGNAME + " TEXT,"
            + LANGCODE + " TEXT,"
            + LANGNATIVE + " TEXT)";

    //SETTINGLANG_TABLE READ QUERY
    static final String SELECT_QUERY_SETTINGLANG = "SELECT * FROM " + SETTINGLANG_TABLE;

    //NEWSFEEDID_TABLE CREATE
    static final String CREATE_NEWSFEEDID_TABLE = "CREATE TABLE IF NOT EXISTS " + NEWSFEEDID_TABLE + " (" + NEWSFEEDID + " TEXT) ";

    //NEWSFEEDID_TABLE READ QUERY
    static final String SELECT_QUERY_NEWSFEEDID = "SELECT * FROM " + NEWSFEEDID_TABLE;

    //SETTINGSTREAM_TABLE CREATE
    static final String CREATE_SETTINGSTREAM_TABLE = "CREATE TABLE IF NOT EXISTS " + SETTINGSTREAM_TABLE + " ("
            + STREAMNAME + " TEXT)";

    //SETTINGSTREAM_TABLE READ QUERY
    static final String SELECT_QUERY_SETTINGSTREAM = "SELECT * FROM " + SETTINGSTREAM_TABLE;

    //RATING_TABLE CREATE QUERY
    static final String CREATE_RATING_TABLE = "CREATE TABLE IF NOT EXISTS " + RATING_TABLE + " ("
            + UID + " TEXT,"
            + UIDENCODE + " TEXT,"
            + NEWSID + " TEXT,"
            + RATING + " INTEGER)";

    //RATING_TABLE READ QUERY
    static final String SELECT_QUERY_RATING_TABLE = "SELECT * FROM " + RATING_TABLE;


    //BRATING_TABLE_ID CREATE QUERY
    static final String CREATE_RATING_TABLE_ID = "CREATE TABLE IF NOT EXISTS " + RATING_TABLE_ID + " ("
            + RATING_ID + " TEXT)";

    //RATING_TABLE_ID READ QUERY
    static final String SELECT_QUERY_FOR_RATING_ID = "SELECT * FROM " + RATING_TABLE_ID;



    //SHARE_TABLE CREATE QUERY
    static final String CREATE_SHARE_TABLE = "CREATE TABLE IF NOT EXISTS " + SHAREID_TABLE + " ("
            + SHARE_ID + " TEXT)";

    //SHARE_TABLE READ QUERY
    static final String SELECT_QUERY_SHARE_TABLE = "SELECT * FROM " + SHAREID_TABLE;

    //REPORTID_TABLE CREATE QUERY
    static final String CREATE_REPORTID_TABLE= "CREATE TABLE IF NOT EXISTS " + REPORTID_TABLE + " ("
            + REPORT_ID + " TEXT)";

    //REPORTID_TABLE READ QUERY
    static final String SELECT_QUERY_FOR_REPORT_ID = "SELECT * FROM " + REPORTID_TABLE;

}
