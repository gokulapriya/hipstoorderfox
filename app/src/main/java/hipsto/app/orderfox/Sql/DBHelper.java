package hipsto.app.orderfox.Sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Gokulapriya on 6/25/2019.
 */

public class DBHelper extends SQLiteOpenHelper {

    private Context context;
    public static String DB_NAME = "hipstoDB";
    private static int DB_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBConstants.CREATE_LOGIN_TABLE);
        db.execSQL(DBConstants.CREATE_NOTIFY_TABLE);
        db.execSQL(DBConstants.CREATE_BOOKMARK_TABLE);
        db.execSQL(DBConstants.CREATE_SETTINGLANG_TABLE);
        db.execSQL(DBConstants.CREATE_SETTINGSTREAM_TABLE);
        db.execSQL(DBConstants.CREATE_BOOKMARK_TABLE_ID);
        db.execSQL(DBConstants.CREATE_RATING_TABLE);
        db.execSQL(DBConstants.CREATE_RATING_TABLE_ID);
        db.execSQL(DBConstants.CREATE_NEWSFEEDID_TABLE);
        db.execSQL(DBConstants.CREATE_REPORTID_TABLE);
        db.execSQL(DBConstants.CREATE_SHARE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.LOGIN_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.NOTIFY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.BOOKMARK_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.SETTINGLANG_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.SETTINGSTREAM_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.RATING_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.RATING_TABLE_ID);
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.BOOKMARK_TABLE_ID);
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.NEWSFEEDID_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.REPORTID_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DBConstants.SHAREID_TABLE);
    }

}
