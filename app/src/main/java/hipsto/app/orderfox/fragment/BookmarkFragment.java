package hipsto.app.orderfox.fragment;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.Sql.DBHelper;
import hipsto.app.orderfox.Sql.DBQueries;
import hipsto.app.orderfox.activity.HomeActivity;
import hipsto.app.orderfox.adapter.BookmarkListAdapter;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.models.NewsFeedResponse;
import hipsto.app.orderfox.retrofit.HipstoAPI;
import hipsto.app.orderfox.utils.Constants;
import hipsto.app.orderfox.utils.Utilsdefault;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Gokulapriya on 6/20/2019.
 */

public class BookmarkFragment extends BaseFragment {

    private RecyclerView rvBookmarkList;
    private TextView tvNoBookmarkList;
    BookmarkListAdapter bookmarkListAdapter;
    //private ArrayList<Bookmark> newsBookmarkListArray = new ArrayList<>();
    private ArrayList<String> newsBookmarkIdList = new ArrayList<>();
    private List<NewsFeedResponse.ItemsItem> newsBookmarkListArray = new ArrayList<>();
    private NewsFeedResponse feedResponse = new NewsFeedResponse();
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public HipstoAPI hipstoAPI;
    ProgressDialog mDialog;
    DBHelper dbHelper;
    DBQueries dbQueries;
    EditText etSearch;

    public RelativeLayout rtSearch, rtDelete, rtPageTop, rtShare, rtWhatapp, rtFb, rtTelegram, rtMoreOption;
    boolean loading = true;
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    private FirebaseAnalytics mFirebaseAnalytics;
    Boolean start = true;
    Boolean search = true;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_bookmark, container, false);

        HipstoApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        initViews(v);
        performClick(v);


        return v;
    }

    private void initViews(View v) {

        rvBookmarkList = (RecyclerView) v.findViewById(R.id.rv_bookmark_list);
        tvNoBookmarkList = (TextView) v.findViewById(R.id.tv_no_bookmark_list);

        //share
        rtShare = (RelativeLayout) v.findViewById(R.id.rt_share);
        rtWhatapp = (RelativeLayout) v.findViewById(R.id.rt_whatapp);
        rtFb = (RelativeLayout) v.findViewById(R.id.rt_fb);
        rtTelegram = (RelativeLayout) v.findViewById(R.id.rt_telegram);
        rtMoreOption = (RelativeLayout) v.findViewById(R.id.rt_more);
        rtPageTop = (RelativeLayout) v.findViewById(R.id.rt_page_top);
        rtSearch = (RelativeLayout) v.findViewById(R.id.rt_search);
        rtDelete = (RelativeLayout) v.findViewById(R.id.rt_delete);
        etSearch = (EditText) v.findViewById(R.id.et_search);
        swipeRefresh = v.findViewById(R.id.swipe_refresh);
        swipeRefresh.setColorSchemeResources(R.color.colorButton);

        dbHelper = new DBHelper(getContext());
        dbQueries = new DBQueries(getContext());
        dbQueries.open(getActivity());

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        HomeActivity.rt_navigationView.setVisibility(View.VISIBLE);
        HomeActivity.toolbar.setVisibility(View.VISIBLE);
        HomeActivity.rtSetting.setVisibility(View.VISIBLE);
        HomeActivity.rt_bookmark.setVisibility(View.GONE);
        HomeActivity.rtSearch.setVisibility(View.VISIBLE);
        HomeActivity.ivLogo.setEnabled(true);
        editor.putInt(Constants.LOADFRAGMENT,0).apply();

        rvBookmarkList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvBookmarkList.setItemAnimator(new DefaultItemAnimator());
        rvBookmarkList.setLayoutManager(linearLayoutManager);

        /*Firebase Event log*/
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle bun = new Bundle();
        bun.putString(FirebaseAnalytics.Param.ITEM_ID, "0");
        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Bookmark Page");
        bun.putString(FirebaseAnalytics.Param.CONTENT, "Bookmark Page Click");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);


        bookmarkListAdapter = new BookmarkListAdapter(getContext(), newsBookmarkListArray, BookmarkFragment.this);
        rvBookmarkList.setAdapter(bookmarkListAdapter);

        if(search){
            System.out.println("Hii search no");
            rtSearch.setVisibility(View.GONE);
            HomeActivity.rtSearch.setVisibility(View.VISIBLE);
        }else{
            System.out.println("Hii search Yes");
            rtSearch.setVisibility(View.VISIBLE);
            HomeActivity.rtSearch.setVisibility(View.GONE);
        }

        if (Utilsdefault.isNetworkAvailable()) {
            if (start) {
                getBookmark("");
                start = false;
            } else {
                if (newsBookmarkListArray.size() == 0) {
                    tvNoBookmarkList.setVisibility(View.VISIBLE);
                    rvBookmarkList.setVisibility(View.GONE);
                }else{
                    bookmarkListAdapter.notifyDataSetChanged();
                }
            }
        } else {
            if (getActivity() != null) {
                Utilsdefault.alertNetwork(getActivity());
            }
        }
        //getBookmarkDb();
    }

    private void performClick(View v) {

        HomeActivity.rtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.rtSearch.setVisibility(View.GONE);
                rtSearch.setVisibility(View.VISIBLE);
                search = false;
            }
        });

        HomeActivity.ivLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                fragment = new NewsFragment();
                ((HomeActivity) getContext()).loadFragment(fragment);
            }
        });

        rtPageTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvBookmarkList.scrollToPosition(0);
                rtPageTop.setVisibility(View.GONE);
            }
        });

        rtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSearch.getText().toString().length() > 0) {
                    etSearch.setText("");
                } else {
                    newsBookmarkListArray.clear();
                    newsBookmarkIdList.clear();
                    rvBookmarkList.setAdapter(null);
                    bookmarkListAdapter = new BookmarkListAdapter(getContext(), newsBookmarkListArray, BookmarkFragment.this);
                    rvBookmarkList.setAdapter(bookmarkListAdapter);
                    getBookmark("");
                    //newsBookmarkListArray = dbQueries.readBookmark();
                    etSearch.setText("");
                    search = true;
                    HomeActivity.rtSearch.setVisibility(View.VISIBLE);
                    rtSearch.setVisibility(View.GONE);
                }
            }
        });

        etSearch.setFilters(new InputFilter[]{new EmojiExcludeFilter()});
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (etSearch.getText().toString().toLowerCase().trim().length() > 0) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etSearch.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        newsBookmarkListArray.clear();
                        newsBookmarkIdList.clear();
                        rvBookmarkList.setAdapter(null);
                        search = false;
                        bookmarkListAdapter = new BookmarkListAdapter(getContext(), newsBookmarkListArray, BookmarkFragment.this);
                        rvBookmarkList.setAdapter(bookmarkListAdapter);
                        getBookmark(etSearch.getText().toString().trim().toLowerCase());
                    } else {
                        Toast.makeText(getContext(), "Enter something", Toast.LENGTH_SHORT).show();
                        // getBookmark("");
                        //newsBookmarkListArray = dbQueries.readBookmark();
                    }

                    return true;
                }

                return false;
            }
        });


        rvBookmarkList.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                int pastVisibleItems = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                if (pastVisibleItems == 0) {
                    rtPageTop.setVisibility(View.GONE);
                }

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if (dy > 0) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rvBookmarkList.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    rtPageTop.setVisibility(View.VISIBLE);
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
            /*      if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            //  if (offset < 1) {
                            loading = false;
                            offset = offset + limit;
                            String lastId = String.valueOf(newsBookmarkListArray.get(newsBookmarkListArray.size() - 1).getMid());
                            getNewsFeed(lastId, "");

                            //  }
                        }
                    }*/
                }else {
                    // Scrolling down
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rvBookmarkList.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

                }
            }
        });

       /* bookmarkListAdapter.setOnClickListen(new BookmarkListAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Fragment newsDetailFragment = new NewsDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("pageStatus", "2");
                bundle.putString("newsId", newsBookmarkListArray.get(position).getMid());
                bundle.putInt("position", position);
                bundle.putStringArrayList("DetailIdList", newsBookmarkIdList);
                newsDetailFragment.setArguments(bundle);
                ((HomeActivity) getContext()).loadFragment(newsDetailFragment);
            }
        });

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.ACTION_STATE_IDLE,
                ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                Fragment newsDetailFragment = new NewsDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("pageStatus", "2");
                bundle.putString("newsId", newsBookmarkListArray.get(viewHolder.getAdapterPosition()).getMid());
                bundle.putInt("position", viewHolder.getAdapterPosition());
                bundle.putStringArrayList("DetailIdList", newsBookmarkIdList);
                newsDetailFragment.setArguments(bundle);
                ((HomeActivity) getContext()).loadFragment(newsDetailFragment);
            }
        });
        itemTouchHelper.attachToRecyclerView(rvBookmarkList);*/

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        newsBookmarkListArray.clear();
                        newsBookmarkIdList.clear();
                        rvBookmarkList.setAdapter(null);
                        bookmarkListAdapter = new BookmarkListAdapter(getContext(), newsBookmarkListArray, BookmarkFragment.this);
                        rvBookmarkList.setAdapter(bookmarkListAdapter);
                        etSearch.setText("");
                        search = true;
                        rtSearch.setVisibility(View.GONE);
                        HomeActivity.rtSearch.setVisibility(View.VISIBLE);
                        getBookmark("");
                        swipeRefresh.setRefreshing(false);
                    }
                }, 0);
            }
        });

    }

    private class EmojiExcludeFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                int type = Character.getType(source.charAt(i));
                if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                    return "";
                }
            }
            return null;
        }
    }

    /* private void getBookmarkDb() {
         newsBookmarkListArray = dbQueries.readBookmark();
         newsBookmarkIdList = dbQueries.readBookmarkId();
         System.out.println("bookmarklist " + newsBookmarkListArray.toString());
         System.out.println("bookmarklist " + newsBookmarkIdList.toString());

         bookmarkListAdapter = new BookmarkListAdapter(getContext(), newsBookmarkListArray, BookmarkFragment.this);
         rvBookmarkList.setAdapter(bookmarkListAdapter);

         showProgressDialog();

         if (newsBookmarkListArray.size() > 0) {
             rvBookmarkList.setVisibility(View.VISIBLE);
             tvNoBookmarkList.setVisibility(View.GONE);
             bookmarkListAdapter.notifyDataSetChanged();
             mDialog.dismiss();

         } else {
             rvBookmarkList.setVisibility(View.GONE);
             tvNoBookmarkList.setVisibility(View.VISIBLE);
             mDialog.dismiss();

         }
     }
 */
    public void removeItemAtPosition(int position) {
        newsBookmarkListArray.remove(position);
        newsBookmarkIdList.remove(position);

        if (newsBookmarkListArray.size() > 0) {
            rvBookmarkList.setVisibility(View.VISIBLE);
            tvNoBookmarkList.setVisibility(View.GONE);
            bookmarkListAdapter.notifyDataSetChanged();

        } else {
            rvBookmarkList.setVisibility(View.GONE);
            tvNoBookmarkList.setVisibility(View.VISIBLE);
        }
    }

    private Uri createShareUri(String newsId) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http") // "http"
                .authority("hipsto.global") // "365salads.xyz"
                .appendPath("hipsto_orderfox") // "salads"
                .appendQueryParameter("news", newsId);
        return builder.build();
    }

    private Uri createDynamicUri(Uri myuri) {
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(myuri)
                //.setDynamicLinkDomain("hipsto.page.link")
                .setDynamicLinkDomain("orderfox.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
//                .setIosParameters(DynamicLink.IosParameters.Builder("ibi").build())
                .buildDynamicLink();
        return dynamicLink.getUri();
    }

    //ShareFunction
    public void ShareClickListener(final String mid, final String dataTitle, final ImageView ivShare) {
      /*  rtShareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtShare.setVisibility(View.VISIBLE);
            }
        });*/

        rtShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtShare.setVisibility(View.GONE);
            }
        });

        rtWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(mid));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle + getResources().getString(R.string.share_text) + System.lineSeparator() + shortLink;
                                    Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                                    whatsappIntent.setType("text/plain");
                                    whatsappIntent.setPackage("com.whatsapp");
                                    whatsappIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    whatsappIntent.putExtra(Intent.EXTRA_TEXT, msg);
                                    try {
                                        startActivityForResult(whatsappIntent, 1);
                                        if (!dbQueries.readShareId().contains(mid)) {
                                            dbQueries.insertShareId(mid);
                                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                            Bundle bun = new Bundle();
                                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with whatsapp - News Detail");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                            rtShare.setVisibility(View.GONE);
                                        }

                                    } catch (ActivityNotFoundException ex) {
                                        Toast.makeText(getActivity(), "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });
            }
        });

        rtMoreOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtShare.setVisibility(View.GONE);
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(mid));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();

                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle + getResources().getString(R.string.share_text)+ System.lineSeparator() +  shortLink;
                                    Intent share = new Intent(Intent.ACTION_SEND);
                                    share.setType("text/plain");
                                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    share.putExtra(Intent.EXTRA_TEXT, msg);
                                    startActivityForResult(Intent.createChooser(share, "Share link!"), 1);
                                    if (!dbQueries.readShareId().contains(mid)) {
                                        dbQueries.insertShareId(mid);
                                        ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                        Bundle bun = new Bundle();
                                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with other - News Detail");
                                        bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                        rtShare.setVisibility(View.GONE);
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });
            }
        });

        rtFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(mid));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle + getResources().getString(R.string.share_text)+ System.lineSeparator() +  shortLink;
                                    Intent fbIntent = new Intent(Intent.ACTION_SEND);
                                    fbIntent.setType("text/plain");
                                    fbIntent.setPackage("com.facebook.orca");
                                    fbIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    fbIntent.putExtra(Intent.EXTRA_TEXT, msg);
                                    try {
                                        startActivityForResult(fbIntent, 1);
                                        if (!dbQueries.readShareId().contains(mid)) {
                                            dbQueries.insertShareId(mid);
                                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                            Bundle bun = new Bundle();
                                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with Facebook - News Detail");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                            rtShare.setVisibility(View.GONE);
                                        }
                                    } catch (ActivityNotFoundException ex) {
                                        Toast.makeText(getActivity(), "Facebook have not been installed.", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });

            }
        });

        rtTelegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(mid));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle +getResources().getString(R.string.share_text) + System.lineSeparator() +  shortLink;
                                    Intent telegramIntent = new Intent(Intent.ACTION_SEND);
                                    telegramIntent.setType("text/plain");
                                    telegramIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    telegramIntent.setPackage("org.telegram.messenger");
                                    telegramIntent.putExtra(Intent.EXTRA_TEXT, msg);
                                    try {
                                        startActivityForResult(telegramIntent, 1);
                                        if (!dbQueries.readShareId().contains(mid)) {
                                            dbQueries.insertShareId(mid);
                                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                            Bundle bun = new Bundle();
                                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with Telegram - News Detail");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                            rtShare.setVisibility(View.GONE);
                                        }
                                    } catch (ActivityNotFoundException ex) {
                                        Toast.makeText(getActivity(), "Telegram have not been installed.", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });
            }
        });

    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void getBookmark(String Search) {
        showProgressDialog();

        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rvBookmarkList.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

        loading = false;
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        String user = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

        hipstoAPI.readBookmark(authToken, user.trim(), Search).
                enqueue(new Callback<NewsFeedResponse>() {
                    @Override
                    public void onResponse(Call<NewsFeedResponse> call, Response<NewsFeedResponse> response) {
                        try {
                            if (response.body().isSuccess() || !response.body().getItems().isEmpty()) {
                                feedResponse.setItems(response.body().getItems());
                                newsBookmarkListArray.clear();
                                newsBookmarkIdList.clear();
                                newsBookmarkListArray.addAll(feedResponse.getItems());
                                for (int i = 0; i < feedResponse.getItems().size(); i++) {
                                    newsBookmarkIdList.add(feedResponse.getItems().get(i).getMid());
                                    // newsBookmarkListArray.add(feedResponse.getItems().get(i));
                                }
                                loading = true;

                                if (newsBookmarkListArray.size() > 0) {
                                    rvBookmarkList.setVisibility(View.VISIBLE);
                                    tvNoBookmarkList.setVisibility(View.GONE);
                                    bookmarkListAdapter.notifyDataSetChanged();
                                    mDialog.dismiss();

                                } else {
                                    rvBookmarkList.setVisibility(View.GONE);
                                    tvNoBookmarkList.setVisibility(View.VISIBLE);
                                    mDialog.dismiss();
                                }

                            } else {
                                loading = true;
                                mDialog.dismiss();
                                rvBookmarkList.setVisibility(View.GONE);
                                tvNoBookmarkList.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            //loading = true;
                            mDialog.dismiss();
                            e.printStackTrace();
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.no_record), Toast.LENGTH_LONG).show();
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<NewsFeedResponse> call, Throwable t) {
                        mDialog.dismiss();
                        Toast.makeText(getActivity(), getResources().getString(R.string.failed) + "--" + t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

    }

}
