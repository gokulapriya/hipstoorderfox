package hipsto.app.orderfox.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.gson.JsonElement;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.BindView;
import hipsto.app.orderfox.R;
import hipsto.app.orderfox.Sql.DBQueries;
import hipsto.app.orderfox.activity.HomeActivity;
import hipsto.app.orderfox.activity.WebActivity;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.models.Bookmark;
import hipsto.app.orderfox.models.NewsData;
import hipsto.app.orderfox.models.NewsDetailResponse;
import hipsto.app.orderfox.models.Rating;
import hipsto.app.orderfox.models.TrendingResponse;
import hipsto.app.orderfox.retrofit.HipstoAPI;
import hipsto.app.orderfox.utils.Constants;
import hipsto.app.orderfox.utils.MySpannable;
import hipsto.app.orderfox.utils.OnSwipeTouchListener;
import hipsto.app.orderfox.utils.Utilsdefault;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

/**
 * Created by Gokulapriya on 6/28/2019.
 */

public class NewsDetailFragment extends BaseFragment implements Serializable {

    @BindView(R.id.vp_new_list)
    ViewPager vpNewList;
    @BindView(R.id.tv_no_rc_news_list)
    TextView tvNoNewsList;
    @BindView(R.id.tv_rate)
    TextView tvRate;
    @BindView(R.id.tv_news_topic)
    TextView mTitle;
    @BindView(R.id.tv_news_email)
    TextView mDomain;
    @BindView(R.id.tv_email)
    TextView mDomainLink;
    RatingBar ratingBar;
    RatingBar rbRateTheNews;
    @BindView(R.id.tv_date)
    TextView mDate;
    @BindView(R.id.tv_time)
    TextView mTime;
    @BindView(R.id.tv_news_description)
    TextView mDescription;
    @BindView(R.id.iv_news_img)
    ImageView newsImage;
    @BindView(R.id.tv_getty)
    TextView mTrend;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public HipstoAPI hipstoAPI;
    ProgressDialog mDialog;
    Dialog dialogRate;
    RelativeLayout rtDetails;
    ScrollView sv_details;

    String newsId;
    String newsUrlId;
    String pageStatus;
    int newsPostion = 0;
    int entryPostion = 0;
    private ArrayList<String> DetailResponseList = new ArrayList<>();
    private ArrayList<TrendingResponse> TrendingDetailList = new ArrayList<>();
    View headernews;
    ImageView ivBookmark, ivShare;
    RelativeLayout rtBookmark, rtShareIcon, rtShare, rtWhatapp, rtFb, rtTelegram, rtMoreOption;
    List<String> bookmarkList = new ArrayList<>();
    List<Rating> rateList = new ArrayList<>();
    Button buttonReadMore;
    String topicCode = "", mid = "", dataImageMain = "", itemUrl = "", dataTitle = "",
            dataContent = "", dataContentFull = "", domain = "", url = "", dataPublished = "", sourceName = "", sourceLang = "", hipstoOwnBookmarkId = "", timeRead = "";
    Integer hipstoOwnRating = 0, hipstoOwnViews = 0;
    DBQueries dbQueries;
    private FirebaseAnalytics mFirebaseAnalytics;
    int ratingStatus = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_news_detail, container, false);

        HipstoApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        initViews(v);
        performClick(v);
        ShareClickListener();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (sharedPreferences.getString("InterstitialAdsShown", "").equals("false")) {
          /*  Appodeal.setTesting(false);
            Appodeal.setLogLevel(com.appodeal.ads.utils.Log.LogLevel.debug);
            Appodeal.getUserSettings(getContext())
                    .setAge(25)
                    .setGender(UserSettings.Gender.OTHER);
            Appodeal.initialize((Activity) getContext(), "b878181640b3a5d461f4a658379295af81373a42c059f30d", Appodeal.INTERSTITIAL, true);
            Appodeal.setAutoCache(Appodeal.INTERSTITIAL, true);*/
          /*  Appodeal.setInterstitialCallbacks(new AppodealInterstitialCallbacks(getActivity()));
            System.out.println("New Ads interstitial 1" + sharedPreferences.getString("InterstitialAdsShown", ""));
            Appodeal.setTriggerOnLoadedOnPrecache(Appodeal.INTERSTITIAL, false);
            if (Appodeal.isLoaded(Appodeal.INTERSTITIAL)) {
                System.out.println("INTERSTITIAL isLoaded --> True");
            } else {
                System.out.println("INTERSTITIAL isLoaded --> False");
            }
            if (Appodeal.isPrecache(Appodeal.INTERSTITIAL)) {
                System.out.println("INTERSTITIAL isPrecache --> True");
            } else {
                System.out.println("INTERSTITIAL isPrecache --> False");
            }*/

            System.out.println("New Ads interstitial");
            final Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 20 seconds
                    boolean isShown = Appodeal.show(getActivity(), Appodeal.INTERSTITIAL);
                    editor.putString("InterstitialAdsShown", "").apply();
                    System.out.println("INTERSTITIAL show -->"+ String.valueOf(isShown));
                    //Toast.makeText(getContext(), "Interstitial " + String.valueOf(isShown), Toast.LENGTH_SHORT).show();
                }
            }, 0);  //the time is in milisecond
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViews(View v) {

        vpNewList = v.findViewById(R.id.vp_new_list);
        tvNoNewsList = v.findViewById(R.id.tv_no_rc_news_list);
        tvRate = v.findViewById(R.id.tv_rate);
        mTitle = v.findViewById(R.id.tv_news_topic);
        mDomain = v.findViewById(R.id.tv_news_email);
        mDomainLink = v.findViewById(R.id.tv_email);
        ratingBar = v.findViewById(R.id.rb);
        rbRateTheNews = v.findViewById(R.id.rb_rate_the_news);
        mDate = v.findViewById(R.id.tv_date);
        mTime = v.findViewById(R.id.tv_time);
        mDescription = v.findViewById(R.id.tv_news_description);
        newsImage = v.findViewById(R.id.iv_news_img);
        mTrend = v.findViewById(R.id.tv_getty);
        buttonReadMore = v.findViewById(R.id.btn_read_more);
        rtShareIcon = (RelativeLayout) v.findViewById(R.id.rt_share_icon);
        rtShare = (RelativeLayout) v.findViewById(R.id.rt_share);
        rtBookmark = (RelativeLayout) v.findViewById(R.id.rt_bookmark_icon);
        ivBookmark = (ImageView) v.findViewById(R.id.iv_bookmark);
        ivShare = (ImageView) v.findViewById(R.id.iv_share);
        rtWhatapp = (RelativeLayout) v.findViewById(R.id.rt_whatapp);
        rtFb = (RelativeLayout) v.findViewById(R.id.rt_fb);
        rtTelegram = (RelativeLayout) v.findViewById(R.id.rt_telegram);
        rtMoreOption = (RelativeLayout) v.findViewById(R.id.rt_more);
        rtDetails = (RelativeLayout) v.findViewById(R.id.rt_details);
        sv_details = (ScrollView) v.findViewById(R.id.sv_details);
        headernews = v.findViewById(R.id.vw_news);

        rtBookmark.setEnabled(false);

        HomeActivity.rt_navigationView.setVisibility(View.VISIBLE);
        HomeActivity.toolbar.setVisibility(View.VISIBLE);
        HomeActivity.rt_bookmark.setVisibility(View.VISIBLE);
        HomeActivity.rtSetting.setVisibility(View.VISIBLE);
        HomeActivity.rtSearch.setVisibility(View.INVISIBLE);
        HomeActivity.ivLogo.setEnabled(true);
        editor.putInt(Constants.LOADFRAGMENT,0).apply();

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        dbQueries = new DBQueries(getActivity());
        dbQueries.open(getActivity());

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            pageStatus = bundle.getString("pageStatus");
            assert pageStatus != null;
            switch (pageStatus) {
                case "1":
                    newsUrlId = bundle.getString("newsId");
                    newsPostion = bundle.getInt("position");
                    entryPostion = bundle.getInt("position");
                    DetailResponseList = bundle.getStringArrayList("DetailIdList");
                    //TrendingDetailList = (ArrayList<TrendingResponse>) bundle.getSerializable("trendingList");
                    break;
                case "2":
                    newsId = bundle.getString("newsId");
                    newsPostion = bundle.getInt("position");
                    entryPostion = bundle.getInt("position");
                    DetailResponseList = bundle.getStringArrayList("DetailIdList");

                    break;
                case "share":
                    newsId = bundle.getString("newsId");
                    if (Utilsdefault.isNetworkAvailable()) {
                        newsDetail("", newsId, ratingStatus, "");
                    } else {
                        Utilsdefault.alertNetwork(getActivity());
                    }
                    break;
            }
        }

        Appodeal.setBannerViewId(R.id.appodealBannerView);
        boolean isShown = Appodeal.show(getActivity(), Appodeal.BANNER_VIEW);
        System.out.println("isShow --> " + isShown);

        tvRate.setText(getResources().getString(R.string.rate_news));
        rbRateTheNews.setRating(0);

        //NewDetails
        if (Utilsdefault.isNetworkAvailable()) {
            assert DetailResponseList != null;
            assert TrendingDetailList != null;
            if (DetailResponseList.size() > 0) {
                Log.d("News Details->", "News Details->" + newsId + "-" + newsPostion + "-" + DetailResponseList.size() + "-" + DetailResponseList);
                rtBookmark.setVisibility(View.VISIBLE);
                rtShareIcon.setVisibility(View.VISIBLE);
                tvRate.setVisibility(View.VISIBLE);
                rbRateTheNews.setVisibility(View.VISIBLE);
                if (pageStatus.equals("1")) {
                    loadNewsDetails(newsPostion, newsUrlId, newsId, DetailResponseList, 1, "Trend");
                } else if (pageStatus.equals("2")) {
                    loadNewsDetails(newsPostion, newsUrlId, newsId, DetailResponseList, 1, "News");
                }


            } /*else if (TrendingDetailList.size() > 0) {

                Log.d("News Details->", "News Details->" + newsId + "-" + newsPostion + "-" + TrendingDetailList.size() + "-" + TrendingDetailList);
                rtBookmark.setVisibility(View.INVISIBLE);
                rtShareIcon.setVisibility(View.INVISIBLE);
                tvRate.setVisibility(View.GONE);
                rbRateTheNews.setVisibility(View.GONE);
                loadTrendNewsDetails(newsPostion, newsId, TrendingDetailList, 1);
            }*/
        } else {
            Utilsdefault.alertNetwork(getActivity());
        }


    }

    //NewDetails
    private void loadTrendNewsDetails(int Position, String Id, ArrayList<TrendingResponse> newsIdList, int status) {

        if (status == 1) {

            newsId = String.valueOf(newsIdList.get(Position).getId());
            newsPostion = Position;
            TrendingDetailList = newsIdList;

            //NewsFeedDetails
            if (Utilsdefault.isNetworkAvailable()) {
                newsTrendDetail(Position);
            } else {
                Utilsdefault.alertNetwork(getActivity());
            }

        } else if (status == 2) {

            int pos = Position - 1;

            newsId = String.valueOf(newsIdList.get(pos).getId());
            newsPostion = pos;
            TrendingDetailList = newsIdList;

            //NewsFeedDetails
            if (Utilsdefault.isNetworkAvailable()) {
                newsTrendDetail(pos);
            } else {
                Utilsdefault.alertNetwork(getActivity());
            }
        } else if (status == 3) {
            int pos = Position + 1;

            newsId = String.valueOf(newsIdList.get(pos).getId());
            newsPostion = pos;
            TrendingDetailList = newsIdList;

            //NewsFeedDetails
            if (Utilsdefault.isNetworkAvailable()) {
                newsTrendDetail(pos);
            } else {
                Utilsdefault.alertNetwork(getActivity());
            }
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private void newsTrendDetail(final Integer newsPostion) {
   /*Firebase Event log trend*/
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle bun = new Bundle();
        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "News Detail - Trending");
        bun.putString(FirebaseAnalytics.Param.CONTENT, TrendingDetailList.get(newsPostion).getTitle().trim());
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);


        Glide.with(getContext()).load(TrendingDetailList.get(newsPostion).getImage()).into(newsImage);
        mTitle.setText(Html.fromHtml(Html.fromHtml(TrendingDetailList.get(newsPostion).getTitle().trim()).toString()));
        mDomain.setText("@" + TrendingDetailList.get(newsPostion).getDomain());
        mDomainLink.setText("@" + TrendingDetailList.get(newsPostion).getDomain());
        mDescription.setText(Html.fromHtml(Html.fromHtml(Jsoup.parse(TrendingDetailList.get(newsPostion).getDescription()).text()).toString().trim()));
        String date = TrendingDetailList.get(newsPostion).getFound();
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat parser1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        parser.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsed = null;
        Date parsed1 = null;
        try {
            parsed = parser.parse(date);
            parsed1 = parser1.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        parser = new SimpleDateFormat("dd MMM");
        parser1 = new SimpleDateFormat("HH:mm");

        mDate.setText(parser.format(parsed));
        mTime.setText(parser1.format(parsed1));
        ratingBar.setRating(0);

        buttonReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("uri", TrendingDetailList.get(newsPostion).getDomain());
                bundle.putString("title", TrendingDetailList.get(newsPostion).getTitle());
                bundle.putString("resource", TrendingDetailList.get(newsPostion).getUrl());
                bundle.putString("newsId", String.valueOf(TrendingDetailList.get(newsPostion).getId()));
                Intent i = new Intent(getContext(), WebActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtras(bundle);
                startActivity(i);

            }
        });

        mDomainLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("uri", TrendingDetailList.get(newsPostion).getDomain());
                bundle.putString("title", TrendingDetailList.get(newsPostion).getTitle());
                bundle.putString("resource", TrendingDetailList.get(newsPostion).getUrl());
                bundle.putString("newsId", String.valueOf(TrendingDetailList.get(newsPostion).getId()));
                Intent i = new Intent(getContext(), WebActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        sv_details.setOnTouchListener(new OnSwipeTouchListener(getContext()) {

            public void onSwipeRight() {
                if (entryPostion == (newsPostion)) {
                    getActivity().onBackPressed();
                } else {
                    loadTrendNewsDetails(newsPostion, newsId, TrendingDetailList, 2);
                }
            }

            public void onSwipeLeft() {
                if (TrendingDetailList.size() == (newsPostion + 1)) {
                    final Dialog dialog = new Dialog(getContext());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_rating);

                    final LinearLayout ltRating = dialog.findViewById(R.id.lt_rating);
                    final LinearLayout ltAlert = dialog.findViewById(R.id.lt_alert);
                    final TextView tvAlertMsg = dialog.findViewById(R.id.tv_alert_msg);
                    TextView tvOkay = dialog.findViewById(R.id.tv_okay);

                    ltAlert.setVisibility(View.VISIBLE);
                    ltRating.setVisibility(View.GONE);
                    tvAlertMsg.setText(getActivity().getResources().getString(R.string.back_press_newsdetail));

                    dialog.show();

                    tvOkay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            getActivity().onBackPressed();
                        }
                    });

                } else {
                    loadTrendNewsDetails(newsPostion, newsId, TrendingDetailList, 3);
                }
            }
        });
    }


    private void loadNewsDetails(int Position, String urlId, String Id, ArrayList<String> newsIdList, int status, String sourcefrom) {

        if (status == 1) {

            if (sourcefrom.equals("News")) {
                //Local Db Read/Unread Status
                if (dbQueries.readNewsFeedId().toString().contains(Id)) {
                    Log.e("Already in db", "-" + Id);
                } else {
                    dbQueries.insertNewsFeedId(Id);
                }
            }

            newsUrlId = urlId;
            newsId = Id;
            newsPostion = Position;
            DetailResponseList = newsIdList;

            //NewsFeedDetails
            if (Utilsdefault.isNetworkAvailable()) {
                newsDetail(urlId, Id, status, sourcefrom);
            } else {
                Utilsdefault.alertNetwork(getActivity());
            }

        } else if (status == 2) {

            int pos = Position - 1;

            if (sourcefrom.equals("News")) {
                //Local Db Read/Unread Status
                if (dbQueries.readNewsFeedId().toString().contains(newsIdList.get(pos))) {
                    Log.e("Already in db", "-" + newsIdList.get(pos));
                } else {
                    dbQueries.insertNewsFeedId(newsIdList.get(pos));
                }
            }
            newsUrlId = newsIdList.get(pos);
            newsId = newsIdList.get(pos);
            newsPostion = pos;
            DetailResponseList = newsIdList;

            //NewsFeedDetails
            if (Utilsdefault.isNetworkAvailable()) {
                newsDetail(newsIdList.get(pos), newsIdList.get(pos), status, sourcefrom);
            } else {
                Utilsdefault.alertNetwork(getActivity());
            }
        } else if (status == 3) {
            int pos = Position + 1;

            if (sourcefrom.equals("News")) {
                //Local Db Read/Unread Status
                if (dbQueries.readNewsFeedId().toString().contains(newsIdList.get(pos))) {
                    Log.e("Already in db", "-" + newsIdList.get(pos));
                } else {
                    dbQueries.insertNewsFeedId(newsIdList.get(pos));
                }
            }

            newsUrlId = newsIdList.get(pos);
            newsId = newsIdList.get(pos);
            newsPostion = pos;
            DetailResponseList = newsIdList;

            //NewsFeedDetails
            if (Utilsdefault.isNetworkAvailable()) {
                newsDetail(newsIdList.get(pos), newsIdList.get(pos), status, sourcefrom);
            } else {
                Utilsdefault.alertNetwork(getActivity());
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void performClick(View v) {

        HomeActivity.ivLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                fragment = new NewsFragment();
                ((HomeActivity) getContext()).loadFragment(fragment);
            }
        });


        rbRateTheNews.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dialogRateTheNews();
                return false;
            }
        });

    }

    private void insertBookmark(final Bookmark bookmark) {

        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

        hipstoAPI.addBookmark(authToken, userEncode.trim(), bookmark.getMid(), bookmark.getMid()).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    Boolean status = jsonObject.getBoolean("success");
                    if (status) {
                        dbQueries.insertBookmark(bookmark);
                        dbQueries.insertBookmarkId(bookmark.getMid());
                        ivBookmark.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_bookmark_blue_shape));
                        // mDialog.dismiss();
                        Toast.makeText(getActivity(), getResources().getString(R.string.bookmark_add_success), Toast.LENGTH_LONG).show();
                        /*Firebase Event log trend*/
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                        Bundle bun = new Bundle();
                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Bookmark_add - News Detail");
                        bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);

                        newsDetail(newsUrlId, newsId, ratingStatus, "");


                    } else {
                        Toast.makeText(getActivity(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                if (getActivity() != null) {
                    Toast.makeText(getActivity(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void removeBookmark(final Bookmark bookmark) {
        //  showProgressDialog();
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);


        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.HIPSTO_BASE_URL)
                .build();

        HipstoAPI service = retrofit.create(HipstoAPI.class);

        Call<JsonElement> call = service.removeBookmark(authToken, userEncode.trim(), bookmark.getMid());

        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    Boolean status = jsonObject.getBoolean("success");
                    if (status) {
                        dbQueries.deleteBookmarkId(bookmark.getMid());
                        dbQueries.deleteBookmark(bookmark);
                        ivBookmark.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_bookmark_gray_shape));
                        //mDialog.dismiss();
                        Toast.makeText(getContext(), getResources().getString(R.string.bookmark_add_remove), Toast.LENGTH_LONG).show();

                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                        Bundle bun = new Bundle();
                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Bookmark_remove - News Detail");
                        bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);

                        newsDetail(newsUrlId, newsId, ratingStatus, "");
                    } else {
                        // mDialog.dismiss();
                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (getContext() != null) {
                        // mDialog.dismiss();
                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                if (getContext() != null) {
                    //  mDialog.dismiss();
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                }
            }

        });
    }

    private Uri createShareUri(String newsId) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http") // "http"
                .authority("hipsto.global") // "365salads.xyz"
                .appendPath("hipsto_orderfox") // "salads"
                .appendQueryParameter("news", newsId);
        return builder.build();
    }

    private Uri createDynamicUri(Uri myuri) {
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(myuri)
                //.setDynamicLinkDomain("hipsto.page.link")
                .setDynamicLinkDomain("orderfox.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
//                .setIosParameters(DynamicLink.IosParameters.Builder("ibi").build())
                .buildDynamicLink();
        return dynamicLink.getUri();
    }


    private void ShareClickListener() {
        rtShareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtShare.setVisibility(View.VISIBLE);
            }
        });

        rtShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtShare.setVisibility(View.GONE);
            }
        });

        rtWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(newsId));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle + getResources().getString(R.string.share_text) + System.lineSeparator() +  shortLink;
                                    Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                                    whatsappIntent.setType("text/plain");
                                    whatsappIntent.setPackage("com.whatsapp");
                                    whatsappIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    whatsappIntent.putExtra(Intent.EXTRA_TEXT, msg);
                                    try {
                                        startActivityForResult(whatsappIntent, 1);
                                        if (!dbQueries.readShareId().contains(mid)) {
                                            dbQueries.insertShareId(mid);
                                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                            Bundle bun = new Bundle();
                                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with whatsapp - News Detail");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                            rtShare.setVisibility(View.GONE);
                                        }

                                    } catch (ActivityNotFoundException ex) {
                                        Toast.makeText(getActivity(), "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });

               /* Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.whatsapp");
                whatsappIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, dataTitle + ". Download HIPSTO ORDERFOX Now : " + link);

                try {
                    startActivityForResult(whatsappIntent, 1);
                    if (!dbQueries.readShareId().contains(mid)) {
                        dbQueries.insertShareId(mid);
                        ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                        Bundle bun = new Bundle();
                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with whatsapp - News Detail");
                        bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                    }

                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
                }*/
            }
        });

        rtMoreOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                rtShare.setVisibility(View.GONE);
                Uri shortlink = createDynamicUri(createShareUri(newsId));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle + getResources().getString(R.string.share_text) + System.lineSeparator() +  shortLink;
                                    Intent share = new Intent(Intent.ACTION_SEND);
                                    share.setType("text/plain");
                                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    share.putExtra(Intent.EXTRA_TEXT, msg);
                                    startActivityForResult(Intent.createChooser(share, "Share link!"), 1);
                                    if (!dbQueries.readShareId().contains(mid)) {
                                        dbQueries.insertShareId(mid);
                                        ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                        Bundle bun = new Bundle();
                                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with other - News Detail");
                                        bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                        rtShare.setVisibility(View.GONE);
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });

              /*  rtShare.setVisibility(View.GONE);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_TEXT, dataTitle + ". Download HIPSTO ORDERFOX Now : http://" + getString(R.string.app_deep_link) + "news/" + mid);

                *//*if (fData.getAttachmentsList() != null) {*//*
                // share.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_feeds_path) + "/" + fData.getAttachmentsList().get(0));
                *//* }*//*
                startActivityForResult(Intent.createChooser(share, "Share link!"), 1);
                if (!dbQueries.readShareId().contains(mid)) {
                    dbQueries.insertShareId(mid);
                    ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                    mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                    Bundle bun = new Bundle();
                    bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                    bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                    bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with other - News Detail");
                    bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                }*/
            }
        });

        rtFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(newsId));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle +getResources().getString(R.string.share_text) + System.lineSeparator() +  shortLink;
                                    Intent fbIntent = new Intent(Intent.ACTION_SEND);
                                    fbIntent.setType("text/plain");
                                    fbIntent.setPackage("com.facebook.orca");
                                    fbIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    fbIntent.putExtra(Intent.EXTRA_TEXT, msg);
                                    try {
                                        startActivityForResult(fbIntent, 1);
                                        if (!dbQueries.readShareId().contains(mid)) {
                                            dbQueries.insertShareId(mid);
                                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                            Bundle bun = new Bundle();
                                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with Facebook - News Detail");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                            rtShare.setVisibility(View.GONE);
                                        }
                                    } catch (ActivityNotFoundException ex) {
                                        Toast.makeText(getActivity(), "Facebook have not been installed.", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });

              /*  Intent fbIntent = new Intent(Intent.ACTION_SEND);
                fbIntent.setType("text/plain");
                fbIntent.setPackage("com.facebook.orca");
                fbIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                fbIntent.putExtra(Intent.EXTRA_TEXT, dataTitle + ". Download HIPSTO ORDERFOX Now : http://" + getString(R.string.app_deep_link) + "news/" + mid);
                try {
                    startActivityForResult(fbIntent, 1);
                    if (!dbQueries.readShareId().contains(mid)) {
                        dbQueries.insertShareId(mid);
                        ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                        Bundle bun = new Bundle();
                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with Facebook - News Detail");
                        bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                    }
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Facebook have not been installed.", Toast.LENGTH_LONG).show();
                }*/
            }
        });

        rtTelegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(newsId));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle + getResources().getString(R.string.share_text) + System.lineSeparator() +  shortLink;
                                    Intent telegramIntent = new Intent(Intent.ACTION_SEND);
                                    telegramIntent.setType("text/plain");
                                    telegramIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    telegramIntent.setPackage("org.telegram.messenger");
                                    telegramIntent.putExtra(Intent.EXTRA_TEXT, msg);
                                    try {
                                        startActivityForResult(telegramIntent, 1);
                                        if (!dbQueries.readShareId().contains(mid)) {
                                            dbQueries.insertShareId(mid);
                                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                            Bundle bun = new Bundle();
                                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with Telegram - News Detail");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                            rtShare.setVisibility(View.GONE);
                                        }
                                    } catch (ActivityNotFoundException ex) {
                                        Toast.makeText(getActivity(), "Telegram have not been installed.", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });
               /* Intent telegramIntent = new Intent(Intent.ACTION_SEND);
                telegramIntent.setType("text/plain");
                telegramIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                telegramIntent.setPackage("org.telegram.messenger");
                telegramIntent.putExtra(Intent.EXTRA_TEXT, dataTitle + ". Download HIPSTO ORDERFOX Now : http://" + getString(R.string.app_deep_link) + "news/" + mid);

                try {
                    startActivityForResult(telegramIntent, 1);
                    if (!dbQueries.readShareId().contains(mid)) {
                        dbQueries.insertShareId(mid);
                        ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                        Bundle bun = new Bundle();
                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with Telegram - News Detail");
                        bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                    }
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Telegram have not been installed.", Toast.LENGTH_LONG).show();
                }*/
            }
        });

    }


    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void newsDetail(final String urlId, final String nsId, final int status, final String source) {
        showProgressDialog();
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        final String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();
        final String userId = /*sharedPreferences.getString(Constants.USERID, "")*/dbQueries.readLoginID(1).getUser_id();

        String NID = nsId;

        //Here a logging interceptor is created
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        //The logging interceptor will be added to the http client
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        //The Retrofit builder will have the client attached, in order to get connection logs
        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.HIPSTO_BASE_URL)
                .build();

        HipstoAPI service = retrofit.create(HipstoAPI.class);

        if (source.equals("Trend")) {
            NID = urlId;
        }

        Call<NewsDetailResponse> call = service.newsDetail(authToken, userEncode.trim(), NID);

        call.enqueue(new Callback<NewsDetailResponse>() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onResponse(Call<NewsDetailResponse> call, Response<NewsDetailResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        mDialog.dismiss();
                        rtBookmark.setEnabled(true);

                        NewsDetailResponse newsDetailResponse = new NewsDetailResponse();
                        newsDetailResponse = response.body();

                        topicCode = newsDetailResponse.getTopicCode();
                        mid = newsDetailResponse.getMid();
                        newsId = newsDetailResponse.getMid();
                        dataImageMain = newsDetailResponse.getDataImageMain();
                        itemUrl = newsDetailResponse.getItemUrl();
                        if (newsDetailResponse.getDataTitle() != null) {
                            dataTitle = newsDetailResponse.getDataTitle().trim();
                        } else {
                            dataTitle = "ORDERFOX news";
                        }

                        if (newsDetailResponse.getDataContent() != null) {
                            dataContent = newsDetailResponse.getDataContent().trim();
                        } else {
                            dataContent = "ORDERFOX news";
                        }

                        if (newsDetailResponse.getSource().getDomain() != null) {
                            domain = newsDetailResponse.getSource().getDomain();
                            mDomain.setText("@" + domain);
                            mDomainLink.setText("@" + domain);
                        } else {
                            domain = "";
                        }

                        dataContentFull = newsDetailResponse.getDataContentFull();
                        url = newsDetailResponse.getSource().getUrl();
                        dataPublished = newsDetailResponse.getDataPublished();
                        sourceName = newsDetailResponse.getSourceName();
                        sourceLang = newsDetailResponse.getSourceLang();

                        if (!newsDetailResponse.getHipsto_own_bookmark_id().equals("")) {
                            hipstoOwnBookmarkId = newsDetailResponse.getHipsto_own_bookmark_id();
                        } else {
                            hipstoOwnBookmarkId = "";
                        }

                        timeRead = newsDetailResponse.getTimeRead();
                        hipstoOwnRating = Integer.valueOf(newsDetailResponse.getHipstoOwnRating());
                        hipstoOwnViews = Integer.valueOf(newsDetailResponse.getHipstoOwnViews());

                        if (!newsDetailResponse.getHipsto_own_bookmark_id().equals("")) {
                            ivBookmark.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_bookmark_blue_shape));
                        } else {
                            ivBookmark.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_bookmark_gray_shape));
                        }

                        //DB Bookmark
                        if (dbQueries.readBookmarkId().contains(mid)) {
                            ivBookmark.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_bookmark_blue_shape));
                        } else {
                            ivBookmark.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_bookmark_gray_shape));
                        }

                        //DB Read/Unread Status
                        if (source.equals("Trend")) {
                            if (dbQueries.readNewsFeedId().toString().contains(mid)) {
                                Log.e("Already in db", "-" + mid);
                            } else {
                                dbQueries.insertNewsFeedId(mid);
                            }
                        }

                        Glide.with(getActivity()).load(newsDetailResponse.getDataImagePreview()).apply(RequestOptions.placeholderOf(R.mipmap.placeholder_detail)).into(newsImage);
                        mTitle.setText(dataTitle);
                        dataContent = Jsoup.parse(dataContent).text();
                        mDescription.setText(Html.fromHtml(Html.fromHtml(dataContent).toString().trim()));
                        String date = newsDetailResponse.getDataPublished();
                        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                        parser.setTimeZone(TimeZone.getTimeZone("UTC"));
                        Date parsed = null;
                        try {
                            parsed = parser.parse(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        parser = new SimpleDateFormat("dd MMM");
                        date = parser.format(parsed);
                        System.out.println(date);
                        mDate.setText(parser.format(parsed));
                        mTime.setText(newsDetailResponse.getTimeRead());
                        ratingBar.setRating(newsDetailResponse.getHipstoOwnRating());
                        mTrend.setText(newsDetailResponse.getSourceName());


                        rateList = dbQueries.readRating();
                        for (int i = 0; i < rateList.size(); i++) {
                            if (rateList.get(i).getNewsId().contains(mid)) {
                                ratingBar.setRating(rateList.get(i).getRating());
                                rbRateTheNews.setRating(rateList.get(i).getRating());
                                //  rbRateTheNews.setEnabled(false);
                                break;
                            }
                        }

                        //DB Share
                        System.out.println("share list " + dbQueries.readShareId().toString());
                        if (dbQueries.readShareId().contains(mid)) {
                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                        } else {
                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_grey));
                        }


                        buttonReadMore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle bundle = new Bundle();
                                bundle.putString("uri", itemUrl);
                                bundle.putString("title", dataTitle);
                                bundle.putString("resource", domain);
                                bundle.putString("newsId", newsId);
                                Intent i = new Intent(getContext(), WebActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.putExtras(bundle);
                                startActivity(i);
                            }
                        });

                        rtBookmark.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dbQueries.open(getActivity());
                                bookmarkList = dbQueries.readBookmarkId();
                                System.out.println(bookmarkList.toString());
                                Bookmark bookmark = new Bookmark(topicCode, mid, dataImageMain, itemUrl, dataTitle, dataContent,
                                        dataContentFull, domain, url, dataPublished, sourceName, sourceLang, hipstoOwnRating, hipstoOwnViews, hipstoOwnBookmarkId, timeRead);
                                if (!hipstoOwnBookmarkId.equals("") || bookmarkList.contains(mid)) {
                                    removeBookmark(bookmark);
                                } else if (hipstoOwnBookmarkId.equals("") && !bookmarkList.contains(mid)) {
                                    insertBookmark(bookmark);
                                }
                            }
                        });

                        sv_details.setOnTouchListener(new OnSwipeTouchListener(getContext()) {

                            public void onSwipeRight() {
                                if (entryPostion == (newsPostion)) {
                                    if(dialogRate != null){
                                        dialogRate.dismiss();
                                    }

                                    getActivity().onBackPressed();
                                } else {
                                    if(dialogRate != null){
                                        dialogRate.dismiss();
                                    }

                                    loadNewsDetails(newsPostion, urlId, mid, DetailResponseList, 2, source);
                                }
                            }

                            public void onSwipeLeft() {
                                if (DetailResponseList.size() == (newsPostion + 1)) {

                                    if(dialogRate != null){
                                        dialogRate.dismiss();
                                    }

                                    final Dialog dialog = new Dialog(getContext());
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setCancelable(false);
                                    dialog.setContentView(R.layout.dialog_rating);

                                    final LinearLayout ltRating = dialog.findViewById(R.id.lt_rating);
                                    final LinearLayout ltAlert = dialog.findViewById(R.id.lt_alert);
                                    final TextView tvAlertMsg = dialog.findViewById(R.id.tv_alert_msg);
                                    TextView tvOkay = dialog.findViewById(R.id.tv_okay);

                                    ltAlert.setVisibility(View.VISIBLE);
                                    ltRating.setVisibility(View.GONE);
                                    tvAlertMsg.setText(getActivity().getResources().getString(R.string.back_press_newsdetail));
                                    dialog.show();

                                    tvOkay.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                            getActivity().onBackPressed();
                                        }
                                    });

                                } else {
                                    if(dialogRate != null){
                                        dialogRate.dismiss();
                                    }

                                    loadNewsDetails(newsPostion, urlId, mid, DetailResponseList, 3, source);
                                }
                            }
                        });

                        mDomainLink.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle bundle = new Bundle();
                                bundle.putString("uri", itemUrl);
                                bundle.putString("title", dataTitle);
                                bundle.putString("resource", domain);
                                bundle.putString("newsId", newsId);
                                Intent i = new Intent(getContext(), WebActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.putExtras(bundle);
                                startActivity(i);
                            }
                        });
                        if (newsDetailResponse.getHipsto_own_ratings() != null) {
                            tvRate.setText(getResources().getString(R.string.your_rating));
                            rbRateTheNews.setRating(Float.parseFloat(newsDetailResponse.getHipsto_own_ratings()));
                        } else {
                            tvRate.setText(getResources().getString(R.string.rate_news));
                            rbRateTheNews.setRating(0);
                        }

                        if (status != 0) {
                            Rating rating = new Rating(userId, userEncode, mid, newsDetailResponse.getHipstoOwnRating());
                            if (dbQueries.readRatingId().contains(mid)) {
                                dbQueries.deleteRating(rating);
                                dbQueries.deleteRatingId(mid);
                                dbQueries.insertRating(rating);
                                dbQueries.insertRatingId(mid);
                            } else {
                                dbQueries.insertRating(rating);
                                dbQueries.insertRatingId(mid);
                            }

                            ratingStatus = 0;
                        }


                        /*Firebase Event log*/
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                        Bundle bun = new Bundle();
                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "News Detail");
                        bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                        setIncreaseViewAPI(mid);

                    } else {
                        mDialog.dismiss();
                        getActivity().onBackPressed();
                       // Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();

                    }

                } catch (Exception e) {
                    mDialog.dismiss();
                    e.printStackTrace();
                }
                // BusProvider.getInstance().post(new ServerEvent(response.body()));

                Log.e(TAG, "Success" + response.body());
            }

            @Override
            public void onFailure(Call<NewsDetailResponse> call, Throwable t) {
                // handle execution failures like no internet connectivity
                // BusProvider.getInstance().post(new ErrorEvent(-2,t.getMessage()));
                mDialog.dismiss();
                Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
            }
        });

    }


    private void setIncreaseViewAPI(String newsId) {
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        //The logging interceptor will be added to the http client
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        //The Retrofit builder will have the client attached, in order to get connection logs
        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.HIPSTO_BASE_URL)
                .build();

        HipstoAPI service = retrofit.create(HipstoAPI.class);
        service.increaceViewcount(authToken, userEncode.trim(), newsId).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> responses) {
                try {
                    //JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    // Boolean status = jsonObject.getBoolean("success");
                    if (responses.isSuccessful()) {
                        Log.e("success", "view count: " + responses.message());
                    } else {
                        Log.e("failed", "view count: " + responses.message());
                        // Toast.makeText(getActivity(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (getActivity() != null) {
                        Log.e("catched", "view count: " + e);
                        //  Toast.makeText(getActivity(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                if (getActivity() != null) {
                    Log.e("failiure response", "view count: " + t);
                    // Toast.makeText(getActivity(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                }
            }
        });

    }


    private void dialogRateTheNews() {

        //LottieAnimationView loader;
       /* loader = dialog.findViewById(R.id.hipsto_loader);
        loader.playAnimation();*/
        // loader = dialog.findViewById(R.id.hipsto_loader);
        // loader.setAnimation("hipstoloader.json");
        // loader.setProgress(0);
        // loader.pauseAnimation();
        //loader.playAnimation();

        dialogRate = new Dialog(getContext());
        dialogRate.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogRate.setCancelable(false);
        dialogRate.setContentView(R.layout.dialog_rating);

        final RatingBar mRatingBar = dialogRate.findViewById(R.id.rb_rate_the_news);
        final LinearLayout ltRating = dialogRate.findViewById(R.id.lt_rating);
        final LinearLayout ltAlert = dialogRate.findViewById(R.id.lt_alert);
        final TextView tvAlertMsg = dialogRate.findViewById(R.id.tv_alert_msg);
        TextView tvOkay = dialogRate.findViewById(R.id.tv_okay);
        TextView tvNewsTopic = dialogRate.findViewById(R.id.tv_news_topic);
        TextView tvNewsDomain = dialogRate.findViewById(R.id.tv_news_domain);
        TextView cancel = dialogRate.findViewById(R.id.tv_cancel);
        Button submit = dialogRate.findViewById(R.id.btn_submit);
        final EditText comments = dialogRate.findViewById(R.id.comment_edit);

        ltAlert.setVisibility(View.GONE);
        ltRating.setVisibility(View.VISIBLE);
        tvNewsTopic.setText(dataTitle);
        tvNewsDomain.setText("@" + domain);
        dialogRate.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRate.dismiss();
            }
        });

        tvOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pageStatus.equals("1")) {
                    newsDetail(newsUrlId, newsId, ratingStatus, "Trend");
                } else {
                    newsDetail(newsUrlId, newsId, ratingStatus, "News");
                }
                dialogRate.dismiss();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer rating = Math.round(mRatingBar.getRating());
                if (rating != 0) {
                    showProgressDialog();

                    String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
                    String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

                    if (Utilsdefault.isNetworkAvailable()) {

                        NewsData newsData = new NewsData();
                        newsData.setRating(rating.toString());
                        newsData.setComment(comments.getText().toString().trim());
                        hipstoAPI.rating(authToken, userEncode.trim(), newsId, newsData).enqueue(new Callback<JsonElement>() {
                            @Override
                            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                                if (response.isSuccessful()) {
                                    ltAlert.setVisibility(View.VISIBLE);
                                    int unicode = 0x1F44D;
                                    //  tvAlertMsg.setText("Rating Added Successfully" + new String(Character.toChars(unicode)));
                                    tvAlertMsg.setText(getResources().getString(R.string.rate_add_success) + new String(Character.toChars(unicode)));
                                    ltRating.setVisibility(View.GONE);
                                    ratingStatus = 1;
                                    mDialog.dismiss();
                                } else {
                                    ltAlert.setVisibility(View.VISIBLE);
                                    tvAlertMsg.setText(getContext().getResources().getString(R.string.failed));
                                    ltRating.setVisibility(View.GONE);
                                    ratingStatus = 0;
                                    mDialog.dismiss();
                                }

                            }

                            @Override
                            public void onFailure(Call<JsonElement> call, Throwable t) {
                                if (getActivity() != null) {
                                    System.out.println("error" + t.getMessage());
                                    Toast.makeText(getActivity(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    } else {
                        Utilsdefault.alertNetwork(getActivity());
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.add_rating), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv, final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {


            ssb.setSpan(new MySpannable(false) {
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "See Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, ".. See More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

}
