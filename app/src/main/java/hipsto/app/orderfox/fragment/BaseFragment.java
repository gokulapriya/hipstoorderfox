package hipsto.app.orderfox.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.activity.BaseActivity;

public class BaseFragment extends Fragment {
    String blockCharacterSet = "~#^|$%&*!)(?/-+@:,.;'=_1234567890#{}[]";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int stringId) {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.showProgress(stringId);
        }
    }

    public void hideProgress() {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.hideProgress();
        }
    }
    public static String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd MMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
    public static String getTimeFormat(String time) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(time);
            simpleDateFormat.applyPattern("hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public InputFilter editTextRestrictSmileys() {

        InputFilter restrictSmileys = new InputFilter() {

            public CharSequence filter(CharSequence src, int start,
                                       int end, Spanned dst, int dstart, int dend) {
                if (src.equals("")) { // for backspace
                    return src;
                }
                if (src.toString().matches("[\\x00-\\x7F]+")) {
                    return src;
                }
                return "";
            }
        };
        return restrictSmileys;
    }
    public InputFilter editTextRestrictSpecialCharacters() {
        InputFilter restrictSpecialCharacter = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                if (source != null && blockCharacterSet.contains(("" + source))) {
                    return "";
                }
                return null;
            }
        };
        return restrictSpecialCharacter;
    }
    public InputFilter editTextRestrictLength() {

        InputFilter setLength = new InputFilter.LengthFilter(30);
        return setLength;
    }
}
