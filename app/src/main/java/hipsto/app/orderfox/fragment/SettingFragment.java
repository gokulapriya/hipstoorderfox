package hipsto.app.orderfox.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.Sql.DBHelper;
import hipsto.app.orderfox.Sql.DBQueries;
import hipsto.app.orderfox.activity.HomeActivity;
import hipsto.app.orderfox.adapter.LanguageAdapter;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.firebase.DeepLinkManager;
import hipsto.app.orderfox.models.NewsData;
import hipsto.app.orderfox.models.TopicLangResponse;
import hipsto.app.orderfox.retrofit.HipstoAPI;
import hipsto.app.orderfox.utils.Constants;
import hipsto.app.orderfox.utils.Utilsdefault;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Gokulapriya on 6/30/2019.
 */

public class SettingFragment extends BaseFragment implements
        DeepLinkManager.DeepLinkListener {

    RecyclerView rvLanguageList;
    LanguageAdapter languageAdapter;
    ImageView ivLanguage, ivNewsStreams;
    RadioButton mostRecent, mostViewed, mostRated;
    RelativeLayout rtFeedback, rtLanguage, rtNewsStreams, rtMedia, rtFb, rtInsta, rtTwitter, rtVideo,
            rtRadio, rtPodcast, rtLanguageList, rtManagesub, rtInvitefrnd, rtRateapp;
    LinearLayout lnNewsStreamsList;
    List<TopicLangResponse> LanguageList = new ArrayList<>();
    List<String> allLanguageList = new ArrayList<>();
    Boolean language = true;
    Boolean news = true;
    SwitchCompat swMedia, swFb, swInsta, swTwitter, swVideo, swRadio, swPodcast;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public HipstoAPI hipstoAPI;
    ProgressDialog mDialog;
    DBHelper dbHelper;
    DBQueries dbQueries;
    private List<String> streamListArray = new ArrayList<>();
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting, container, false);

        HipstoApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        initViews(v);
        performClick(v);

        return v;
    }

    private void initViews(View v) {
        rvLanguageList = (RecyclerView) v.findViewById(R.id.rv_language_list);

        mostRecent = v.findViewById(R.id.rb_most_recent);
        mostRated = v.findViewById(R.id.rb_most_rated);
        mostViewed = v.findViewById(R.id.rb_most_viewed);

        rtLanguage = (RelativeLayout) v.findViewById(R.id.rt_language);
        rtFeedback = (RelativeLayout) v.findViewById(R.id.rt_feedback);
        ivLanguage = (ImageView) v.findViewById(R.id.iv_language_arrow);
        ivNewsStreams = (ImageView) v.findViewById(R.id.iv_news_streams_arrow);
        rtNewsStreams = (RelativeLayout) v.findViewById(R.id.rt_news_streams);
        rtMedia = (RelativeLayout) v.findViewById(R.id.rt_media);
        rtFb = (RelativeLayout) v.findViewById(R.id.rt_fb);
        rtInsta = (RelativeLayout) v.findViewById(R.id.rt_insta);
        rtTwitter = (RelativeLayout) v.findViewById(R.id.rt_twitter);
        rtVideo = (RelativeLayout) v.findViewById(R.id.rt_video);
        rtRadio = (RelativeLayout) v.findViewById(R.id.rt_radio);
        rtPodcast = (RelativeLayout) v.findViewById(R.id.rt_podcast);
        lnNewsStreamsList = (LinearLayout) v.findViewById(R.id.ln_news_streams);
        rtLanguageList = (RelativeLayout) v.findViewById(R.id.rt_language_list);
        rtManagesub = (RelativeLayout) v.findViewById(R.id.rt_mng_sub);
        rtInvitefrnd = (RelativeLayout) v.findViewById(R.id.rt_invite_frds);
        rtRateapp = (RelativeLayout) v.findViewById(R.id.rt_rate_app);

        swMedia = (SwitchCompat) v.findViewById(R.id.sw_media);
        swFb = (SwitchCompat) v.findViewById(R.id.sw_fb);
        swInsta = (SwitchCompat) v.findViewById(R.id.sw_insta);
        swTwitter = (SwitchCompat) v.findViewById(R.id.sw_twitter);
        swVideo = (SwitchCompat) v.findViewById(R.id.sw_video);
        swRadio = (SwitchCompat) v.findViewById(R.id.sw_radio);
        swPodcast = (SwitchCompat) v.findViewById(R.id.sw_podcast);

        HomeActivity.rt_navigationView.setVisibility(View.VISIBLE);
        HomeActivity.toolbar.setVisibility(View.VISIBLE);
        HomeActivity.rtSetting.setVisibility(View.GONE);
        HomeActivity.rtSearch.setVisibility(View.GONE);
        HomeActivity.rt_bookmark.setVisibility(View.VISIBLE);
        HomeActivity.ivLogo.setEnabled(true);
        editor.putInt(Constants.LOADFRAGMENT,0).apply();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvLanguageList.setLayoutManager(linearLayoutManager);
        rvLanguageList.setItemAnimator(new DefaultItemAnimator());

        dbHelper = new DBHelper(getContext());
        dbQueries = new DBQueries(getContext());
        dbQueries.open(getContext());

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        if (language) {
            rtLanguageList.setVisibility(View.VISIBLE);
            ivLanguage.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_up_icon));
            language = false;
        }

        if (news) {
            lnNewsStreamsList.setVisibility(View.VISIBLE);
            ivNewsStreams.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_up_icon));
            news = false;
        }

        /*Firebase Event log*/
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle bundle = new Bundle();
        if (bundle != null) {
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "0");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Settings Page");
            bundle.putString(FirebaseAnalytics.Param.CONTENT, "Settings Page Click");
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        }

        try {
            if (Utilsdefault.isNetworkAvailable()) {
                run();
                //NewStreamModule
                newStreams();
                //SortBy
                sortBy();

            } else {
                Utilsdefault.alertNetwork(getActivity());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void performClick(View v) {

        HomeActivity.ivLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                fragment = new NewsFragment();
                ((HomeActivity) getContext()).loadFragment(fragment);
            }
        });

        // Up & Down Arrow
        rtLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (language) {
                    rtLanguageList.setVisibility(View.VISIBLE);
                    ivLanguage.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_up_icon));
                    language = false;
                } else {
                    rtLanguageList.setVisibility(View.GONE);
                    ivLanguage.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_down));
                    language = true;
                }

            }
        });

        rtNewsStreams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (news) {
                    lnNewsStreamsList.setVisibility(View.VISIBLE);
                    ivNewsStreams.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_up_icon));
                    news = false;
                } else {
                    lnNewsStreamsList.setVisibility(View.GONE);
                    ivNewsStreams.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_down));
                    news = true;
                }
            }
        });

        rtFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  Bundle bundle = new Bundle();
                bundle.putString("uri", "https://hipsto.global/contact/");*/
                dialogFeedback();

            }
        });

        rtManagesub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });

        rtInvitefrnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 /* Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                            .setMessage(getString(R.string.invitation_message))
                            .setDeepLink(Uri.parse("https://share.hipsto.global/?link=https://hipsto.global/hipsto_orderfox&apn=hipsto.app.orderfox.marcel&isi=1417111395&ibi=hipsto.app.orderfox.marcel"))
                           .setCallToActionText(getString(R.string.invitation_cta))
                          .build();
                    startActivityForResult(intent, 100);

                Toast.makeText(getContext(), "Coming Soon", Toast.LENGTH_SHORT).show();*/

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, getString(R.string.invitation_message) + " " + getString(R.string.invitation_deep_link));
                startActivityForResult(Intent.createChooser(share, "Share link!"), 1);

            }
        });

        rtRateapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void run() throws IOException {
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        String topicId = sharedPreferences.getString(Constants.TOPICID, "");

        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.HIPSTO_BASE_URL + "topic-settings/topic_id").newBuilder();

        String url = urlBuilder.build().toString();

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .header("Authorization", authToken)
                .header("Accept", "application/json")
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                LanguageList.clear();
                final String myResponse = response.body().string();
                Log.e("res", ">" + myResponse);
                try {
                    JSONArray json = new JSONArray(myResponse);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject jsonObject = json.getJSONObject(i);
                        TopicLangResponse topicLangResponse = new TopicLangResponse(
                                jsonObject.getString("code"),
                                jsonObject.getString("name"),
                                jsonObject.getString("native"));
                        LanguageList.add(topicLangResponse);
                        allLanguageList.add(LanguageList.get(i).getName());
                    }

                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                languageAdapter = new LanguageAdapter(getContext(), LanguageList);
                                rvLanguageList.setAdapter(languageAdapter);
                            }
                        });

                        if(dbQueries.readSettingLang().size() < 0){
                            editor.putString(Constants.ALLLANGUAGE,allLanguageList.toString()).apply();
                        }else{
                            System.out.println("Hii language "+dbQueries.readSettingLang().size()+"--Array--"+dbQueries.readSettingLang().toString());
                            allLanguageList.clear();
                            for (int i=0;i<dbQueries.readSettingLang().size();i++){
                                allLanguageList.add(dbQueries.readSettingLang().get(i).getName());
                            }
                            editor.putString(Constants.ALLLANGUAGE,allLanguageList.toString()).apply();
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void newStreams() {

        System.out.println("Stream"+dbQueries.readSettingStream().size()+"-"+dbQueries.readSettingStream());
        streamListArray = dbQueries.readSettingStream();
        if(!(streamListArray.size()>0)){
            dbQueries.insertSettingStream("media");
            dbQueries.insertSettingStream("twitter");
            swMedia.setChecked(true);
            swTwitter.setChecked(true);
        }

     /*   swMedia.setChecked(true);
        swTwitter.setChecked(true);*/

        swMedia.setClickable(false);
        swFb.setClickable(false);
        swInsta.setClickable(false);
        swVideo.setClickable(false);
        swRadio.setClickable(false);
        swPodcast.setClickable(false);

        swMedia.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(getContext(), "Media couldn't be switched off", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        swFb.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(getContext(), "Coming Soon", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        swInsta.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(getContext(), "Coming Soon", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        swVideo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(getContext(), "Coming Soon", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        swRadio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(getContext(), "Coming Soon", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        swPodcast.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(getContext(), "Coming Soon", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        if (streamListArray.contains("media")) {
            swMedia.setChecked(true);
        } else {
            swMedia.setChecked(false);
        }

        if (streamListArray.contains("twitter")) {
            swTwitter.setChecked(true);
        } else {
            swTwitter.setChecked(false);
        }


     /*   if(streamListArray.contains("facebook")){
            swFb.setChecked(true);
        }else{
            swFb.setChecked(false);
        }

        if(streamListArray.contains("instagram")){
            swInsta.setChecked(true);
        }else{
            swInsta.setChecked(false);
        }*/

    /*    if(streamListArray.contains("video")){
            swVideo.setChecked(true);
        }else{
            swVideo.setChecked(false);
        }

        if(streamListArray.contains("radio")){
            swRadio.setChecked(true);
        }else{
            swRadio.setChecked(false);
        }

        if(streamListArray.contains("podcast")){
            swPodcast.setChecked(true);
        }else{
            swPodcast.setChecked(false);
        }*/

        swMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (swMedia.isChecked()) {
                    swMedia.setChecked(true);
                    dbQueries.insertSettingStream("media");
                } else {
                    if(dbQueries.readSettingStream().size()>1){
                        System.out.println("Hiiiiiiiiii delete");
                        dbQueries.deleteSettingStream("media");
                    }else{
                        swMedia.setChecked(true);
                        Toast.makeText(getContext(), "You need to enable atleast one news stream", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        swTwitter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (swTwitter.isChecked()) {
                    swMedia.setChecked(true);
                    dbQueries.insertSettingStream("twitter");
                } else {
                    if(dbQueries.readSettingStream().size()>1){
                        dbQueries.deleteSettingStream("twitter");
                    }else{
                        swTwitter.setChecked(true);
                        Toast.makeText(getContext(), "You need to enable atleast one news stream", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

      /*  swFb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (swFb.isChecked()) {
                    dbQueries.insertSettingStream("facebook");
                } else {
                    dbQueries.deleteSettingStream("facebook");
                }
            }
        });

        swInsta.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (swInsta.isChecked()) {
                    dbQueries.insertSettingStream("instagram");
                } else {
                    dbQueries.deleteSettingStream("instagram");
                }
            }
        });*/

     /*   swVideo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (swVideo.isChecked()) {
                    dbQueries.insertSettingStream("video");
                } else {
                    dbQueries.deleteSettingStream("video");
                }
            }
        });


        swRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (swRadio.isChecked()) {
                    dbQueries.insertSettingStream("radio");
                } else {
                    dbQueries.deleteSettingStream("radio");
                }
            }
        });

        swPodcast.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (swPodcast.isChecked()) {
                    dbQueries.insertSettingStream("podcast");
                } else {
                    dbQueries.deleteSettingStream("podcast");
                }
            }
        });*/
    }

    private void sortBy() {

        String sortBy = sharedPreferences.getString(Constants.SORTBY, "");
        assert sortBy != null;
        switch (sortBy) {
            case "":
                mostRecent.setChecked(true);
                break;
            case "viewed":
                mostViewed.setChecked(true);
                break;
            case "rated":
                mostRated.setChecked(true);
                break;
        }

        if (mostRecent.isChecked()) {
            mostRecent.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.black_background));
        } else {
            mostRecent.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.gray_background));
        }

        if (mostViewed.isChecked()) {
            mostViewed.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.black_background));
        } else {
            mostViewed.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.gray_background));
        }

        if (mostRated.isChecked()) {
            mostRated.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.black_background));
        } else {
            mostRated.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.gray_background));
        }

        mostRecent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mostRecent.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.black_background));
                    editor.putString(Constants.SORTBY, "").apply();
                } else {
                    mostRecent.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.gray_background));
                }
            }
        });

        mostViewed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mostViewed.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.black_background));
                    editor.putString(Constants.SORTBY, "viewed").apply();
                } else {
                    mostViewed.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.gray_background));
                }
            }
        });

        mostRated.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The Radio is enabled
                    mostRated.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.black_background));
                    editor.putString(Constants.SORTBY, "rated").apply();
                } else {
                    mostRated.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.gray_background));

                }
            }
        });

    }

    private void dialogFeedback() {

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_feedback);

        final RadioButton rbSmile = dialog.findViewById(R.id.rb_smile);
        final RadioButton rbVsmile = dialog.findViewById(R.id.rb_vsmile);
        final RadioButton rbSad = dialog.findViewById(R.id.rb_sad);
        final RadioButton rbLike = dialog.findViewById(R.id.rb_like);
        final RadioButton rbDislike = dialog.findViewById(R.id.rb_dislike);
        final RadioGroup rgSmiley = dialog.findViewById(R.id.rg_smile);

        TextView tvCancel = dialog.findViewById(R.id.tv_fb_cancel);
        Button btnSubmit = dialog.findViewById(R.id.btn_fb_submit);
        final EditText etFeedback = dialog.findViewById(R.id.et_feedback_edit);

        dialog.show();

        final ArrayList<String> ar = new ArrayList<String>();
        ar.add(new String(Character.toChars(0x1F44D)));
        ar.add(new String(Character.toChars(0x1F60A)));
        ar.add(new String(Character.toChars(0x1F642)));
        ar.add(new String(Character.toChars(0x1F641)));
        ar.add(new String(Character.toChars(0x1F44E)));

        etFeedback.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().length() == 0) {
                    System.out.println("Smiley " + s.toString() + " " + new String(Character.toChars(0x1F60A)));
                    rbLike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_like_gray));
                    rbVsmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gd_gray));
                    rbSmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gray));
                    rbSad.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_sad_gray));
                    rbDislike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_dislike_gray));
                } else {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }


        });


        rgSmiley.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.rb_like) {

                    rbLike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_like));
                    rbVsmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gd_gray));
                    rbSmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gray));
                    rbSad.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_sad_gray));
                    rbDislike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_dislike_gray));

                    String characterFilter = "[^\\p{L}\\p{M}\\p{N}\\p{P}\\p{Z}\\p{Cf}\\p{Cs}\\s]";
                    String emotionless = etFeedback.getText().toString().trim().replaceAll(characterFilter, "");
                    int unicode = 0x1F604;
                    String fb = emotionless + new String(Character.toChars(unicode));
                    etFeedback.setText(fb);
                    etFeedback.setSelection(etFeedback.getText().length());

                } else if (checkedId == R.id.rb_vsmile) {

                    rbLike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_like_gray));
                    rbVsmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gd));
                    rbSmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gray));
                    rbSad.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_sad_gray));
                    rbDislike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_dislike_gray));

                    String characterFilter = "[^\\p{L}\\p{M}\\p{N}\\p{P}\\p{Z}\\p{Cf}\\p{Cs}\\s]";
                    String emotionless = etFeedback.getText().toString().trim().replaceAll(characterFilter, "");
                    int unicode = 0x1F60A;
                    String fb = emotionless + new String(Character.toChars(unicode));
                    etFeedback.setText(fb);
                    etFeedback.setSelection(etFeedback.getText().length());

                } else if (checkedId == R.id.rb_smile) {

                    rbLike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_like_gray));
                    rbVsmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gd_gray));
                    rbSmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile));
                    rbSad.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_sad_gray));
                    rbDislike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_dislike_gray));

                    String characterFilter = "[^\\p{L}\\p{M}\\p{N}\\p{P}\\p{Z}\\p{Cf}\\p{Cs}\\s]";
                    String emotionless = etFeedback.getText().toString().trim().replaceAll(characterFilter, "");
                    int unicode = 0x1F611;
                    String fb = emotionless + new String(Character.toChars(unicode));
                    etFeedback.setText(fb);
                    etFeedback.setSelection(etFeedback.getText().length());

                } else if (checkedId == R.id.rb_sad) {

                    rbLike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_like_gray));
                    rbVsmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gd_gray));
                    rbSmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gray));
                    rbSad.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_sad));
                    rbDislike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_dislike_gray));

                    String characterFilter = "[^\\p{L}\\p{M}\\p{N}\\p{P}\\p{Z}\\p{Cf}\\p{Cs}\\s]";
                    String emotionless = etFeedback.getText().toString().trim().replaceAll(characterFilter, "");
                    int unicode = 0x1F61F;
                    String fb = emotionless + new String(Character.toChars(unicode));
                    etFeedback.setText(fb);
                    etFeedback.setSelection(etFeedback.getText().length());

                } else if (checkedId == R.id.rb_dislike) {

                    rbLike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_like_gray));
                    rbVsmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gd_gray));
                    rbSmile.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smile_gray));
                    rbSad.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_sad_gray));
                    rbDislike.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_smiley_dislike));

                    String characterFilter = "[^\\p{L}\\p{M}\\p{N}\\p{P}\\p{Z}\\p{Cf}\\p{Cs}\\s]"; //*Regrex: "[^\\p{L}\\p{M}\\p{N}\\p{P}\\p{Z}\\p{Cf}\\p{Cs}\\s]" ///////////Explanation////////[\\p{L}\\p{M}\\p{N}\\p{P}\\p{Z}\\p{Cf}\\p{Cs}\\s] is a range representing all numeric (\\p{N}), letter (\\p{L}), mark (\\p{M}), punctuation (\\p{P}), whitespace/separator (\\p{Z}), other formatting (\\p{Cf}) and other characters above U+FFFF in Unicode (\\p{Cs}), and newline (\\s) characters. \\p{L} specifically includes the characters from other alphabets such as Cyrillic, Latin, Kanji, etc.*//*
                    String emotionless = etFeedback.getText().toString().trim().replaceAll(characterFilter, "");
                    int unicode = 0x1F622;
                    String fb = emotionless + new String(Character.toChars(unicode));
                    etFeedback.setText(fb);
                    etFeedback.setSelection(etFeedback.getText().length());

                }
            }

        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etFeedback.setText("");
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!etFeedback.getText().toString().trim().equals("")) {
                    String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
                    String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

                    NewsData newsData = new NewsData();
                newsData.setFeedback(etFeedback.getText().toString().trim());
                if (Utilsdefault.isNetworkAvailable()) {
                    hipstoAPI.feedback(authToken, userEncode.trim(), newsData).enqueue(new retrofit2.Callback<JsonElement>() {
                        @Override
                        public void onResponse(retrofit2.Call<JsonElement> call, retrofit2.Response<JsonElement> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.feedback_success), Toast.LENGTH_LONG).show();

                                dialog.dismiss();
                            } else {
                                Toast.makeText(getContext(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(retrofit2.Call<JsonElement> call, Throwable t) {
                            if (getActivity() != null) {
                                System.out.println("error" + t.getMessage());
                                Toast.makeText(getActivity(), getContext().getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } else {
                    Utilsdefault.alertNetwork(getActivity());
                }}else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.feedback_mandatory), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("", "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                for (String id : ids) {
                    System.out.println("sent invitation " + id);
                }

            } else {
                Toast.makeText(getActivity(), "Google Play Services Error", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onConnectionError(String errorMessage) {
        Toast.makeText(getActivity(), "Google Play Services Error" + errorMessage, Toast.LENGTH_LONG).show();
    }

/*

   private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void getSettingAPI() {
        showProgressDialog();
        String authToken = sharedPreferences.getString(Constants.PREFTOKEN, "");
        hipstoAPI.settingsTopicRespo(authToken, "")
                .enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        try {
                            Log.e("res", ">" + response.body());
                            mDialog.dismiss();

                            if (!response.getApiKey().isEmpty()) {
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                                rvLanguageList.setLayoutManager(linearLayoutManager);
                                rvLanguageList.setItemAnimator(new DefaultItemAnimator());
                                editor.putString(Constants.TOPICID, response.body().getId().getOid()).commit();
                                if (!response.body().getDataLang().isEmpty()) {
                                    LanguageList = response.body().getDataLang();
                                    // LanguageList.add("English");
                                    // LanguageList.add("Nederlands, Viaams");
                                    languageAdapter = new LanguageAdapter(getContext(), LanguageList);
                                    rvLanguageList.setAdapter(languageAdapter);
                                }
                                mDialog.dismiss();
                            } else {
                                mDialog.dismiss();
                                Toast.makeText(getActivity(), "No Records found", Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            mDialog.dismiss();
                            e.printStackTrace();
                            Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        mDialog.dismiss();
                        Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                });
    }
*/

}
