package hipsto.app.orderfox.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.activity.HomeActivity;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.utils.Constants;

/**
 * Created by Gokulapriya on 6/22/2019.
 */

public class ExploreFragment extends Fragment {

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        HipstoApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_explore, container, false);
        HomeActivity.rt_navigationView.setVisibility(View.VISIBLE);
        HomeActivity.toolbar.setVisibility(View.VISIBLE);
        HomeActivity.rtSetting.setVisibility(View.VISIBLE);
        HomeActivity.rtSearch.setVisibility(View.GONE);
        HomeActivity.rt_bookmark.setVisibility(View.VISIBLE);
        HomeActivity.ivLogo.setEnabled(true);
        HomeActivity.navigation.getMenu().getItem(1).setChecked(true);
        editor.putInt(Constants.LOADFRAGMENT,0).apply();
        HomeActivity.ivLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                fragment = new NewsFragment();
                ((HomeActivity) getContext()).loadFragment(fragment);
            }
        });
        return v;
    }

}
