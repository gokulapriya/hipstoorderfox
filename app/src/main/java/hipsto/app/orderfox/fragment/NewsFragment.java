package hipsto.app.orderfox.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.appodeal.ads.Appodeal;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.gson.JsonElement;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.Sql.DBQueries;
import hipsto.app.orderfox.activity.HomeActivity;
import hipsto.app.orderfox.adapter.AppodealWrapperAdapter;
import hipsto.app.orderfox.adapter.NewsImageSliderAdapter;
import hipsto.app.orderfox.adapter.NewsListAdapter;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.models.BadgeData;
import hipsto.app.orderfox.models.NewsData;
import hipsto.app.orderfox.models.NewsFeedResponse;
import hipsto.app.orderfox.models.NewsPostResponse;
import hipsto.app.orderfox.models.TrendingResponse;
import hipsto.app.orderfox.retrofit.HipstoAPI;
import hipsto.app.orderfox.utils.AppBarStateChangeListener;
import hipsto.app.orderfox.utils.Constants;

import hipsto.app.orderfox.utils.Utilsdefault;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

/**
 * Created by Gokulapriya on 5/23/2019.
 */

public class NewsFragment extends BaseFragment{

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public HipstoAPI hipstoAPI;
    private FirebaseAnalytics mFirebaseAnalytics;

    private AppBarLayout appBarLayout;
    private ViewPager vpNewList;
    private CirclePageIndicator cpiNewList;
    private RelativeLayout rtTrend;
    private ImageView ivDelete;
    private EditText etSearch;
    private RelativeLayout rtSearch, rtDelete, rtPageTop;
    public RelativeLayout rtShare, rtWhatapp, rtFb, rtTelegram, rtMoreOption;
    public ProgressDialog mDialog;
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    private RecyclerView rvNewslist;
    private TextView tvNoNewsList;
    private Button btnCount;
    CoordinatorLayout coordinatorLayout;

    DBQueries dbQueries;
    NewsListAdapter newsListAdapter;
    private int nativeTemplateType = 2;
    private AppodealWrapperAdapter appodealWrapperAdapter;
    NewsImageSliderAdapter newsImageSliderAdapter;
    private ArrayList<TrendingResponse> trendingResponseList = new ArrayList<>();
    private ArrayList<String> TrendDetailResponseList = new ArrayList<>();
    private ArrayList<String> DetailResponseList = new ArrayList<>();
    private List<NewsFeedResponse.ItemsItem> feedResponseArrayList = new ArrayList<>();
    ArrayList<String> myLangList = new ArrayList<>();
    ArrayList<String> myTrendLangList = new ArrayList<>();
    private List<String> myStreamList = new ArrayList<>();
    private NewsFeedResponse feedResponse = new NewsFeedResponse();
    private int count = 0;
    int offset = 1;
    int limit = 1;
    boolean loading = true;
    Boolean collapse = true;
    Boolean search = true;
    Boolean start = true;
    Boolean network = true;

    Boolean startIndicator = true;
    String searchText = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_news, container, false);

        HipstoApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        initViews(v);
        performClick(v);

        return v;
    }

    //initalize
    @SuppressLint("ClickableViewAccessibility")
    private void initViews(View v) {

        appBarLayout = (AppBarLayout) v.findViewById(R.id.appbar);

        /*Trend Layout*/

        coordinatorLayout = v.findViewById(R.id.cl);
        vpNewList = v.findViewById(R.id.vp_new_list);
        cpiNewList = (CirclePageIndicator) v.findViewById(R.id.indicator);
        rtTrend = v.findViewById(R.id.rt_trend);

        /*Main Layout*/

        swipeRefresh = v.findViewById(R.id.swipe_refresh);
        rvNewslist = (RecyclerView) v.findViewById(R.id.rv_news_list);
        tvNoNewsList = (TextView) v.findViewById(R.id.tv_no_news_list);

        btnCount = (Button) v.findViewById(R.id.lt_count);
        rtPageTop = (RelativeLayout) v.findViewById(R.id.rt_page_top);

        /*Search layout*/
        rtSearch = (RelativeLayout) v.findViewById(R.id.rt_search);
        etSearch = (EditText) v.findViewById(R.id.et_search);
        rtDelete = (RelativeLayout) v.findViewById(R.id.rt_delete);
        ivDelete = (ImageView) v.findViewById(R.id.iv_delete);

        /*Share Layout*/

        rtShare = (RelativeLayout) v.findViewById(R.id.rt_share);
        rtWhatapp = (RelativeLayout) v.findViewById(R.id.rt_whatapp);
        rtFb = (RelativeLayout) v.findViewById(R.id.rt_fb);
        rtTelegram = (RelativeLayout) v.findViewById(R.id.rt_telegram);
        rtMoreOption = (RelativeLayout) v.findViewById(R.id.rt_more);

        /*Firebase Analytics Screen view*/

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "News Feed", null /* class override */);

        /*Firebase Analytics Events Log*/
        Bundle bundle = new Bundle();
        if (bundle != null) {
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "0");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "News Feed");
            bundle.putString(FirebaseAnalytics.Param.CONTENT, "News Feed Page Click");
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        }

        /*AppBar Layout*/

        if (collapse) {
            appBarLayout.setExpanded(true, true);
        } else {
            appBarLayout.setExpanded(false, true);
        }

        if(search){
            System.out.println("Hii search no");
            rtSearch.setVisibility(View.GONE);
            HomeActivity.rtSearch.setVisibility(View.VISIBLE);
        }else{
            System.out.println("Hii search Yes");
            rtSearch.setVisibility(View.VISIBLE);
            HomeActivity.rtSearch.setVisibility(View.GONE);
        }


        appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                Log.d("STATE", state.name());
                if (state.name().equals("EXPANDED")) {
                    swipeRefresh.setEnabled(true);
                } else {
                    swipeRefresh.setEnabled(false);
                }
            }
        });

        /*Progress Dialog*/

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        /*Toolbar Layout*/

        HomeActivity.rt_navigationView.setVisibility(View.VISIBLE);
        HomeActivity.toolbar.setVisibility(View.VISIBLE);
        HomeActivity.rtSetting.setVisibility(View.VISIBLE);
        HomeActivity.rtSearch.setVisibility(View.VISIBLE);
        HomeActivity.rt_bookmark.setVisibility(View.VISIBLE);
        HomeActivity.ivLogo.setEnabled(false);
        HomeActivity.navigation.getMenu().getItem(0).setChecked(true);
        editor.putInt(Constants.LOADFRAGMENT, 1).apply();

        /*Local Db*/

        dbQueries = new DBQueries(getActivity());
        dbQueries.open(getActivity());


        /*Appodeal*/

       /* Appodeal.setTesting(false);
        Appodeal.setLogLevel(com.appodeal.ads.utils.Log.LogLevel.debug);

        Appodeal.getUserSettings(getContext())
                .setAge(25)
                .setGender(UserSettings.Gender.OTHER);
        Appodeal.initialize(getActivity(), "b878181640b3a5d461f4a658379295af81373a42c059f30d", Appodeal.NATIVE | Appodeal.INTERSTITIAL | Appodeal.BANNER, true);

        Appodeal.setAutoCache(Appodeal.NATIVE, true);
        Appodeal.setNativeCallbacks(new AppodealNativeCallbacks(getActivity()));

        if (Appodeal.isLoaded(Appodeal.NATIVE)) {
            System.out.println("isLoaded --> True");
        }*/

        Appodeal.setBannerViewId(R.id.appodealBannerView);
        boolean isShown = Appodeal.show(getActivity(), Appodeal.BANNER_VIEW);
        System.out.println("Banner isShow --> " + isShown);

        /* RecyclerView */

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvNewslist.setItemAnimator(new DefaultItemAnimator());
        rvNewslist.setLayoutManager(linearLayoutManager);
        newsListAdapter = new NewsListAdapter(getContext(), feedResponseArrayList, NewsFragment.this);
        appodealWrapperAdapter = new AppodealWrapperAdapter(newsListAdapter, 10, 0);
        rvNewslist.setAdapter(appodealWrapperAdapter);

        rvNewslist.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                int pastVisibleItems = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                if (pastVisibleItems == 0) {
                    rtPageTop.setVisibility(View.GONE);
                }

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if (dy > 0) {


                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(coordinatorLayout.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);


                    rtPageTop.setVisibility(View.VISIBLE);
                    collapse = false;
                    swipeRefresh.setEnabled(false);
                    appBarLayout.setExpanded(false, true);
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (Utilsdefault.isNetworkAvailable()) {
                                loading = false;
                                offset = offset + limit;
                                String lastId = String.valueOf(feedResponseArrayList.get(feedResponseArrayList.size() - 1).getMid());
                                getNewsFeed(lastId, searchText);
                            } else {
                                network = false;
                                Utilsdefault.alertNetwork(getActivity());
                            }
                        }
                    }
                }else {
                    // Scrolling down
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(coordinatorLayout.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

                }
            }
        });

       /* newsListAdapter.setOnClickListen(new NewsListAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Fragment newsDetailFragment = new NewsDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("pageStatus", "2");
                bundle.putString("newsId", feedResponseArrayList.get(position).getMid());
                bundle.putInt("position", position);
                bundle.putStringArrayList("DetailIdList", DetailResponseList);
                newsDetailFragment.setArguments(bundle);
                ((HomeActivity) getContext()).loadFragment(newsDetailFragment);
            }
        });
*/

      /* ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.ACTION_STATE_IDLE,
                ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }
        });
        itemTouchHelper.attachToRecyclerView(rvNewslist);*/
       /* ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                final int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.LEFT) {
                    if(!(((position+1) % 5) == 0))
                    newsListAdapter.onItemLeftSwipe(position);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                try {

                    Bitmap icon;
                    if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                        View itemView = viewHolder.itemView;
                        float height = (float) itemView.getBottom() - (float) itemView.getTop();
                        float width = height / 5;
                        viewHolder.itemView.setTranslationX(dX / 5);

                        *//*paint.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX / 5, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, paint);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete);
                        RectF icon_dest = new RectF((float) (itemView.getRight() + dX /7), (float) itemView.getTop()+width, (float) itemView.getRight()+dX/20, (float) itemView.getBottom()-width);
                        c.drawBitmap(icon, null, icon_dest, paint);*//*
                    } else {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        };*/


     /*   ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvNewslist);*/


        /* Swipe Refresh Layout */

        swipeRefresh.setColorSchemeResources(R.color.appType);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Utilsdefault.isNetworkAvailable()) {
                            DetailResponseList.clear();
                            feedResponseArrayList.clear();
                            rvNewslist.setAdapter(null);
                            appodealWrapperAdapter.notifyDataSetChanged();
                            newsListAdapter = new NewsListAdapter(getContext(), feedResponseArrayList, NewsFragment.this);
                            appodealWrapperAdapter = new AppodealWrapperAdapter(newsListAdapter, 10, 0);
                            rvNewslist.setAdapter(appodealWrapperAdapter);
                            trendingResponseList.clear();
                            TrendDetailResponseList.clear();
                            etSearch.setText("");
                            search = true;
                            searchText = "";
                            rtSearch.setVisibility(View.GONE);
                            HomeActivity.rtSearch.setVisibility(View.VISIBLE);
                            appBarLayout.setExpanded(true, true);
                            startIndicator = true;
                            getTrendFeed();
                            getNewsFeed("", searchText);
                            swipeRefresh.setRefreshing(false);
                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                            Bundle bun = new Bundle();
                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, "0");
                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Refresh");
                            bun.putString(FirebaseAnalytics.Param.CONTENT, "Refresh Page Click");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                        } else {
                            network = false;
                            if (getActivity() != null) {
                                Utilsdefault.alertNetwork(getActivity());
                            }
                        }
                    }
                }, 0);
            }
        });


        /*NewsFeed*/
        if (Utilsdefault.isNetworkAvailable()) {
            if (start) {
                getNewsFeed("", searchText);
                start = false;
            } else {
                if (feedResponseArrayList.size() == 0) {
                    tvNoNewsList.setVisibility(View.VISIBLE);
                    rvNewslist.setVisibility(View.GONE);
                }else{
                    tvNoNewsList.setVisibility(View.GONE);
                    rvNewslist.setVisibility(View.VISIBLE);
                    newsListAdapter.notifyDataSetChanged();
                }
            }
        } else {
            network = false;
            if (getActivity() != null) {
                Utilsdefault.alertNetwork(getActivity());
            }
        }

    }

    public int ItemDismiss(int adapterPosition) {
        return adapterPosition;
    }

    //ClickFunction
    @SuppressLint("ClickableViewAccessibility")
    private void performClick(View v) {
        editor.putString("InterstitialAdsShown", "").apply();

        coordinatorLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(coordinatorLayout.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                return false;
            }
        });

        final Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds
                if (Utilsdefault.isNetworkAvailable()) {
                    trendingResponseList.clear();
                    TrendDetailResponseList.clear();
                    getTrendFeed();
                } else {
                    network = false;
                    if (getActivity() != null) {
                        Utilsdefault.alertNetwork(getActivity());
                    }
                }
                handler1.postDelayed(this, 900000);
            }
        }, 0);  //the time is in miliseconds

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String sortBy = sharedPreferences.getString(Constants.SORTBY, "");
                assert sortBy != null;
                if (sortBy.equals("") && searchText.equals("")) {
                    //Do something after 20 seconds
                    if (feedResponseArrayList.size() > 0) {
                        getNewContent(feedResponseArrayList.get(0).getMid());
                    }
                }
                handler.postDelayed(this, 900000);
            }
        }, 15000);  //the time is in miliseconds

        HomeActivity.rtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.rtSearch.setVisibility(View.GONE);
                rtSearch.setVisibility(View.VISIBLE);
                search = false;

                /*Firebase Event log*/
                mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                Bundle bun = new Bundle();
                bun.putString(FirebaseAnalytics.Param.ITEM_ID, "0");
                bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Search News");
                bun.putString(FirebaseAnalytics.Param.CONTENT, "Search News Click");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);

            }
        });

        etSearch.setFilters(new InputFilter[]{new EmojiExcludeFilter()});

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (etSearch.getText().toString().toLowerCase().trim().length() > 0) {
                        if (Utilsdefault.isNetworkAvailable()) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(coordinatorLayout.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                            DetailResponseList.clear();
                            feedResponseArrayList.clear();
                            rvNewslist.setAdapter(null);
                            search = false;
                            appodealWrapperAdapter.notifyDataSetChanged();
                            newsListAdapter = new NewsListAdapter(getContext(), feedResponseArrayList, NewsFragment.this);
                            appodealWrapperAdapter = new AppodealWrapperAdapter(newsListAdapter, 10, 0);
                            rvNewslist.setAdapter(appodealWrapperAdapter);
                            getNewsFeed("", etSearch.getText().toString().trim().toLowerCase());
                            searchText = etSearch.getText().toString().trim().toLowerCase();
                        } else {
                            network = false;
                            if (getActivity() != null) {
                                Utilsdefault.alertNetwork(getActivity());
                            }
                        }
                    } else {
                        Toast.makeText(getContext(), "Enter something", Toast.LENGTH_SHORT).show();
                    }

                    return true;
                }

                return false;
            }
        });

        rtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (etSearch.getText().toString().length() > 0) {
                    etSearch.setText("");
                } else {*/
                if (Utilsdefault.isNetworkAvailable()) {
                    searchText = "";
                    search = true;
                    DetailResponseList.clear();
                    feedResponseArrayList.clear();
                    rvNewslist.setAdapter(null);
                    appodealWrapperAdapter.notifyDataSetChanged();
                    newsListAdapter = new NewsListAdapter(getContext(), feedResponseArrayList, NewsFragment.this);
                    appodealWrapperAdapter = new AppodealWrapperAdapter(newsListAdapter, 10, 0);
                    rvNewslist.setAdapter(appodealWrapperAdapter);
                    etSearch.setText("");
                    getNewsFeed("", "");
                    HomeActivity.rtSearch.setVisibility(View.VISIBLE);
                    rtSearch.setVisibility(View.GONE);
                } else {
                    network = false;
                    if (getActivity() != null) {
                        Utilsdefault.alertNetwork(getActivity());
                    }
                }
               // }
            }
        });

        rtPageTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvNewslist.scrollToPosition(0);
                collapse = true;
                appBarLayout.setExpanded(true, true);
                rtPageTop.setVisibility(View.GONE);
            }
        });

        btnCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utilsdefault.isNetworkAvailable()) {
                    searchText = "";
                    etSearch.setText("");
                    HomeActivity.rtSearch.setVisibility(View.VISIBLE);
                    search = true;
                    rtSearch.setVisibility(View.GONE);
                    DetailResponseList.clear();
                    feedResponseArrayList.clear();
                    rvNewslist.setAdapter(null);
                    appodealWrapperAdapter.notifyDataSetChanged();
                    newsListAdapter = new NewsListAdapter(getContext(), feedResponseArrayList, NewsFragment.this);
                    appodealWrapperAdapter = new AppodealWrapperAdapter(newsListAdapter, 10, 0);
                    rvNewslist.setAdapter(appodealWrapperAdapter);
                    getNewsFeed("", searchText);
                    appBarLayout.setExpanded(true, true);
                    btnCount.setVisibility(View.GONE);
                } else {
                    network = false;
                    if (getActivity() != null) {
                        Utilsdefault.alertNetwork(getActivity());
                    }
                }
            }
        });

    }


    private class EmojiExcludeFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                int type = Character.getType(source.charAt(i));
                if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                    return "";
                }
            }
            return null;
        }
    }

    private Uri createShareUri(String newsId) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http") // "http"
                .authority("hipsto.global") // "365salads.xyz"
                .appendPath("hipsto_orderfox") // "salads"
                .appendQueryParameter("news", newsId);
        return builder.build();
    }

    private Uri createDynamicUri(Uri myuri) {
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(myuri)
                //.setDynamicLinkDomain("hipsto.page.link")
                .setDynamicLinkDomain("orderfox.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
//                .setIosParameters(DynamicLink.IosParameters.Builder("ibi").build())
                .buildDynamicLink();
        return dynamicLink.getUri();
    }

    //ShareFunction
    public void ShareClickListener(final String mid, final String dataTitle, final ImageView ivShare) {
      /*  rtShareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtShare.setVisibility(View.VISIBLE);
            }
        });*/

        rtShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtShare.setVisibility(View.GONE);
            }
        });

        rtWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(mid));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle + getResources().getString(R.string.share_text) + System.lineSeparator() +  shortLink;
                                    Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                                    whatsappIntent.setType("text/plain");
                                    whatsappIntent.setPackage("com.whatsapp");
                                    whatsappIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    whatsappIntent.putExtra(Intent.EXTRA_TEXT, msg);
                                    try {
                                        startActivityForResult(whatsappIntent, 1);
                                        if (!dbQueries.readShareId().contains(mid)) {
                                            dbQueries.insertShareId(mid);
                                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                            Bundle bun = new Bundle();
                                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with whatsapp - News Detail");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                            rtShare.setVisibility(View.GONE);
                                        }

                                    } catch (ActivityNotFoundException ex) {
                                        Toast.makeText(getActivity(), "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });
            }
        });

        rtMoreOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtShare.setVisibility(View.GONE);
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(mid));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();

                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle + getResources().getString(R.string.share_text) + System.lineSeparator() +  shortLink;
                                    Intent share = new Intent(Intent.ACTION_SEND);
                                    share.setType("text/plain");
                                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    share.putExtra(Intent.EXTRA_TEXT, msg);
                                    startActivityForResult(Intent.createChooser(share, "Share link!"), 1);
                                    if (!dbQueries.readShareId().contains(mid)) {
                                        dbQueries.insertShareId(mid);
                                        ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                        Bundle bun = new Bundle();
                                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with other - News Detail");
                                        bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                        rtShare.setVisibility(View.GONE);
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });
            }
        });

        rtFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(mid));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle + getResources().getString(R.string.share_text) + System.lineSeparator() +  shortLink;
                                    Intent fbIntent = new Intent(Intent.ACTION_SEND);
                                    fbIntent.setType("text/plain");
                                    fbIntent.setPackage("com.facebook.orca");
                                    fbIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    fbIntent.putExtra(Intent.EXTRA_TEXT, msg);
                                    try {
                                        startActivityForResult(fbIntent, 1);
                                        if (!dbQueries.readShareId().contains(mid)) {
                                            dbQueries.insertShareId(mid);
                                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                            Bundle bun = new Bundle();
                                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with Facebook - News Detail");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                            rtShare.setVisibility(View.GONE);
                                        }
                                    } catch (ActivityNotFoundException ex) {
                                        Toast.makeText(getActivity(), "Facebook have not been installed.", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });

            }
        });

        rtTelegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                Uri shortlink = createDynamicUri(createShareUri(mid));
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(shortlink)
                        .buildShortDynamicLink()
                        .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                            @Override
                            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                mDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    String msg = dataTitle + getResources().getString(R.string.share_text) + System.lineSeparator() +  shortLink;
                                    Intent telegramIntent = new Intent(Intent.ACTION_SEND);
                                    telegramIntent.setType("text/plain");
                                    telegramIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                    telegramIntent.setPackage("org.telegram.messenger");
                                    telegramIntent.putExtra(Intent.EXTRA_TEXT, msg);
                                    try {
                                        startActivityForResult(telegramIntent, 1);
                                        if (!dbQueries.readShareId().contains(mid)) {
                                            dbQueries.insertShareId(mid);
                                            ivShare.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_share_green));
                                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                            Bundle bun = new Bundle();
                                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with Telegram - News Detail");
                                            bun.putString(FirebaseAnalytics.Param.CONTENT, dataTitle);
                                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                            rtShare.setVisibility(View.GONE);
                                        }
                                    } catch (ActivityNotFoundException ex) {
                                        Toast.makeText(getActivity(), "Telegram have not been installed.", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Log.e("dynamic link", "" + task.getException());
                                }
                            }
                        });
            }
        });

    }

    //ShareFunction
/*
    public void ShareClickListener(final String mid, final String dataTitle) {

        rtShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtShare.setVisibility(View.GONE);
            }
        });

        rtWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.whatsapp");
                whatsappIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, dataTitle + ". Download HIPSTO ORDERFOX Now : http://" + getString(R.string.app_deep_link) + "news/" + mid);
                try {
                    startActivityForResult(whatsappIntent, 1);
                    if (!dbQueries.readShareId().contains(mid)) {
                        dbQueries.insertShareId(mid);
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                        Bundle bun = new Bundle();
                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with whatsapp - News list");
                        bun.putString(FirebaseAnalytics.Param.CONTENT,dataTitle);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                    }

                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Whatsapp have not been installed.", Toast.LENGTH_LONG).show();
                }
            }
        });

        rtMoreOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtShare.setVisibility(View.GONE);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_TEXT, dataTitle + ". Download HIPSTO ORDERFOX Now : http://" + getString(R.string.app_deep_link) + "news/" + mid);
                startActivityForResult(Intent.createChooser(share, "Share link!"), 1);
                if (!dbQueries.readShareId().contains(mid)) {
                    dbQueries.insertShareId(mid);
                    mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                    Bundle bun = new Bundle();
                    bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                    bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                    bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with more - News list");
                    bun.putString(FirebaseAnalytics.Param.CONTENT,dataTitle);
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                }
            }
        });

        rtFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fbIntent = new Intent(Intent.ACTION_SEND);
                fbIntent.setType("text/plain");
                fbIntent.setPackage("com.facebook.orca");
                fbIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                fbIntent.putExtra(Intent.EXTRA_TEXT, dataTitle + ". Download HIPSTO ORDERFOX Now : http://" + getString(R.string.app_deep_link) + "news/" + mid);
                try {
                    startActivityForResult(fbIntent, 1);
                    if (!dbQueries.readShareId().contains(mid)) {
                        dbQueries.insertShareId(mid);
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                        Bundle bun = new Bundle();
                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with Facebook - News list");
                        bun.putString(FirebaseAnalytics.Param.CONTENT,dataTitle);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                    }
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Facebook have not been installed.", Toast.LENGTH_LONG).show();
                }
            }
        });

        rtTelegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent telegramIntent = new Intent(Intent.ACTION_SEND);
                telegramIntent.setType("text/plain");
                telegramIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                telegramIntent.setPackage("org.telegram.messenger");
                telegramIntent.putExtra(Intent.EXTRA_TEXT, dataTitle + ". Download HIPSTO ORDERFOX Now : http://" + getString(R.string.app_deep_link) + "news/" + mid);
                try {
                    startActivityForResult(telegramIntent, 1);
                    if (!dbQueries.readShareId().contains(mid)) {
                        dbQueries.insertShareId(mid);
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                        Bundle bun = new Bundle();
                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, mid);
                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Share with Telegram - News list");
                        bun.putString(FirebaseAnalytics.Param.CONTENT,dataTitle);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                    }
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Telegram have not been installed.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
*/

    //Progress Dialog
    public void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    //TrendingNewsFeedSlider
    private void trendingImageSlider() {

        rtTrend.setVisibility(View.GONE);
        vpNewList.setVisibility(View.VISIBLE);
        cpiNewList.setVisibility(View.VISIBLE);
        /*vpNewList.startAutoScroll();
        vpNewList.setInterval(4000);*/
        /*vpNewList.setCycle(true);
        vpNewList.setStopScrollWhenTouch(true);*/
        newsImageSliderAdapter = new NewsImageSliderAdapter(getActivity(), trendingResponseList);
        vpNewList.setAdapter(newsImageSliderAdapter);
        newsImageSliderAdapter.notifyDataSetChanged();
        if (startIndicator) {
            cpiNewList.onPageSelected(0);
            startIndicator = false;
        }

        cpiNewList.setViewPager(vpNewList);

        final float density = getResources().getDisplayMetrics().density;
        cpiNewList.setRadius(4 * density);
        count = trendingResponseList.size();
        vpNewList.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
                if (state == 0) {
                    swipeRefresh.setEnabled(true);
                } else {
                    swipeRefresh.setEnabled(false);
                }
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                // Check if this is the page you want.
            }
        });

        newsImageSliderAdapter.setOnClickListen(new NewsImageSliderAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {

                Fragment newsDetailFragment = new NewsDetailFragment();
                byte[] encodeValue = Base64.encode(trendingResponseList.get(position).getUrl().getBytes(), Base64.DEFAULT);
                String urlBase64 = new String(encodeValue).replace("=", "").replace("\n", "").trim();
                Bundle bundle = new Bundle();
                bundle.putString("pageStatus", "1");
                bundle.putString("newsId", urlBase64);
                bundle.putInt("position", position);
                bundle.putStringArrayList("DetailIdList", TrendDetailResponseList);
                //bundle.putSerializable("trendingList", trendingResponseList);
                newsDetailFragment.setArguments(bundle);
                collapse = true;
                ((HomeActivity) getContext()).loadFragment(newsDetailFragment);

            /*    Fragment newsDetailFragment = new NewsDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("pageStatus", "1");
                bundle.putString("newsId", String.valueOf(trendingResponseList.get(position).getId()));
                bundle.putInt("position", position);
                bundle.putSerializable("trendingList", trendingResponseList);
                newsDetailFragment.setArguments(bundle);
                collapse = true;
                ((HomeActivity) getContext()).loadFragment(newsDetailFragment);*/
            }
        });
    }

    //TrendingNewsFeed
    private void getTrendFeed() {
        String cookie = /*sharedPreferences.getString(Constants.COOKIES, "Any")*/dbQueries.readLoginID(1).getCookies();
        myTrendLangList.clear();
        if (!dbQueries.readSettingLang().isEmpty()) {
            for (int i = 0; i < dbQueries.readSettingLang().size(); i++) {
                myTrendLangList.add(dbQueries.readSettingLang().get(i).getNative());
            }
        }
        //Here a logging interceptor is created
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        //The logging interceptor will be added to the http client
        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        //The Retrofit builder will have the client attached, in order to get connection logs
        final Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .baseUrl("http://api.trendolizer.com/v3/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HipstoAPI service = retrofit.create(HipstoAPI.class);

        Call<List<TrendingResponse>> call = service.trendingRespo(cookie, sharedPreferences.getString(Constants.ALLLANGUAGE, "Any"));

        call.enqueue(new Callback<List<TrendingResponse>>() {
            @Override
            public void onResponse(Call<List<TrendingResponse>> call, Response<List<TrendingResponse>> response) {
                try {
                    if (response.isSuccessful()) {
                        trendingResponseList = new ArrayList<>();
                       /* trendingResponseList.clear();
                        TrendDetailResponseList.clear();*/
                        System.out.println("Trending ==>" + response.body().toString());
                        trendingResponseList.addAll(response.body());

                        for (int i = 0; i < trendingResponseList.size(); i++) {

                            byte[] encodeValue = Base64.encode(trendingResponseList.get(i).getUrl().getBytes(), Base64.DEFAULT);
                            Log.d("TAG", "encodeURLValue = " + new String(encodeValue) + "-----" + trendingResponseList.get(i).getUrl());

                            String urlBase64 = new String(encodeValue).replace("=", "").replace("\n", "").trim();
                            TrendDetailResponseList.add(urlBase64);
                            Log.d("TAG", "encodeTrendURLValue = " + urlBase64);
                        }


                        if (trendingResponseList.size() > 0) {
                            trendingImageSlider();
                        } else {
                            rtTrend.setVisibility(View.VISIBLE);
                            vpNewList.setVisibility(View.GONE);
                            cpiNewList.setVisibility(View.GONE);
                            collapse = false;
                            swipeRefresh.setEnabled(false);
                            appBarLayout.setExpanded(false, true);
                        }

                    } else {
                        rtTrend.setVisibility(View.VISIBLE);
                        vpNewList.setVisibility(View.GONE);
                        cpiNewList.setVisibility(View.GONE);
                        collapse = false;
                        swipeRefresh.setEnabled(false);
                        appBarLayout.setExpanded(false, true);
                    }
                       /* if (trendingResponseList.size() > 0) {
                            trendingImageSlider();
                        } else {
                            rtTrend.setVisibility(View.VISIBLE);
                            vpNewList.setVisibility(View.GONE);
                            cpiNewList.setVisibility(View.GONE);
                        }

                    } else {
                        rtTrend.setVisibility(View.VISIBLE);
                        vpNewList.setVisibility(View.GONE);
                        cpiNewList.setVisibility(View.GONE);
                    }
*/
                } catch (Exception e) {
                    System.out.println("trend exc" + e.getMessage());
                    e.printStackTrace();
                }

                Log.e(TAG, "Success" + response.body());
            }

            @Override
            public void onFailure(Call<List<TrendingResponse>> call, Throwable t) {
            }
        });

    }


    //NewsFeed
    private void getNewsFeed(final String listerOld, String Search) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(coordinatorLayout.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

        showProgressDialog();
        loading = false;
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        String user = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

        String sortBy = sharedPreferences.getString(Constants.SORTBY, "");
        myLangList.clear();
        if (!dbQueries.readSettingLang().isEmpty()) {
            for (int i = 0; i < dbQueries.readSettingLang().size(); i++) {
                myLangList.add(dbQueries.readSettingLang().get(i).getCode());
            }
        }
        myStreamList.clear();
        if (!(dbQueries.readSettingStream().size() > 0)) {
            dbQueries.insertSettingStream("media");
            dbQueries.insertSettingStream("twitter");
        }
        if (!dbQueries.readSettingStream().isEmpty()) {
            myStreamList = dbQueries.readSettingStream();
        }

        NewsData newsData = new NewsData();
        newsData.setSources(myStreamList);
        newsData.setLangs(myLangList);
        newsData.setSearch(Search);
        newsData.setList_older_than(listerOld);
        newsData.setIdentifiers(null);
        newsData.setSort_by(sortBy);
        hipstoAPI.newsFeeds1(authToken, user.trim(), newsData).
                enqueue(new Callback<NewsFeedResponse>() {
                    @Override
                    public void onResponse
                            (Call<NewsFeedResponse> call, Response<NewsFeedResponse> response) {
                        try {
                            if (response.body().isSuccess() || !response.body().getItems().isEmpty()) {
                                tvNoNewsList.setVisibility(View.GONE);
                                rvNewslist.setVisibility(View.VISIBLE);
                                feedResponse.setItems(response.body().getItems());
                                feedResponseArrayList.addAll(feedResponse.getItems());

                                for (int i = 0; i < feedResponse.getItems().size(); i++) {
                                    DetailResponseList.add(feedResponse.getItems().get(i).getMid());
                                }
                                loading = true;
                                if(listerOld.equals("") && searchText.equals("")){
                                    dbQueries.updateNewsID(1,feedResponseArrayList.get(0).getMid());
                                }

                                newsListAdapter.notifyDataSetChanged();
                                mDialog.dismiss();
                            } else {
                                loading = true;
                                mDialog.dismiss();
                                newsListAdapter.notifyDataSetChanged();
                                if (feedResponseArrayList.size() == 0) {
                                    tvNoNewsList.setVisibility(View.VISIBLE);
                                    rvNewslist.setVisibility(View.GONE);
                                }
                                Toast.makeText(getActivity(), getResources().getString(R.string.no_record), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            mDialog.dismiss();
                            e.printStackTrace();
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.no_record), Toast.LENGTH_LONG).show();
                            }


                        }

                    }

                    @Override
                    public void onFailure(Call<NewsFeedResponse> call, Throwable t) {
                        mDialog.dismiss();
                        Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                });

    }

    //searchNewsFeed
    private void getSearchNewsFeed(String listerOld, String Search) {
        showProgressDialog();
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        String user = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

        String sortBy = sharedPreferences.getString(Constants.SORTBY, "");
        myLangList.clear();
        if (!dbQueries.readSettingLang().isEmpty()) {
            for (int i = 0; i < dbQueries.readSettingLang().size(); i++) {
                myLangList.add(dbQueries.readSettingLang().get(i).getCode());
            }
        }
        myStreamList.clear();
        if (!(dbQueries.readSettingStream().size() > 0)) {
            dbQueries.insertSettingStream("media");
            dbQueries.insertSettingStream("twitter");
        }
        if (!dbQueries.readSettingStream().isEmpty()) {
            myStreamList = dbQueries.readSettingStream();
        }

        NewsData newsData = new NewsData();
        newsData.setSources(myStreamList);
        newsData.setLangs(myLangList);
        newsData.setSearch(Search);
        newsData.setList_older_than(listerOld);
        newsData.setIdentifiers(null);
        newsData.setSort_by(sortBy);
        hipstoAPI.newsFeeds1(authToken, user.trim(), newsData).
                enqueue(new Callback<NewsFeedResponse>() {
                    @Override
                    public void onResponse(Call<NewsFeedResponse> call, Response<NewsFeedResponse> response) {
                        try {
                            if (!response.body().getItems().isEmpty()) {
                                tvNoNewsList.setVisibility(View.GONE);
                                rvNewslist.setVisibility(View.VISIBLE);
                                feedResponse.setItems(response.body().getItems());
                                feedResponseArrayList.addAll(feedResponse.getItems());
                                loading = true;
                                rvNewslist.setHasFixedSize(true);
                                newsListAdapter.notifyDataSetChanged();
                                mDialog.dismiss();

                            } else {
                                loading = true;
                                mDialog.dismiss();
                                tvNoNewsList.setVisibility(View.VISIBLE);
                                rvNewslist.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), getResources().getString(R.string.no_record), Toast.LENGTH_LONG).show();

                            }
                        } catch (Exception e) {
                            loading = true;
                            mDialog.dismiss();
                            e.printStackTrace();
                            Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<NewsFeedResponse> call, Throwable t) {
                        mDialog.dismiss();
                        Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                });

    }

    //NewNewsFeedCount
  /*  private void getNewPost(String newsId) {
        String authToken = sharedPreferences.getString(Constants.PREFTOKEN, "");

        //Here a logging interceptor is created
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        //The logging interceptor will be added to the http client
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        //The Retrofit builder will have the client attached, in order to get connection logs
        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.HIPSTO_BASE_URL)
                .build();

        HipstoAPI service = retrofit.create(HipstoAPI.class);

        Call<NewsPostResponse> call = service.newsPost(authToken, newsId);

        call.enqueue(new Callback<NewsPostResponse>() {
            @Override
            public void onResponse(Call<NewsPostResponse> call, Response<NewsPostResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        NewsPostResponse newsPostResponse = new NewsPostResponse();
                        newsPostResponse = response.body();
                        if (newsPostResponse.getCount() != 0) {
                            btnCount.setVisibility(View.VISIBLE);
                            btnCount.setText(newsPostResponse.getCount() + " New Content");
                           *//* if (newsPostResponse.getCount() == 1)
                                btnCount.setText(newsPostResponse.getCount() + " New Content");
                            else
                                btnCount.setText(newsPostResponse.getCount() + " New Contents");*//*
                        } else {
                            btnCount.setVisibility(View.GONE);
                        }

                    } else {
                        btnCount.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "Success" + response.body());
            }

            @Override
            public void onFailure(Call<NewsPostResponse> call, Throwable t) {
            }
        });

    }*/

    //NewNewsFeedCount
    private void getNewContent(String newsId) {
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        //editor.putString(Constants.SORTBY,"").apply();
        String sortBy = sharedPreferences.getString(Constants.SORTBY, "");
        myLangList.clear();
        if (!dbQueries.readSettingLang().isEmpty()) {
            for (int i = 0; i < dbQueries.readSettingLang().size(); i++) {
                myLangList.add(dbQueries.readSettingLang().get(i).getCode());
            }
        }
        myStreamList.clear();
        if (!(dbQueries.readSettingStream().size() > 0)) {
            dbQueries.insertSettingStream("media");
            dbQueries.insertSettingStream("twitter");
        }
        if (!dbQueries.readSettingStream().isEmpty()) {
            myStreamList = dbQueries.readSettingStream();
        }

        NewsData newsData = new NewsData();
        newsData.setId(newsId);
        newsData.setSources(myStreamList);
        newsData.setLangs(myLangList);
        newsData.setSearch("");
        newsData.setSort_by(sortBy);

        hipstoAPI.newsContent(authToken, newsData).enqueue(new Callback<NewsPostResponse>() {
            @Override
            public void onResponse(Call<NewsPostResponse> call, Response<NewsPostResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        NewsPostResponse newsPostResponse = new NewsPostResponse();
                        newsPostResponse = response.body();
                        if (newsPostResponse.getCount() != 0) {
                            btnCount.setVisibility(View.VISIBLE);
                            btnCount.setText(newsPostResponse.getCount() + " New Content");
                           /* if (newsPostResponse.getCount() == 1)
                                btnCount.setText(newsPostResponse.getCount() + " New Content");
                            else
                                btnCount.setText(newsPostResponse.getCount() + " New Contents");*/
                        } else {
                            btnCount.setVisibility(View.GONE);
                        }

                    } else {
                        btnCount.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "Success" + response.body());
            }

            @Override
            public void onFailure(Call<NewsPostResponse> call, Throwable t) {
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if(!network){
            if (Utilsdefault.isNetworkAvailable()) {
                trendingResponseList.clear();
                TrendDetailResponseList.clear();
                getTrendFeed();
                network = true;
            } else {
                network = false;
                if (getActivity() != null) {
                    Utilsdefault.alertNetwork(getActivity());
                }
            }
        }

    }

    //////////// EditText TextWatcher //////////////////////

     /* etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                getNewsFeed("", s.toString());
                if(s!=null){
                    getSearchNewsFeed("",s.toString().toLowerCase());
                }else{
                    getNewsFeed("",s.toString().toLowerCase());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

        });*/

    /////////// Nested ScrollView /////////////////

       /*    //ViewCompat.setNestedScrollingEnabled(nestedScrollView, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            nestedScrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                    if (view != null) {
                        if (scrollY > oldScrollY) {
                            rtPageTop.setVisibility(View.VISIBLE);
                            Log.i("Nested", "Scroll DOWN");
                        }
                        if (scrollY < oldScrollY) {
                            Log.i("Nested", "Scroll UP");
                        }

                        if (scrollY == 0) {
                            Log.i("Nested", "TOP SCROLL");
                            rtPageTop.setVisibility(View.GONE);
                        }


                        if ((scrollY >= (view.getMeasuredHeight() - nestedScrollView.getMeasuredHeight())) && scrollY > oldScrollY) {
                            if (Utilsdefault.isNetworkAvailable()) {
                                String lastId = String.valueOf(feedResponseArrayList.get(feedResponseArrayList.size() - 1).getMid());
                                getNewsFeed(lastId, "");
                            } else {
                                Utilsdefault.alertNetwork(getActivity());
                            }
                        }
                    }

                }
            });

        }*/

}
