package hipsto.app.orderfox.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import javax.inject.Inject;

import hipsto.app.orderfox.dagger.AppModule;
import hipsto.app.orderfox.dagger.ApplicationComponent;
import hipsto.app.orderfox.dagger.DaggerApplicationComponent;
import hipsto.app.orderfox.retrofit.RetrofitModule;
import hipsto.app.orderfox.utils.Constants;

public class HipstoApplication extends MultiDexApplication {

    @Inject
    public SharedPreferences sharedPreferences;
    private static HipstoApplication mInstance;

    public static HipstoApplication getContext() {
        return mInstance;
    }

    private ApplicationComponent mComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule(Constants.HIPSTO_BASE_URL, getContext()))
                .build();
        mComponent.inject(this);

    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }

    public static HipstoApplication from(@NonNull Context context) {
        return (HipstoApplication) context.getApplicationContext();
    }
}
