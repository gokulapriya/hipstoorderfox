package hipsto.app.orderfox.retrofit;

public class Fields {

    public static final class CommonField {
        public static final String APPID= "app_id";
        public static final String APIKEY= "api_key";
        public static final String AUTH= "Authorization";
        public static final String SOURCE= "sources";
        public static final String LANGS= "langs";
        public static final String SEARCH = "search";
        public static final String IDENTIFIRES= "identifiers";
        public static final String LISTOLDER= "list_older_than";
        public static final String NEWSID= "id";
        public static final String RATING= "rating";
        public static final String USER= "user";
        public static final String TOPICID= "topic_id";
        public static final String COMMENTS= "comment";
        public static final String REPORT= "report";
        public static final String lANGUAGE= "language";
        public static final String FEEDBACK= "feedback";

    }

}
