package hipsto.app.orderfox.retrofit;


import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.List;

import hipsto.app.orderfox.models.BadgeData;
import hipsto.app.orderfox.models.NewsData;
import hipsto.app.orderfox.models.NewsDetailResponse;
import hipsto.app.orderfox.models.NewsFeedResponse;
import hipsto.app.orderfox.models.NewsPostResponse;
import hipsto.app.orderfox.models.SettingsResponse;
import hipsto.app.orderfox.models.TrendingResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface HipstoAPI {

    @FormUrlEncoded
    @POST("application/token")
    Call<JsonElement> userAuth(@Field(Fields.CommonField.APPID) String appid,
                               @Field(Fields.CommonField.APIKEY) String apikey);

    @FormUrlEncoded
    @POST("v2/content")
    Call<NewsFeedResponse> newsFeeds(@Header(Fields.CommonField.AUTH) String auth,
                                     @Field(Fields.CommonField.SOURCE) String source,
                                     @Field(Fields.CommonField.LANGS) ArrayList<String> lang,
                                     @Field(Fields.CommonField.SEARCH) String search,
                                     @Field(Fields.CommonField.IDENTIFIRES) String identifiers,
                                     @Field(Fields.CommonField.LISTOLDER) String listoler);

    @POST("news")
    Call<NewsFeedResponse> newsFeeds1(@Header(Fields.CommonField.AUTH) String auth,
                                      @Header(Fields.CommonField.USER) String user,
                                      @Body NewsData data);

    @PUT("news/{id}/rate")
    Call<JsonElement> rating(@Header(Fields.CommonField.AUTH) String auth,
                             @Header(Fields.CommonField.USER) String user,
                             @Path(Fields.CommonField.NEWSID) String id,
                             @Body NewsData data);

    @GET("application/topic")
    Call<SettingsResponse> settingsRespo(@Header(Fields.CommonField.AUTH) String auth);

    @GET("links?autoreload=0&direction=desc&infinite=1&limit=5&set=2335&sort=rate_likes")
    Call<List<TrendingResponse>> trendingRespo(@Header("cookie") String cookie, @Query(Fields.CommonField.lANGUAGE) String lang);


    @GET("topic-settings/topic_id")
    Call<JsonElement> settingsTopicRespo(@Header(Fields.CommonField.AUTH) String auth, @Query("5b06e559800ae7afe8115608") String topi);

    @GET("news/{id}")
    Call<NewsDetailResponse> newsDetail(@Header(Fields.CommonField.AUTH) String auth,
                                        @Header(Fields.CommonField.USER) String user,
                                        @Path(Fields.CommonField.NEWSID) String id);

    @GET("check-new/{id}")
    Call<NewsPostResponse> newsPost(@Header(Fields.CommonField.AUTH) String auth,
                                    @Path(Fields.CommonField.NEWSID) String id);

    @POST("check-new")
    Call<NewsPostResponse> newsContent(@Header(Fields.CommonField.AUTH) String auth,
                                      @Body NewsData data);

    @FormUrlEncoded
    @PUT("bookmarks/{id}")
    Call<JsonElement> addBookmark(@Header(Fields.CommonField.AUTH) String auth,
                                  @Header(Fields.CommonField.USER) String user_id,
                                  @Path(Fields.CommonField.NEWSID) String id,
                                  @Field(Fields.CommonField.NEWSID) String newid);


    @DELETE("bookmarks/{id}")
    Call<JsonElement> removeBookmark(@Header(Fields.CommonField.AUTH) String auth,
                                     @Header(Fields.CommonField.USER) String user_id,
                                     @Path(Fields.CommonField.NEWSID) String id);

    @FormUrlEncoded
    @POST("bookmarks")
    Call<NewsFeedResponse> readBookmark(@Header(Fields.CommonField.AUTH) String auth,
                                        @Header(Fields.CommonField.USER) String user_id,
                                        @Field(Fields.CommonField.SEARCH) String search);


    @PUT("news/{id}/report")
    Call<JsonElement> addReport(@Header(Fields.CommonField.AUTH) String auth,
                                @Header(Fields.CommonField.USER) String user_id,
                                @Path(Fields.CommonField.NEWSID) String id,
                                @Body NewsData data);

    @PUT("application/feedback")
    Call<JsonElement> feedback(@Header(Fields.CommonField.AUTH) String auth,
                               @Header(Fields.CommonField.USER) String user_id,
                               @Body NewsData data);

    @PUT("news/{id}/view")
    Call<JsonElement> increaceViewcount(@Header(Fields.CommonField.AUTH) String auth,
                                        @Header(Fields.CommonField.USER) String user_id,
                                        @Path(Fields.CommonField.NEWSID) String id);

    @PUT("application/notifications")
    Call<JsonElement> notificationBadgeCount(@Header(Fields.CommonField.AUTH) String auth,
                                             @Header(Fields.CommonField.USER) String user_id,
                                             @Body BadgeData data);

}
