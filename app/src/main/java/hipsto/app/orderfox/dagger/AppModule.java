package hipsto.app.orderfox.dagger;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.retrofit.HipstoAPI;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

@Module
public class AppModule {

    public static final String HIPSTO_PREFS = "hipsto";

    private final HipstoApplication hisptoapp;

    public AppModule(HipstoApplication app) {
        this.hisptoapp = app;
    }
    //provides dependencies (return application class objects)
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return hisptoapp;
    }

    //provides dependencies (return sharedprference objects)
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return hisptoapp.getSharedPreferences(HIPSTO_PREFS, MODE_PRIVATE);
    }

    //provides dependencies (return API class objects)
    @Provides
    public HipstoAPI provideViduApiInterface(Retrofit retrofit) {
        return retrofit.create(HipstoAPI.class);
    }
}
