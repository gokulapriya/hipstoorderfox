package hipsto.app.orderfox.dagger;

import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Component;
import hipsto.app.orderfox.activity.HomeActivity;
import hipsto.app.orderfox.activity.SplashScreenActivity;
import hipsto.app.orderfox.activity.WebActivity;
import hipsto.app.orderfox.adapter.BookmarkListAdapter;
import hipsto.app.orderfox.adapter.LanguageAdapter;
import hipsto.app.orderfox.adapter.NewsListAdapter;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.fragment.BookmarkFragment;
import hipsto.app.orderfox.fragment.ExploreFragment;
import hipsto.app.orderfox.fragment.NewsDetailFragment;
import hipsto.app.orderfox.fragment.NewsFragment;
import hipsto.app.orderfox.fragment.SettingFragment;
import hipsto.app.orderfox.retrofit.RetrofitModule;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface ApplicationComponent {
    void inject(HipstoApplication application);

    SharedPreferences sharedPreferences();

    Retrofit retrofit();

    void inject(SplashScreenActivity splashScreenActivity);

    void inject(HomeActivity homeActivity);

    void inject(WebActivity webActivity);

    void inject(NewsFragment newsFragment);

    void inject(ExploreFragment exploreFragment);

    void inject(NewsDetailFragment newsDetailFragment);

    void inject(BookmarkFragment bookmarkFragment);

    void inject(SettingFragment settingFragment);

    void inject(NewsListAdapter newsListAdapter);

    void inject(BookmarkListAdapter bookmarkListAdapter);

    void inject(LanguageAdapter languageAdapter);

}
