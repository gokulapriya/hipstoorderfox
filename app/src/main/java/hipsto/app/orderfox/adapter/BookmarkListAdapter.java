package hipsto.app.orderfox.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.Sql.DBQueries;
import hipsto.app.orderfox.activity.HomeActivity;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.fragment.BookmarkFragment;
import hipsto.app.orderfox.fragment.NewsDetailFragment;
import hipsto.app.orderfox.models.Bookmark;
import hipsto.app.orderfox.models.NewsData;
import hipsto.app.orderfox.models.NewsFeedResponse;
import hipsto.app.orderfox.retrofit.HipstoAPI;
import hipsto.app.orderfox.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Gokulapriya on 11/6/2019.
 */

public class BookmarkListAdapter extends RecyclerView.Adapter<BookmarkListAdapter.MyViewHolder> {

    private List<NewsFeedResponse.ItemsItem/*Bookmark*/> bookmarkList;
    private Context context;
    private BookmarkFragment bookmarkFragment;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public HipstoAPI hipstoAPI;
    //private AddTouchListen addTouchListen;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ArrayList<String> DetailResponseList = new ArrayList<>();


    public BookmarkListAdapter(Context context, List<NewsFeedResponse.ItemsItem/*Bookmark*/> newArrayList, BookmarkFragment bookmarkFragment) {
        this.context = context;
        this.bookmarkList = newArrayList;
        this.bookmarkFragment = bookmarkFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
        return new MyViewHolder(itemView);
    }

    /*   public interface AddTouchListen {
           public void onTouchClick(int position);
       }

       public void setOnClickListen(AddTouchListen addTouchListen) {
           this.addTouchListen = addTouchListen;
       }
   */
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        HipstoApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        final DBQueries dbQueries = new DBQueries(context);
        dbQueries.open(context);

        Glide.with(context).load(bookmarkList.get(position).getDataImageMain()).apply(RequestOptions.placeholderOf(R.mipmap.placeholder_news)).into(holder.domainImage);

        if(bookmarkList.get(position).getDataTitle() != null){
            holder.mTitle.setText(bookmarkList.get(position).getDataTitle().trim());
        }else{
            holder.mTitle.setText("ORDERFOX news");
        }

        if(bookmarkList.get(position).getSource().getDomain() != null){
            holder.mDomain.setText("@" + bookmarkList.get(position).getSource().getDomain());
        }else{
            holder.mDomain.setText("");
        }


        //holder.mTitle.setText(bookmarkList.get(position).getData_title().trim());
        holder.mTrend.setText(bookmarkList.get(position).getSourceName());
        //holder.mTrend.setText(bookmarkList.get(position).getSource_name());

        //holder.mDomain.setText("@" + bookmarkList.get(position).getDomain());

        String date = bookmarkList.get(position).getDataPublished();
        //String date = bookmarkList.get(position).getData_published();
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        parser.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsed = null;
        try {
            parsed = parser.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        parser = new SimpleDateFormat("dd MMM");
        date = parser.format(parsed);
        System.out.println(date);
        holder.mDate.setText(parser.format(parsed));
        holder.mTime.setText(bookmarkList.get(position).getTimeRead());
        //holder.mTime.setText(bookmarkList.get(position).getTime_read());

        //API report
        holder.ivBookmark.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bookmark_blue_shape));

        //DB Bookmark
        if (dbQueries.readBookmarkId().contains(bookmarkList.get(position).getMid())) {
            holder.ivBookmark.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bookmark_blue_shape));
        } else {
            holder.ivBookmark.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bookmark_gray_shape));
        }

        //DB Read/UnRead
        Log.e("news id", "> " + dbQueries.readNewsFeedId().toString());
        if (dbQueries.readNewsFeedId().contains(bookmarkList.get(position).getMid())) {
            holder.headerView.setBackgroundColor(context.getResources().getColor(R.color.colorGrayMedium));
        } else {
            holder.headerView.setBackgroundColor(context.getResources().getColor(R.color.appType));
        }

        //API report
        if (bookmarkList.get(position).getHipsto_own_reports() != null) {
            holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_red));
        } else {
            holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_grey));
        }

        //DB report
        if (dbQueries.readReportId().contains(bookmarkList.get(position).getMid())) {
            holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_red));
        } else {
            holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_grey));
        }

        //DB Share
        if (dbQueries.readShareId().contains(bookmarkList.get(position).getMid())) {
            holder.ivShare.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_share_green));
        } else {
            holder.ivShare.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_share_grey));
        }

        //API Rating
        holder.rbRating.setRating(bookmarkList.get(position).getHipstoOwnRating());

        //DB Rating
        for (int i = 0; i < dbQueries.readRating().size(); i++) {
            if (dbQueries.readRating().get(i).getNewsId().contains(bookmarkList.get(position).getMid())) {
                holder.rbRating.setRating(dbQueries.readRating().get(i).getRating());
                break;
            }
        }

        holder.rtBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!bookmarkList.get(position).getHipsto_own_bookmark_id().equals("")) {

                    final Bookmark bookmark = new Bookmark(bookmarkList.get(position).getTopicCode(),
                            bookmarkList.get(position).getMid(), bookmarkList.get(position).getDataImageMain(),
                            bookmarkList.get(position).getItemUrl(), bookmarkList.get(position).getDataTitle(),
                            bookmarkList.get(position).getDataContent(), bookmarkList.get(position).getDataContentFull(),
                            bookmarkList.get(position).getSource().getDomain(), bookmarkList.get(position).getSource().getUrl(),
                            bookmarkList.get(position).getDataPublished(), bookmarkList.get(position).getSourceName(),
                            bookmarkList.get(position).getSourceLang(), bookmarkList.get(position).getHipstoOwnRating(),
                            bookmarkList.get(position).getHipstoOwnViews(), bookmarkList.get(position).getHipsto_own_bookmark_id(), bookmarkList.get(position).getTimeRead());

           /*     Bookmark bookmark = new Bookmark(bookmarkList.get(position).getTopicCode(),
                        bookmarkList.get(position).getMid(), bookmarkList.get(position).getData_image_main(),
                        bookmarkList.get(position).getItem_url(), bookmarkList.get(position).getData_title(),
                        bookmarkList.get(position).getDataContent(), bookmarkList.get(position).getData_content_full(),
                        bookmarkList.get(position).getDomain(), bookmarkList.get(position).getUrl(),
                        bookmarkList.get(position).getData_published(), bookmarkList.get(position).getSource_name(),
                        bookmarkList.get(position).getSource_lang(), bookmarkList.get(position).getHipsto_own_rating(),
                        bookmarkList.get(position).getHipsto_own_views(), bookmarkList.get(position).getTime_read());*/

                    InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(holder.ivBookmark.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
                    String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "")*/dbQueries.readLoginID(1).getUser_id_encode();


                    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                    logging.setLevel(HttpLoggingInterceptor.Level.BODY);

                    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                    httpClient.addInterceptor(logging);


                    Retrofit retrofit = new Retrofit.Builder()
                            .client(httpClient.build())
                            .addConverterFactory(GsonConverterFactory.create())
                            .baseUrl(Constants.HIPSTO_BASE_URL)
                            .build();

                    HipstoAPI service = retrofit.create(HipstoAPI.class);

                    Call<JsonElement> call = service.removeBookmark(authToken, userEncode.trim(), bookmarkList.get(position).getMid());

                    call.enqueue(new Callback<JsonElement>() {
                        @Override
                        public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                            try {
                                JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                                Boolean status = jsonObject.getBoolean("success");
                                if (status) {

                                    /*Firebase Event log trend*/
                                    mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                                    Bundle bun = new Bundle();
                                    bun.putString(FirebaseAnalytics.Param.ITEM_ID, bookmarkList.get(position).getMid());
                                    bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                    bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Bookmark_remove - Bookmark list");
                                    bun.putString(FirebaseAnalytics.Param.CONTENT, bookmarkList.get(position).getDataTitle());
                                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);

                                    dbQueries.deleteBookmarkId(bookmarkList.get(position).getMid());
                                    dbQueries.deleteBookmark(bookmark);
                                    Toast.makeText(context, context.getResources().getString(R.string.bookmark_add_remove), Toast.LENGTH_LONG).show();
                                    holder.ivBookmark.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bookmark_gray_shape));
                                    bookmarkFragment.removeItemAtPosition(position);
                                } else {
                                    Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                if (context != null) {
                                    Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<JsonElement> call, Throwable t) {
                            if (context != null) {
                                Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                            }
                        }

                    });

                } else {
                    if (context != null) {
                        Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        holder.rtShareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookmarkFragment.rtShare.setVisibility(View.VISIBLE);
                bookmarkFragment.rtSearch.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(holder.ivShare.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                bookmarkFragment.ShareClickListener(bookmarkList.get(position).getMid(), bookmarkList.get(position).getDataTitle(),holder.ivShare);
            }
        });

        holder.rtReportIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dbQueries.readReportId().contains(bookmarkList.get(position).getMid()) || bookmarkList.get(position).getHipsto_own_reports() != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(R.string.reportabuse_disable);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            reportAPI(bookmarkList.get(position).getMid(), "", holder, "remove", dbQueries);
                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                            Bundle bun = new Bundle();
                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, bookmarkList.get(position).getMid());
                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Report remove- Bookmark list");
                            bun.putString(FirebaseAnalytics.Param.CONTENT, bookmarkList.get(position).getDataTitle());
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                            dialog.dismiss();
                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alert = builder.create();

                    alert.show();
                    alert.setCanceledOnTouchOutside(false);

                } else {
                    //API
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.report_dialog);
                    final TextView tvFake = dialog.findViewById(R.id.report_fake);
                    final TextView tvInapprop = dialog.findViewById(R.id.report_inapprop);
                    dialog.show();
                    tvFake.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            reportAPI(bookmarkList.get(position).getMid(), tvFake.getText().toString(), holder, "add", dbQueries);
                            dialog.dismiss();
                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                            Bundle bun = new Bundle();
                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, bookmarkList.get(position).getMid());
                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Report add- Bookmark list");
                            bun.putString(FirebaseAnalytics.Param.CONTENT, bookmarkList.get(position).getDataTitle());
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                        }
                    });
                    tvInapprop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            reportAPI(bookmarkList.get(position).getMid(), tvInapprop.getText().toString().trim(), holder, "add", dbQueries);
                            dialog.dismiss();
                            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                            Bundle bun = new Bundle();
                            bun.putString(FirebaseAnalytics.Param.ITEM_ID, bookmarkList.get(position).getMid());
                            bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                            bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Report add- Bookmark list");
                            bun.putString(FirebaseAnalytics.Param.CONTENT, bookmarkList.get(position).getDataTitle());
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);

                        }
                    });
                }

            }
        });

        holder.cvNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment newsDetailFragment = new NewsDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("pageStatus", "2");
                bundle.putString("newsId", bookmarkList.get(position).getMid());
                bundle.putInt("position", position);
                for (int i = 0; i < bookmarkList.size(); i++) {
                    DetailResponseList.add(bookmarkList.get(i).getMid());
                }
                bundle.putStringArrayList("DetailIdList", DetailResponseList);
                newsDetailFragment.setArguments(bundle);
                ((HomeActivity) context).loadFragment(newsDetailFragment);
                    /*if (addTouchListen != null) {
                        addTouchListen.onTouchClick(position);
                    }*/
            }
        });

        holder.swipeNews.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
            @Override
            public void onClosed(SwipeRevealLayout view) {

            }

            @Override
            public void onOpened(SwipeRevealLayout view) {
                Fragment newsDetailFragment = new NewsDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("pageStatus", "2");
                bundle.putString("newsId", bookmarkList.get(position).getMid());
                bundle.putInt("position", position);
                for (int i = 0; i < bookmarkList.size(); i++) {
                    DetailResponseList.add(bookmarkList.get(i).getMid());
                }
                bundle.putStringArrayList("DetailIdList", DetailResponseList);
                newsDetailFragment.setArguments(bundle);
                ((HomeActivity) context).loadFragment(newsDetailFragment);
            }

            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset) {

            }
        });



    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return bookmarkList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView domainImage, ivBookmark, ivShare, ivReport;
        RelativeLayout rtBookmark, rtShareIcon, rtReportIcon;
        TextView mTitle, mDomain, mDate, mTime, mTrend;
        View headerView;
        RatingBar rbRating;
        SwipeRevealLayout swipeNews;
        FrameLayout flNews;
        CardView cvNews;

        public MyViewHolder(View view) {
            super(view);
            cvNews = view.findViewById(R.id.cv_news);
            domainImage = view.findViewById(R.id.iv_news_img);
            swipeNews = view.findViewById(R.id.swipe_news);
            flNews = view.findViewById(R.id.fl_news);
            rtBookmark = view.findViewById(R.id.rt_bookmark_icon);
            rtShareIcon = view.findViewById(R.id.rt_share_icon);
            rtReportIcon = view.findViewById(R.id.rt_flag_icon);
            ivBookmark = view.findViewById(R.id.iv_bookmark);
            mTitle = view.findViewById(R.id.tv_news_topic);
            mTrend = view.findViewById(R.id.tv_getty);
            mDomain = view.findViewById(R.id.tv_news_domain);
            mDate = view.findViewById(R.id.tv_date);
            mTime = view.findViewById(R.id.tv_time);
            ivShare = view.findViewById(R.id.iv_share);
            headerView = view.findViewById(R.id.vw_news);
            rbRating = view.findViewById(R.id.rb);
            ivReport = view.findViewById(R.id.iv_flag);
        }
    }

    private void reportAPI(final String id, String report, final MyViewHolder holder, final String flag, final DBQueries dbQueries) {

        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(holder.ivReport.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "")*/dbQueries.readLoginID(1).getUser_id_encode();
        NewsData newsData = new NewsData();
        newsData.setReport(report);
        hipstoAPI.addReport(authToken, userEncode.trim(), id.trim(), newsData).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    Boolean status = jsonObject.getBoolean("success");
                    if (status) {
                        if (flag.equals("add")) {
                            holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_red));

                            Toast.makeText(context, context.getResources().getString(R.string.update_success), Toast.LENGTH_LONG).show();
                            //reportedNewsList.add(id);

                            dbQueries.insertReportId(id);
                        } else {
                            holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_grey));
                            Toast.makeText(context, context.getResources().getString(R.string.update_success), Toast.LENGTH_LONG).show();
                            //reportedNewsList.remove(id);

                            dbQueries.deleteReportId(id);
                        }
                    } else {
                        holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_grey));
                        Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (context != null) {
                        Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                if (context != null) {
                    Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                }
            }

        });
    }
}