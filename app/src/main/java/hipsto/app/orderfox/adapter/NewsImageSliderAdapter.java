package hipsto.app.orderfox.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.models.TrendingResponse;

/**
 * Created by Gokulapriya on 7/20/2019.
 */

public class NewsImageSliderAdapter extends PagerAdapter {


    private ArrayList<TrendingResponse> newArrayList;
    private LayoutInflater inflater;
    private Context context;
    private AddTouchListen addTouchListen;

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }


    public NewsImageSliderAdapter(Context context, ArrayList<TrendingResponse> newList) {
        this.context = context;
        this.newArrayList = newList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return newArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.item_news_image_slider, view, false);

        assert imageLayout != null;
        final ImageView ivNewsImage = imageLayout.findViewById(R.id.iv_news_image);
        final ImageView ivNewsBookmark = imageLayout.findViewById(R.id.iv_news_bookmark);
        final TextView tvNewsDate = imageLayout.findViewById(R.id.tv_news_date);
        final TextView tvNewsTitle = imageLayout.findViewById(R.id.tv_news_title);

        Glide.with(context).load(newArrayList.get(position).getImage()).apply(RequestOptions.placeholderOf(R.mipmap.placeholder_detail)).into(ivNewsImage);


      //  ivNewsImage.setImageResource(newArrayList.get(position).getImage());

      /*  if(newArrayList.get(position).getBookmark() == 1){
            ivNewsBookmark.setVisibility(View.VISIBLE);
        }else{
            ivNewsBookmark.setVisibility(View.INVISIBLE);
        }*/
      /*  String date = newArrayList.get(position).getFound();
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        parser.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsed = null;
        try {
            parsed = parser.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        parser = new SimpleDateFormat("dd MMM yyyy");
      //  SimpleDateFormat parser1 = new SimpleDateFormat("hh:mm");
        date = parser.format(parsed);
        System.out.println(date);
        tvNewsDate.setText(parser.format(parsed));*/
      //  tvNewsDate.setText(newArrayList.get(position).getFound());
        tvNewsTitle.setText(Html.fromHtml(Html.fromHtml(newArrayList.get(position).getTitle().trim()).toString()));

        imageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });

        view.addView(imageLayout, 0);


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}
