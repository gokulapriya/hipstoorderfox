package hipsto.app.orderfox.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.Sql.DBQueries;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.models.TopicLangResponse;
import hipsto.app.orderfox.utils.Constants;

/**
 * Created by Gokulapriya on 10/6/2019.
 */

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.MyViewHolder> {

    private List<TopicLangResponse> newsArrayList;
    private Context context;
    static Boolean isTouched = false;
    List<TopicLangResponse> strings = new ArrayList<>();
    List<String> allLanguageList = new ArrayList<>();
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    public LanguageAdapter(Context context, List<TopicLangResponse> newArrayList) {
        this.context = context;
        this.newsArrayList = newArrayList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_language, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        HipstoApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        final DBQueries dbQueries = new DBQueries(context);
        dbQueries.open(context);

        if (position == 0 ) {
            if(newsArrayList.get(position).getCode().equals("en")){
                holder.tvLanguage.setText(newsArrayList.get(position).getNative());
            }else {
                holder.tvLanguage.setText(newsArrayList.get(position).getNative() + "(" + newsArrayList.get(position).getName() + ")");
            }

            if(dbQueries.readSettingLang().size()==0){

                TopicLangResponse topicLangResponse = new TopicLangResponse(
                        newsArrayList.get(position).getCode(),
                        newsArrayList.get(position).getName(),
                        newsArrayList.get(position).getNative());
                dbQueries.insertSettingLang(topicLangResponse);
            }
        } else {
            holder.tvLanguage.setText(newsArrayList.get(position).getNative() + "(" + newsArrayList.get(position).getName() + ")");
        }

        for (int i = 0; i < dbQueries.readSettingLang().size(); i++) {
                if (dbQueries.readSettingLang().get(i).getCode().equals(newsArrayList.get(position).getCode())) {
                    holder.switchCompat.setChecked(true);
                }
            }
            holder.switchCompat.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    isTouched = true;
                    return false;
                }
            });


            holder.switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        TopicLangResponse topicLangResponse = new TopicLangResponse(
                                newsArrayList.get(position).getCode(),
                                newsArrayList.get(position).getName(),
                                newsArrayList.get(position).getNative());
                        dbQueries.insertSettingLang(topicLangResponse);


                        System.out.println("Hii language "+dbQueries.readSettingLang().size()+"--Array--"+dbQueries.readSettingLang().toString());
                        allLanguageList.clear();
                        for (int i=0;i<dbQueries.readSettingLang().size();i++){
                            allLanguageList.add(dbQueries.readSettingLang().get(i).getName());
                        }
                        editor.putString(Constants.ALLLANGUAGE,allLanguageList.toString()).apply();

                    } else {
                        if (dbQueries.readSettingLang().size() > 1) {
                            TopicLangResponse topicLangResponse = new TopicLangResponse(
                                    newsArrayList.get(position).getCode(),
                                    newsArrayList.get(position).getName(),
                                    newsArrayList.get(position).getNative());
                            dbQueries.deleteSettingsLang(topicLangResponse);

                            System.out.println("Hii language "+dbQueries.readSettingLang().size()+"--Array--"+dbQueries.readSettingLang().toString());
                            allLanguageList.clear();
                            for (int i=0;i<dbQueries.readSettingLang().size();i++){
                                allLanguageList.add(dbQueries.readSettingLang().get(i).getName());
                            }
                            editor.putString(Constants.ALLLANGUAGE,allLanguageList.toString()).apply();

                        } else {
                            holder.switchCompat.setChecked(true);
                            Toast.makeText(context, "You need to enable atleast one Language", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }

        @Override
        public int getItemViewType ( int position){
            return position;
        }

        @Override
        public int getItemCount () {
            return newsArrayList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tvLanguage;
            SwitchCompat switchCompat;

            public MyViewHolder(View view) {
                super(view);
                tvLanguage = view.findViewById(R.id.tv_language);
                switchCompat = view.findViewById(R.id.sw_language);

            }
        }

    }