package hipsto.app.orderfox.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.Sql.DBHelper;
import hipsto.app.orderfox.Sql.DBQueries;
import hipsto.app.orderfox.activity.HomeActivity;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.fragment.NewsDetailFragment;
import hipsto.app.orderfox.fragment.NewsFragment;
import hipsto.app.orderfox.models.Bookmark;
import hipsto.app.orderfox.models.NewsData;
import hipsto.app.orderfox.models.NewsFeedResponse;
import hipsto.app.orderfox.retrofit.HipstoAPI;
import hipsto.app.orderfox.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Gokulapriya on 10/6/2019.
 */

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.MyViewHolder>{

    List<NewsFeedResponse.ItemsItem> newsArrayList;
    private Context context;
    NewsFragment newsFragment;
    int AD_TYPE = 0;
    int CONTENT_TYPE = 1;
    //private AddTouchListen addTouchListen;
    private ArrayList<String> DetailResponseList = new ArrayList<>();
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public HipstoAPI hipstoAPI;
    private FirebaseAnalytics mFirebaseAnalytics;

    public NewsListAdapter(Context context, List<NewsFeedResponse.ItemsItem> newArrayList, NewsFragment newsFragment) {
        this.context = context;
        this.newsArrayList = newArrayList;
        this.newsFragment = newsFragment;
    }

    /*public interface AddTouchListen {
        public void onTouchClick(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }*/


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
/*
        View v = null;
      *//*  Appodeal.setTesting(false);
        Appodeal.setLogLevel(com.appodeal.ads.utils.Log.LogLevel.debug);

        Appodeal.getUserSettings(context)
                .setAge(25)
                .setGender(UserSettings.Gender.OTHER);*//*
        Appodeal.setAutoCache(Appodeal.BANNER, true);
        Appodeal.setBannerViewId(R.id.appodealBannerView);
        //Appodeal.initialize((Activity) context, "b878181640b3a5d461f4a658379295af81373a42c059f30d", Appodeal.BANNER, true);
        Appodeal.setBannerCallbacks(new AppodealBannerCallbacks((Activity) context));


        if (Appodeal.isLoaded(Appodeal.BANNER)) {
            System.out.println("isLoaded --> True");
        } else {
            System.out.println("isLoaded --> False");
        }
        if (viewType == AD_TYPE) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_banner, parent, false);

            boolean isShown = Appodeal.show((Activity) context, Appodeal.BANNER_VIEW);
            System.out.println("isShow --> " + isShown);
            return new MyViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
            return new MyViewHolder(v);
        }*/


        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
       return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {

            HipstoApplication.getContext().getComponent().inject(this);
            editor = sharedPreferences.edit();

            Glide.with(context).load(newsArrayList.get(position).getDataImageMain()).apply(RequestOptions.placeholderOf(R.mipmap.placeholder_news)).into(holder.domainImage);
            holder.mTitle.setText(newsArrayList.get(position).getDataTitle().trim());
            holder.mTrend.setText(newsArrayList.get(position).getSourceName());
            holder.mDomain.setText("@" + newsArrayList.get(position).getSource().getDomain());

            String date = newsArrayList.get(position).getDataPublished();
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            parser.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date parsed = null;
            try {
                parsed = parser.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            parser = new SimpleDateFormat("dd MMM");
            SimpleDateFormat parser1 = new SimpleDateFormat("hh:mm");
            date = parser.format(parsed);
            System.out.println(date);
            holder.mDate.setText(parser.format(parsed));
            holder.mTime.setText(newsArrayList.get(position).getTimeRead());
            DBHelper dbHelper = new DBHelper(context);
            final DBQueries dbQueries = new DBQueries(context);
            dbQueries.open(context);

            //API Bookmark
            if (!newsArrayList.get(position).getHipsto_own_bookmark_id().equals("")) {
                holder.ivBookmark.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bookmark_blue_shape));
            } else {
                holder.ivBookmark.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bookmark_gray_shape));
            }

            //DB Bookmark
            if (dbQueries.readBookmarkId().contains(newsArrayList.get(position).getMid())) {
                holder.ivBookmark.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bookmark_blue_shape));
            } else {
                holder.ivBookmark.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bookmark_gray_shape));
            }

            //API report
            if (newsArrayList.get(position).getHipsto_own_reports() != null) {
                holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_red));
            } else {
                holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_grey));
            }

            //DB Report
            if (dbQueries.readReportId().contains(newsArrayList.get(position).getMid())) {
                holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_red));
            } else {
                holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_grey));
            }

            //DB Read/unRead
            if (dbQueries.readNewsFeedId().contains(newsArrayList.get(position).getMid())) {
                holder.headerView.setBackgroundColor(context.getResources().getColor(R.color.colorGrayMedium));
            } else {
                holder.headerView.setBackgroundColor(context.getResources().getColor(R.color.appType));
            }

            //DB Share
            if (dbQueries.readShareId().contains(newsArrayList.get(position).getMid())) {
                holder.ivShare.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_share_green));
            } else {
                holder.ivShare.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_share_grey));
            }

            //API Rating
            holder.mRating.setRating(newsArrayList.get(position).getHipstoOwnRating());

            //DB Rating
            for (int i = 0; i < dbQueries.readRating().size(); i++) {
                if (dbQueries.readRating().get(i).getNewsId().contains(newsArrayList.get(position).getMid())) {
                    holder.mRating.setRating(dbQueries.readRating().get(i).getRating());
                    break;
                }
            }

            holder.rtBookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Bookmark bookmark = new Bookmark(newsArrayList.get(position).getTopicCode(),
                            newsArrayList.get(position).getMid(), newsArrayList.get(position).getDataImageMain(),
                            newsArrayList.get(position).getItemUrl(), newsArrayList.get(position).getDataTitle(),
                            newsArrayList.get(position).getDataContent(), newsArrayList.get(position).getDataContentFull(),
                            newsArrayList.get(position).getSource().getDomain(), newsArrayList.get(position).getSource().getUrl(),
                            newsArrayList.get(position).getDataPublished(), newsArrayList.get(position).getSourceName(),
                            newsArrayList.get(position).getSourceLang(), newsArrayList.get(position).getHipstoOwnRating(),
                            newsArrayList.get(position).getHipstoOwnViews(), newsArrayList.get(position).getHipsto_own_bookmark_id(), newsArrayList.get(position).getTimeRead());

                    InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(holder.ivBookmark.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    if (!newsArrayList.get(position).getHipsto_own_bookmark_id().equals("") || dbQueries.readBookmarkId().contains(newsArrayList.get(position).getMid())) {

                        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
                        String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

                        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

                        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                        httpClient.addInterceptor(logging);


                        Retrofit retrofit = new Retrofit.Builder()
                                .client(httpClient.build())
                                .addConverterFactory(GsonConverterFactory.create())
                                .baseUrl(Constants.HIPSTO_BASE_URL)
                                .build();

                        HipstoAPI service = retrofit.create(HipstoAPI.class);

                        Call<JsonElement> call = service.removeBookmark(authToken, userEncode.trim(), newsArrayList.get(position).getMid());

                        call.enqueue(new Callback<JsonElement>() {
                            @Override
                            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                                    Boolean status = jsonObject.getBoolean("success");
                                    if (status) {
                                        dbQueries.deleteBookmarkId(newsArrayList.get(position).getMid());
                                        dbQueries.deleteBookmark(bookmark);
                                        Toast.makeText(context, context.getResources().getString(R.string.bookmark_add_remove), Toast.LENGTH_LONG).show();
                                        holder.ivBookmark.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bookmark_gray_shape));
                                        /*Firebase Event log trend*/
                                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                                        Bundle bun = new Bundle();
                                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, newsArrayList.get(position).getMid());
                                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Bookmark_remove - News list");
                                        bun.putString(FirebaseAnalytics.Param.CONTENT, newsArrayList.get(position).getDataTitle());
                                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                    } else {
                                        Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (context != null) {
                                        Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                                    }
                                }

                            }

                            @Override
                            public void onFailure(Call<JsonElement> call, Throwable t) {
                                if (context != null) {
                                    Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                                }
                            }

                        });


                    } else if (newsArrayList.get(position).getHipsto_own_bookmark_id().equals("") && !dbQueries.readBookmarkId().contains(newsArrayList.get(position).getMid())) {

                        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
                        String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

                        hipstoAPI.addBookmark(authToken, userEncode.trim(), bookmark.getMid(), bookmark.getMid()).enqueue(new Callback<JsonElement>() {
                            @Override
                            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                                    Boolean status = jsonObject.getBoolean("success");
                                    if (status) {
                                        String bookmark_id = jsonObject.getString("bookmark_id");
                                        dbQueries.insertBookmark(bookmark);
                                        dbQueries.insertBookmarkId(bookmark.getMid());
                                        Toast.makeText(context, context.getResources().getString(R.string.bookmark_add_success), Toast.LENGTH_LONG).show();
                                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                                        Bundle bun = new Bundle();
                                        bun.putString(FirebaseAnalytics.Param.ITEM_ID, newsArrayList.get(position).getMid());
                                        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Bookmark_add - Newslist");
                                        bun.putString(FirebaseAnalytics.Param.CONTENT, newsArrayList.get(position).getDataTitle());
                                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                                        holder.ivBookmark.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bookmark_blue_shape));
                                    } else {
                                        Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (context != null) {
                                        Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                                    }
                                }

                            }

                            @Override
                            public void onFailure(Call<JsonElement> call, Throwable t) {
                                if (context != null) {
                                    Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                                }
                            }

                        });
                    }

                }
            });

        holder.cvNews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Fragment newsDetailFragment = new NewsDetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("pageStatus", "2");
                    bundle.putString("newsId", newsArrayList.get(position).getMid());
                    bundle.putInt("position", position);
                    for (int i = 0; i < newsArrayList.size(); i++) {
                        DetailResponseList.add(newsArrayList.get(i).getMid());
                    }
                    bundle.putStringArrayList("DetailIdList", DetailResponseList);
                    newsDetailFragment.setArguments(bundle);
                    ((HomeActivity) context).loadFragment(newsDetailFragment);
                    /*if (addTouchListen != null) {
                        addTouchListen.onTouchClick(position);
                    }*/
                }
            });


            holder.swipeNews.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
                @Override
                public void onClosed(SwipeRevealLayout view) {

                }

                @Override
                public void onOpened(SwipeRevealLayout view) {
                    Fragment newsDetailFragment = new NewsDetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("pageStatus", "2");
                    bundle.putString("newsId", newsArrayList.get(position).getMid());
                    bundle.putInt("position", position);
                    for (int i = 0; i < newsArrayList.size(); i++) {
                        DetailResponseList.add(newsArrayList.get(i).getMid());
                    }
                    bundle.putStringArrayList("DetailIdList", DetailResponseList);
                    newsDetailFragment.setArguments(bundle);
                    ((HomeActivity) context).loadFragment(newsDetailFragment);
                }

                @Override
                public void onSlide(SwipeRevealLayout view, float slideOffset) {

                }
            });



            holder.rtShareIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newsFragment.rtShare.setVisibility(View.VISIBLE);

                    InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(holder.ivShare.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    newsFragment.ShareClickListener(newsArrayList.get(position).getMid(), newsArrayList.get(position).getDataTitle(),holder.ivShare);
                }
            });

            holder.rtReportIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dbQueries.readReportId().contains(newsArrayList.get(position).getMid()) || newsArrayList.get(position).getHipsto_own_reports() != null) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(R.string.reportabuse_disable);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                reportAPI(newsArrayList.get(position).getMid(), "", holder, "remove", dbQueries);
                                dialog.dismiss();
                                mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                                Bundle bun = new Bundle();
                                bun.putString(FirebaseAnalytics.Param.ITEM_ID, newsArrayList.get(position).getMid());
                                bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Report remove- News feed list");
                                bun.putString(FirebaseAnalytics.Param.CONTENT, newsArrayList.get(position).getDataTitle());
                                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        final AlertDialog alert = builder.create();

                        alert.show();
                        alert.setCanceledOnTouchOutside(false);

                        /*final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.dialog_rating);

                        final LinearLayout ltRating = dialog.findViewById(R.id.lt_rating);
                        final LinearLayout ltAlert = dialog.findViewById(R.id.lt_alert);
                        final TextView tvAlertMsg = dialog.findViewById(R.id.tv_alert_msg);
                        TextView tvOkay = dialog.findViewById(R.id.tv_okay);

                        ltAlert.setVisibility(View.VISIBLE);
                        ltRating.setVisibility(View.GONE);
                        tvAlertMsg.setText(context.getResources().getString(R.string.reported_already));
                        dialog.show();
                        tvOkay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });*/
                    } else {
                        //API
                        final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(true);
                        dialog.setContentView(R.layout.report_dialog);
                        final TextView tvFake = dialog.findViewById(R.id.report_fake);
                        final TextView tvInapprop = dialog.findViewById(R.id.report_inapprop);
                        dialog.show();
                        tvFake.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                reportAPI(newsArrayList.get(position).getMid(), tvFake.getText().toString(), holder, "add", dbQueries);
                                dialog.dismiss();
                                mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                                Bundle bun = new Bundle();
                                bun.putString(FirebaseAnalytics.Param.ITEM_ID, newsArrayList.get(position).getMid());
                                bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Report add- News feed list");
                                bun.putString(FirebaseAnalytics.Param.CONTENT, newsArrayList.get(position).getDataTitle());
                                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
                            }
                        });
                        tvInapprop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                reportAPI(newsArrayList.get(position).getMid(), tvInapprop.getText().toString().trim(), holder, "add", dbQueries);
                                dialog.dismiss();
                                mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                                Bundle bun = new Bundle();
                                bun.putString(FirebaseAnalytics.Param.ITEM_ID, newsArrayList.get(position).getMid());
                                bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
                                bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Report add- News feed list");
                                bun.putString(FirebaseAnalytics.Param.CONTENT, newsArrayList.get(position).getDataTitle());
                                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);

                            }
                        });
                    }

                    //DB
                   /* reportIdList = dbQueries.readReportId();
                    //dialogReportnews(newsArrayList.get(position).getMid(),holder);
                    if (reportIdList.contains(newsArrayList.get(position).getMid())) {
                        dialogReportnews(newsArrayList.get(position).getMid());
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(R.string.reportabuse_disable);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dbQueries.deleteReportId(newsArrayList.get(position).getMid());
                                holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_grey));
                                dialog.dismiss();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        final AlertDialog alert = builder.create();

                       *//* alert.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface arg0) {
                                alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                                alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));

                            }
                        });*//*

                        alert.show();
                        alert.setCanceledOnTouchOutside(false);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(R.string.reportabuse);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                //   dialogReportnews(newsArrayList.get(position).getMid());
                                dbQueries.insertReportId(newsArrayList.get(position).getMid());
                                holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_red));
                                // notifyDataSetChanged();

                                dbQueries.insertReportId(newsArrayList.get(position).getMid());
                                holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_red));
                                // notifyDataSetChanged();

                                dialog.dismiss();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        final AlertDialog alert = builder.create();
*//*
                        alert.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface arg0) {
                                alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                                alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));

                            }
                        });
*//*
                        alert.show();
                        alert.setCanceledOnTouchOutside(false);
                    }*/
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
       /* if(position % 10 != 0){
            if (position != 0 && position % 5 == 0 ){
                return AD_TYPE;
            }else{
                return CONTENT_TYPE;
            }
        }else{
            return CONTENT_TYPE;
        }*/
        return CONTENT_TYPE;
    }

    @Override
    public int getItemCount() {
        return newsArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView domainImage, ivBookmark, ivShare, ivReport;
        RelativeLayout rtBookmark, rtShareIcon, rtReportIcon;
        TextView mTitle, mDomain, mDate, mTime, mTrend;
        RatingBar mRating;
        View headerView;
        SwipeRevealLayout swipeNews;
        FrameLayout flNews;
        CardView cvNews;

        public MyViewHolder(View view) {
            super(view);
            cvNews = view.findViewById(R.id.cv_news);
            domainImage = view.findViewById(R.id.iv_news_img);
            swipeNews = view.findViewById(R.id.swipe_news);
            flNews = view.findViewById(R.id.fl_news);
            rtBookmark = view.findViewById(R.id.rt_bookmark_icon);
            rtShareIcon = view.findViewById(R.id.rt_share_icon);
            rtReportIcon = view.findViewById(R.id.rt_flag_icon);
            ivBookmark = view.findViewById(R.id.iv_bookmark);
            mTitle = view.findViewById(R.id.tv_news_topic);
            mTrend = view.findViewById(R.id.tv_getty);
            mDomain = view.findViewById(R.id.tv_news_domain);
            mDate = view.findViewById(R.id.tv_date);
            mTime = view.findViewById(R.id.tv_time);
            mRating = view.findViewById(R.id.rb);
            headerView = view.findViewById(R.id.vw_news);
            ivShare = view.findViewById(R.id.iv_share);
            ivReport = view.findViewById(R.id.iv_flag);
        }
    }

    private void reportAPI(final String id, String report, final MyViewHolder holder, final String flag, final DBQueries dbQueries) {
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(holder.ivReport.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        String userEncode = /*sharedPreferences.getString(Constants.USERID_ENCODE, "").trim()*/dbQueries.readLoginID(1).getUser_id_encode();

        NewsData newsData = new NewsData();
        newsData.setReport(report);
        hipstoAPI.addReport(authToken, userEncode.trim(), id.trim(), newsData).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    Boolean status = jsonObject.getBoolean("success");
                    if (status) {
                        if (flag.equals("add")) {
                            holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_red));
                            Toast.makeText(context, context.getResources().getString(R.string.update_success), Toast.LENGTH_LONG).show();
                            //reportedNewsList.add(id);
                            dbQueries.insertReportId(id);
                        } else {
                            holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_grey));
                            Toast.makeText(context, context.getResources().getString(R.string.update_success), Toast.LENGTH_LONG).show();
                            //reportedNewsList.remove(id);
                            dbQueries.deleteReportId(id);
                        }
                    } else {
                        holder.ivReport.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_flag_grey));
                        Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (context != null) {
                        Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                if (context != null) {
                    Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                }
            }

        });
    }



}