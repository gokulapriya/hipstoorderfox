package hipsto.app.orderfox.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.models.New;

/**
 * Created by Gokulapriya on 23/5/2019.
 */

public class RecentNewsListAdapter extends RecyclerView.Adapter<RecentNewsListAdapter.MyViewHolder> {

    private ArrayList<New> newsArrayList;
    private Context context;

    public RecentNewsListAdapter(Context context, ArrayList<New> newArrayList) {
        this.context =context;
        this.newsArrayList = newArrayList;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_image_slider, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.ivNewsImage.setImageResource(newsArrayList.get(position).getImage());

        if(newsArrayList.get(position).getBookmark() == 1){
            holder.ivNewsBookmark.setVisibility(View.VISIBLE);
        }else{
            holder.ivNewsBookmark.setVisibility(View.INVISIBLE);
        }
        holder.tvNewsDate.setText(newsArrayList.get(position).getDate());
        holder.tvNewsTitle.setText(newsArrayList.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return newsArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivNewsImage,ivNewsBookmark;
        TextView tvNewsDate,tvNewsTitle;
        public MyViewHolder(View view) {
            super(view);

            ivNewsImage = view.findViewById(R.id.iv_news_image);
            ivNewsBookmark = view.findViewById(R.id.iv_news_bookmark);
            tvNewsDate = view.findViewById(R.id.tv_news_date);
            tvNewsTitle = view.findViewById(R.id.tv_news_title);
        }
    }

}