package hipsto.app.orderfox.utils;

import android.app.Activity;
import android.util.Log;

import com.appodeal.ads.InterstitialCallbacks;

/**
 * Created by Gokulapriya on 8/28/2019.
 */

public class AppodealInterstitialCallbacks implements InterstitialCallbacks {
    private final Activity mActivity;

    public AppodealInterstitialCallbacks(Activity activity) {
        mActivity = activity;
    }


    @Override
    public void onInterstitialLoaded(boolean isPrecache) {
        Log.d("Appodeal success", String.format("onInterstitialLoaded, isPrecache: %s", isPrecache));
    }

    @Override
    public void onInterstitialFailedToLoad() {
        Log.d("Appodeal success", "onInterstitialFailedToLoad");
    }

    @Override
    public void onInterstitialShown() {

        Log.d("Appodeal success", "onInterstitialShown");
    }

    @Override
    public void onInterstitialClicked() {

        Log.d("Appodeal", "onInterstitialClicked");

    }

    @Override
    public void onInterstitialClosed() {
        Log.d("Appodeal", "onInterstitialClosed");
    }

    @Override
    public void onInterstitialExpired() {
        Log.d("Appodeal", "onInterstitialExpired");
    }

}
