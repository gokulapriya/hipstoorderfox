package hipsto.app.orderfox.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.app.HipstoApplication;

/**
 * Created by harini.m on 7/4/2019.
 */

public class Utilsdefault {
    public static boolean isNetworkAvailable() {
        ConnectivityManager mConnectivityManager =
                (ConnectivityManager) HipstoApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public static void alertNetwork(final Activity context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialog); //Home is name of the activity
        builder.setMessage("No Internet Connection, Please check");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        final AlertDialog alert = builder.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));

            }
        });
        alert.show();
        alert.setCanceledOnTouchOutside(false);


    }

    public static void hideKeyboard(final View view) {
        if (view != null) {
            view.post(new Runnable() {
                @Override
                public void run() {
                    Context context = view.getContext();
                    InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            });
        }
    }
}
