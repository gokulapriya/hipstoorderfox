package hipsto.app.orderfox.utils;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.appodeal.ads.BannerCallbacks;

/**
 * Created by Gokulapriya on 7/9/2019.
 */

public class AppodealBannerCallbacks implements BannerCallbacks {

    private final Activity mActivity;

    public AppodealBannerCallbacks(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void onBannerLoaded(int height, boolean isPrecache) {
        Log.d("Appodeal success", String.format("onBannerLoaded success, %sdp, isPrecache: %s", height, isPrecache));
    }

    @Override
    public void onBannerFailedToLoad() {
        Log.d("Appodeal",  "onBannerFailedToLoad");
        //Toast.makeText(mActivity,  "onBannerFailedToLoad", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBannerShown() {
        Log.d("Appodeal",  "onBannerShown");
    }

    @Override
    public void onBannerClicked() {
        Log.d("Appodeal",  "onBannerClicked");
    }

    @Override
    public void onBannerExpired() {
        Log.d("Appodeal",  "onBannerExpired");
       // Toast.makeText(mActivity,  "onBannerExpired", Toast.LENGTH_SHORT).show();
    }

}
