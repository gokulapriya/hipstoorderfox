package hipsto.app.orderfox.utils;

import android.app.Activity;
import android.util.Log;

import com.appodeal.ads.NativeAd;
import com.appodeal.ads.NativeCallbacks;

/**
 * Created by Gokulapriya on 8/13/2019.
 */

public class AppodealNativeCallbacks implements NativeCallbacks {

    private final Activity mActivity;

    public AppodealNativeCallbacks(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void onNativeLoaded() {
        Log.d("Appodeal",  "onNativeLoaded");
    }

    @Override
    public void onNativeFailedToLoad() {
        Log.d("Appodeal",  "onNativeFailedToLoad");
    }

    @Override
    public void onNativeShown(NativeAd nativeAd) {
        Log.d("Appodeal",  "onNativeShown");
    }

    @Override
    public void onNativeClicked(NativeAd nativeAd) {
        Log.d("Appodeal",  "onNativeClicked");
    }

    @Override
    public void onNativeExpired() {
        Log.d("Appodeal",  "onNativeExpired");
    }

}
