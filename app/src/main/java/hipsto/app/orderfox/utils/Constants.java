package hipsto.app.orderfox.utils;

/**
 * Created by harini.m on 5/23/2019.
 */

public class Constants {
    public static String FCMTOKEN = "fcm_token";
    public static String SECURE_TOKEN = "SECURE_TOKEN";
    public static String VITAPPID = "app-5b4f54ff9d9390.45596440";
    public static String VITAPIKEY = "client-5b4f54ff9d9416.15157620";
    public static String HIPSTO_BASE_URL = "http://backend.hipsto.global/api/";


    //preference value
    public static String PREFTOKEN = "pref_token";
    public static String PREFEXPRY = "pref_expry";
    public static String PREFISLOGIN = "islogin";
    public static String TOPICID = "topicid";
    public static String COOKIES = "cookies";
    public static String USERID = "user";
    public static String USERID_ENCODE = "user_encode";
    public static String SORTBY = "sort_by";
    public static String ALLLANGUAGE = "all_language";
    public static String LOADFRAGMENT = "load_fragment";

}
