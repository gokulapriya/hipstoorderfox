package hipsto.app.orderfox.activity;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;

import hipsto.app.orderfox.R;

/**
 * Created by harini.m on 5/23/2019.
 */

public class BaseActivity extends AppCompatActivity {
    protected Handler mHandler = new Handler();
    protected ProgressDialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int messageId) {
        showProgress(getString(messageId));
    }

    public void showProgress(final CharSequence message) {

        if (getApplicationContext() != null) {
            mHandler.post(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                  //  UtilsDefault.hideKeyboard(BaseActivity.this);
                    //mDialog.setMessage(message);
                    //mDialog.setIndeterminate(true);
                    mDialog = new ProgressDialog(BaseActivity.this);
                    mDialog.getWindow().setBackgroundDrawable(new
                            ColorDrawable(android.graphics.Color.TRANSPARENT));
                    mDialog.setIndeterminate(true);
                    mDialog.setCancelable(false);
                    mDialog.show();
                    mDialog.setContentView(R.layout.custom_progress_view);

                }
            });
        }
    }

    public void hideProgress() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDialog.dismiss();
            }
        });
    }

}
