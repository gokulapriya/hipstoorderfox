package hipsto.app.orderfox.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.firebase.analytics.FirebaseAnalytics;

import javax.inject.Inject;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.utils.Utilsdefault;

/**
 * Created by Gokulapriya on 7/23/2019.
 */

public class WebActivity extends BaseActivity {

    String uri = "", newsId = "", resource = "", title = "";
    WebView mywebview;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_web_view);

        HipstoApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uri = bundle.getString("uri");
        }
        System.out.println("Hiii Url" + uri);
        mywebview = (WebView) findViewById(R.id.webView);

        if (Utilsdefault.isNetworkAvailable()) {

          /*  WebSettings webSettings = mywebview.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setLoadsImagesAutomatically(true);
            webSettings.setDomStorageEnabled(true);
            if (!uri.equals("")) {
                mywebview.loadUrl(uri);
            }
            mywebview.setWebViewClient(new WebviewPromotions());
            mywebview.setWebChromeClient(new WebChromeClient());*/

           CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.setToolbarColor(ContextCompat.getColor(this, R.color.colorWhite));
            //builder.setStartAnimations(this, R.anim.slide_in_right, R.anim.slide_out_left);
            builder.setExitAnimations(this, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.intent.setData(Uri.parse(uri));
            try {
                startActivityForResult(customTabsIntent.intent, 100);
            }catch(Exception e){
                finish();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(uri));
                startActivity(i);
            }
            //customTabsIntent.launchUrl(this, Uri.parse(uri));

        } else {
            Utilsdefault.alertNetwork(this);
        }

        /*Firebase Event log*/
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bun = new Bundle();
        bun.putString(FirebaseAnalytics.Param.ITEM_NAME, "ORDERFOX");
        bun.putString(FirebaseAnalytics.Param.CONTENT, title);
        bun.putString(FirebaseAnalytics.Param.ITEM_ID, newsId);
        bun.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, resource);
        bun.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Resource");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bun);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        editor.putString("InterstitialAdsShown","false").apply();
        finish();
    }

    @Override
    public void onBackPressed() {
        editor.putString("InterstitialAdsShown","false").apply();
        super.onBackPressed();
    }


    public class WebviewPromotions extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }
    }
}
