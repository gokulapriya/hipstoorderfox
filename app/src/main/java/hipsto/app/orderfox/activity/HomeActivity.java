package hipsto.app.orderfox.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import javax.inject.Inject;

import hipsto.app.orderfox.R;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.fragment.BookmarkFragment;
import hipsto.app.orderfox.fragment.ExploreFragment;
import hipsto.app.orderfox.fragment.NewsDetailFragment;
import hipsto.app.orderfox.fragment.NewsFragment;
import hipsto.app.orderfox.fragment.SettingFragment;
import hipsto.app.orderfox.utils.Constants;

/**
 * Created by Gokulapriya on 5/23/2019.
 */

public class HomeActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static RelativeLayout rtSetting, rt_bookmark, rtSearch, rt_navigationView;
    NewsFragment fragment;
    public static ImageView ivLogo;
    String deeplinkPrefix = "no", deeplinkid = "0";
    public static BottomNavigationView navigation;
    public static android.support.v7.widget.Toolbar toolbar;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        HipstoApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        //getting bottom navigation view and attaching the listener
        toolbar = findViewById(R.id.toolbar);
        rt_navigationView = findViewById(R.id.rt_navigationView);
        navigation = findViewById(R.id.navigationView);
        rtSetting = findViewById(R.id.rt_setting);
        rtSearch = findViewById(R.id.rt_search);
        rt_bookmark = findViewById(R.id.rt_bookmark);
        ivLogo = findViewById(R.id.iv_logo);

        rtSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                fragment = new SettingFragment();
                loadFragment(fragment);

            }
        });


        rt_bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                fragment = new BookmarkFragment();
                loadFragment(fragment);

            }
        });
        navigation.setOnNavigationItemSelectedListener(this);

        Intent mainIntent = getIntent();
        String action = mainIntent.getAction();
        String data = mainIntent.getDataString(); // Get the URL

        if (Intent.ACTION_VIEW.equals(action) && data != null) {
            // Code here to parse URL and display content
            System.out.println("action data" + data);
        }

        FirebaseDynamicLinks.getInstance().getDynamicLink(mainIntent).addOnSuccessListener(new OnSuccessListener<PendingDynamicLinkData>() {
            @Override
            public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                try {
                    Log.e("dynamic link", "success" + pendingDynamicLinkData.getLink());

                    String deeplinkid = pendingDynamicLinkData.getLink().getQueryParameter("news");
                    if (deeplinkid != null) {
                        Fragment newsDetailFragment = new NewsDetailFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("newsId", deeplinkid);
                        bundle.putString("pageStatus", "share");
                        newsDetailFragment.setArguments(bundle);
                        loadFragment(newsDetailFragment);
                    } else {
                        loadFragment(new NewsFragment());
                    }
                    Log.e("dynamic timestamp", "success" + pendingDynamicLinkData.getClickTimestamp());
                    Log.e("dynamic min version", "success" + pendingDynamicLinkData.getMinimumAppVersion());
                    Log.e("dynamic updatecontent", "success" + pendingDynamicLinkData.getUpdateAppIntent(HomeActivity.this));
                } catch (Exception e) {
                    e.printStackTrace();
                    loadFragment(new NewsFragment());
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("dynamic data", "exception: " + e);
                loadFragment(new NewsFragment());
            }
        });

      /*  Log.e("Prefix", "App Index deeplinking" + deeplinkPrefix + action + data1);// This will give you prefix as path

        if (mainIntent != null && mainIntent.getData() != null
                && (mainIntent.getData().getScheme().equals("http"))) {
            Uri data = mainIntent.getData();

            List<String> pathSegments = data.getPathSegments();

            if (pathSegments.size() > 0) {
                deeplinkPrefix = pathSegments.get(1);
                deeplinkid = pathSegments.get(2);
            }
            Log.e("Prefix", "deeplinking" + deeplinkPrefix + pathSegments);// This will give you prefix as path
        }*/
        /*if (!deeplinkPrefix.equals("no")) {
            if (deeplinkPrefix.equals("news")) {
                Fragment newsDetailFragment = new NewsDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("newsId", deeplinkid);
                bundle.putString("pageStatus", "share");
                newsDetailFragment.setArguments(bundle);
                loadFragment(newsDetailFragment);
            }
        } else {
            loadFragment(new NewsFragment());
        }*/

    }

    public boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            Log.d("Tag", "Fragment Commit");
            HomeActivity.this.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .addToBackStack("tag")
                    .commitAllowingStateLoss();
            return true;
        }
        return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("Tag", "onSaveInstanceState Called");
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;

        switch (menuItem.getItemId()) {
            case R.id.action_news:
                if(sharedPreferences.getInt(Constants.LOADFRAGMENT,0) != 1){
                    fragment = new NewsFragment();
                }
                break;

            case R.id.action_explore:
                fragment = new ExploreFragment();
                break;

        }

        return loadFragment(fragment);
    }

    private boolean doubleBackToExitPressedOnce = true;

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {

            if (doubleBackToExitPressedOnce) {
                this.doubleBackToExitPressedOnce = false;
                Toast.makeText(this, "Please click BACK again to exit.", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = true;
                    }
                }, 2000);
            } else {
                finishAffinity();
            }
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("requestcode", "onActivityResult: requestcode" + requestCode);
        Log.e("resultcode", "onActivityResult: " + resultCode);

        Log.e("data", "onActivityResult: " + data);
        System.out.println("Hiii Activity" + requestCode);
        //  onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        if (AppInviteReferral.hasReferral(intent)) {
            processReferralIntent(intent);
        }
    }

    private void processReferralIntent(Intent intent) {
        String invitationId = AppInviteReferral.getInvitationId(intent);
        String deepLink = AppInviteReferral.getDeepLink(intent);
        Log.e("referal link", "" + invitationId + ", " + deepLink);
    }


}
