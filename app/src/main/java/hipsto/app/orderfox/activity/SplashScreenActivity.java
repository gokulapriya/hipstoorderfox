package hipsto.app.orderfox.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.UserSettings;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import hipsto.app.orderfox.R;
import hipsto.app.orderfox.Sql.DBQueries;
import hipsto.app.orderfox.app.HipstoApplication;
import hipsto.app.orderfox.models.LoginData;
import hipsto.app.orderfox.models.NotifyData;
import hipsto.app.orderfox.models.TopicLangResponse;
import hipsto.app.orderfox.retrofit.HipstoAPI;
import hipsto.app.orderfox.utils.AppodealBannerCallbacks;
import hipsto.app.orderfox.utils.AppodealInterstitialCallbacks;
import hipsto.app.orderfox.utils.AppodealNativeCallbacks;
import hipsto.app.orderfox.utils.Constants;
import hipsto.app.orderfox.utils.ForceUpdateChecker;
import hipsto.app.orderfox.utils.Utilsdefault;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by harini.m on 5/23/2019.
 */

public class SplashScreenActivity extends BaseActivity implements ForceUpdateChecker.OnUpdateNeededListener {
    @BindView(R.id.get_started)
    Button getStarted;

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public HipstoAPI hipstoAPI;
    private FirebaseAuth mAuth;
    AppEventsLogger logger;
    List<TopicLangResponse> LanguageList = new ArrayList<>();
    List<String> allLanguageList = new ArrayList<>();
    DBQueries dbQueries;
    public static String sortBy;
    public ProgressDialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        boolean check = ForceUpdateChecker.with(this).onUpdateNeeded(this).check();
        if (check) {
            System.out.println("update available");
        } else {
            System.out.println("not update available");

            logger = AppEventsLogger.newLogger(this);
            getStarted = findViewById(R.id.get_started);
            HipstoApplication.getContext().getComponent().inject(this);
            editor = sharedPreferences.edit();


            Appodeal.setTesting(false);
            Appodeal.setLogLevel(com.appodeal.ads.utils.Log.LogLevel.debug);

            Appodeal.getUserSettings(this)
                    .setAge(25)
                    .setGender(UserSettings.Gender.OTHER);
            Appodeal.setAutoCache(Appodeal.NATIVE | Appodeal.BANNER | Appodeal.INTERSTITIAL, true);
            Appodeal.initialize(this, "81ceae313349d1f79511e88967a4c620324061270551a836", Appodeal.NATIVE | Appodeal.BANNER | Appodeal.INTERSTITIAL, true);

            Appodeal.setNativeCallbacks(new AppodealNativeCallbacks(this));
            if (Appodeal.isLoaded(Appodeal.NATIVE)) {
                System.out.println("Native isLoaded --> True");
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            Appodeal.setBannerCallbacks(new AppodealBannerCallbacks(this));

            if (Appodeal.isLoaded(Appodeal.BANNER)) {
                System.out.println("Banner isLoaded --> True");
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            Appodeal.setInterstitialCallbacks(new AppodealInterstitialCallbacks(this));
            System.out.println("New Ads interstitial 1" + sharedPreferences.getString("InterstitialAdsShown", ""));
            Appodeal.setTriggerOnLoadedOnPrecache(Appodeal.INTERSTITIAL, false);
            if (Appodeal.isLoaded(Appodeal.INTERSTITIAL)) {
                System.out.println("INTERSTITIAL isLoaded --> True");
            } else {
                System.out.println("INTERSTITIAL isLoaded --> False");
            }
            if (Appodeal.isPrecache(Appodeal.INTERSTITIAL)) {
                System.out.println("INTERSTITIAL isPrecache --> True");
            } else {
                System.out.println("INTERSTITIAL isPrecache --> False");
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////

            /*Local Db*/

            dbQueries = new DBQueries(this);
            dbQueries.open(this);


            /*Progress Dialog*/

            mDialog = new ProgressDialog(this);
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mDialog.setIndeterminate(true);
            mDialog.setCancelable(false);

            // Initialize Firebase Auth
            mAuth = FirebaseAuth.getInstance();

            if (dbQueries.readLogin().size() == 0 || dbQueries.readLoginID(1).getIslogin() == 0) {
                /*New Session*/
                getStarted.setVisibility(View.VISIBLE);
                //Clear Local DB
                dbQueries.deleteAllData();
                //Click Function
                getStarted.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Utilsdefault.isNetworkAvailable()) {

                            mAuth.signInAnonymously()
                                    .addOnCompleteListener(SplashScreenActivity.this, new OnCompleteListener<AuthResult>() {
                                        public static final String TAG = "firebase auth";

                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                // Sign in success, update UI with the signed-in user's information
                                                FirebaseUser user = mAuth.getCurrentUser();
                                                String uid = user.getUid();
                                                Log.d(TAG, "signInAnonymously:success = " + user.getUid());
                                                byte[] encodeValue = Base64.encode(uid.getBytes(), Base64.DEFAULT);
                                                dbQueries.insertLogin(new LoginData(1, "", "", 0, "", uid, new String(encodeValue)));
                                                dbQueries.insertNotify(new NotifyData(1,Settings.Secure.getString(getContentResolver(),
                                                        Settings.Secure.ANDROID_ID),FirebaseInstanceId.getInstance().getToken(),""));
                                                //dbQueries.insertNotificationOnOff(1, "true");
                                                Gson gson = new Gson();
                                                //System.out.println("Insert Login --->"+gson.toJson(dbQueries.readNotifyID(1))+" & " + dbQueries.readNotify().size());
                                                getAuth();

                                            } else {
                                                // If sign in fails, display a message to the user.
                                                Log.w(TAG, "signInAnonymously:failure", task.getException());
                                                dbQueries.insertLogin(new LoginData(1, "", "", 0, "", "", ""));
                                                dbQueries.insertNotify(new NotifyData(1,Settings.Secure.getString(getContentResolver(),
                                                        Settings.Secure.ANDROID_ID),FirebaseInstanceId.getInstance().getToken(),""));
                                                //dbQueries.insertNotificationOnOff(1, "true");
                                                Gson gson = new Gson();
                                                //System.out.println("Insert Login --->"+gson.toJson(dbQueries.readNotifyID(1))+" & " + dbQueries.readNotify().size());
                                                Toast.makeText(SplashScreenActivity.this, "Authentication failed.",
                                                        Toast.LENGTH_SHORT).show();
                                                getAuth();
                                            }

                                        }
                                    });
                            /*Default News Stream*/
                            if (!(dbQueries.readSettingStream().size() > 0)) {
                                dbQueries.insertSettingStream("media");
                                dbQueries.insertSettingStream("twitter");
                            }

                        } else {
                            Utilsdefault.alertNetwork(SplashScreenActivity.this);
                        }
                    }
                });
            } else if (dbQueries.readLoginID(1).getIslogin() == 1) {
                editor.putString(Constants.SORTBY, "").apply();
                getStarted.setVisibility(View.GONE);
                if (Utilsdefault.isNetworkAvailable()) {
                    getdirectAuth();
                } else {
                    Utilsdefault.alertNetwork(SplashScreenActivity.this);
                }
            }

            PackageInfo info;
            try {
                info = getPackageManager().getPackageInfo("hipsto.app.orderfox", PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md;
                    md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    String something = new String(Base64.encode(md.digest(), 0));
                    //String something = new String(Base64.encodeBytes(md.digest()));
                    System.out.println("hash key" + something);
                }
            } catch (PackageManager.NameNotFoundException e1) {
                Log.e("name not found", e1.toString());
            } catch (NoSuchAlgorithmException e) {
                Log.e("no such an algorithm", e.toString());
            } catch (Exception e) {
                Log.e("exception", e.toString());
            }
        }
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logSentFriendRequestEvent() {
        logger.logEvent("sentFriendRequest");
    }

    //Progress Dialog
    public void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    void getAuth() {
        showProgressDialog();
        hipstoAPI.userAuth(Constants.VITAPPID, Constants.VITAPIKEY).
                enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        try {
                            mDialog.dismiss();
                            JSONObject object = new JSONObject(String.valueOf(response.body()));
                            if (object.getString("success").equals("true")) {
                                JSONObject jsonObject = object.getJSONObject("data");
                                System.out.println("Hii Auth ==> " + dbQueries.readLoginID(1).getUser_id_encode());
                                dbQueries.updateLogin(1, jsonObject.getString("token"), jsonObject.getString("expiration"), 1);

                              /*  editor.putString(Constants.PREFTOKEN, jsonObject.getString("token")).commit();
                                editor.putString(Constants.PREFEXPRY, jsonObject.getString("expiration")).commit();
                                editor.putBoolean(Constants.PREFISLOGIN, true).commit();*/

                                if (Utilsdefault.isNetworkAvailable()) {
                                    run();

                                } else {
                                    Utilsdefault.alertNetwork(SplashScreenActivity.this);
                                }

                            } else {
                                if (dbQueries.readLogin().size() == 0) {
                                    dbQueries.insertLogin(new LoginData(1, "", "", 0, "", "", ""));
                                } else {
                                    dbQueries.updateLogin(1, "", "", 0);
                                }
                                //editor.putBoolean(Constants.PREFISLOGIN, false).commit();
                                Toast.makeText(SplashScreenActivity.this, getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                          mDialog.dismiss();
                            e.printStackTrace();
                            Toast.makeText(SplashScreenActivity.this, getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                      mDialog.dismiss();
                        Toast.makeText(SplashScreenActivity.this, getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                });

    }

    void getdirectAuth() {
        showProgressDialog();
        hipstoAPI.userAuth(Constants.VITAPPID, Constants.VITAPIKEY).
                enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        try {
                            mDialog.dismiss();
                            JSONObject object = new JSONObject(String.valueOf(response.body()));
                            if (object.getString("success").equals("true")) {
                                JSONObject jsonObject = object.getJSONObject("data");
                                System.out.println("Hii direct ==> " + dbQueries.readLoginID(1).getUser_id_encode());
                                dbQueries.updateLogin(1, jsonObject.getString("token"), jsonObject.getString("expiration"), 1);
                           /*  editor.putString(Constants.PREFTOKEN, jsonObject.getString("token")).commit();
                                editor.putString(Constants.PREFEXPRY, jsonObject.getString("expiration")).commit();
                                editor.putBoolean(Constants.PREFISLOGIN, true).commit();*/

                                if (Utilsdefault.isNetworkAvailable()) {
                                    run();
                                } else {
                                    Utilsdefault.alertNetwork(SplashScreenActivity.this);
                                }

                            } else {
                                if (dbQueries.readLogin().size() == 0) {
                                    dbQueries.insertLogin(new LoginData(1, "", "", 0, "", "", ""));
                                } else {
                                    dbQueries.updateLogin(1, "", "", 0);
                                }
                                //editor.putBoolean(Constants.PREFISLOGIN, false).commit();
                                Toast.makeText(SplashScreenActivity.this, getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            mDialog.dismiss();
                            e.printStackTrace();
                            Toast.makeText(SplashScreenActivity.this, getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        mDialog.dismiss();
                        Toast.makeText(SplashScreenActivity.this, getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
                    }
                });

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        Log.e("", ">" + currentUser);
    }

    private void run() throws IOException {
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .header("Authorization", authToken)
                .header("Accept", "application/json")
                .url(Constants.HIPSTO_BASE_URL + "application/topic")
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {

            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {

                final String myResponse = response.body().string();
                Log.e("res settings ", ">" + myResponse);
                try {
                    JSONObject json = new JSONObject(myResponse);
                    JSONObject jsonObject = json.getJSONObject("_id");
                    editor.putString(Constants.TOPICID, jsonObject.getString("$oid")).commit();
                    if (Utilsdefault.isNetworkAvailable()) {
                        runTrendLogin();
                    } else {
                        Utilsdefault.alertNetwork(SplashScreenActivity.this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        });
    }

    private void runTrendLogin() throws IOException {

        final OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://api.trendolizer.com/v3/login?username=Hipsto&password=testing")
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {

            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                Log.i("response", response.toString());
                Headers headerResponse = response.headers();
                //convert header to Map
                Map<String, List<String>> headerMapList = headerResponse.toMultimap();
                //Get List of "Set-Cookie" from Map
                List<String> allCookies = headerMapList.get("Set-Cookie");
                String cookieval = "";
                if (allCookies != null) {
                    for (int i = 0; i < allCookies.size(); i++) {
                        allCookies.get(i);
                        //concat all cookies in cookieval.
                        cookieval = cookieval + allCookies.get(i);
                        System.out.println(".........." + cookieval.split(";", 1));
                        // cookieval.split(";",1);
                        dbQueries.updateCookie(1, cookieval);
                        /*editor.putString(Constants.COOKIES, cookieval).commit();*/
                        if (Utilsdefault.isNetworkAvailable()) {
                            Language();
                        } else {
                            Utilsdefault.alertNetwork(SplashScreenActivity.this);
                        }
                    }
                }else{

                    dbQueries.updateCookie(1, cookieval);

                    if (Utilsdefault.isNetworkAvailable()) {
                        Language();
                    } else {
                        Utilsdefault.alertNetwork(SplashScreenActivity.this);
                    }
                }
            }
        });
    }

    private void Language() throws IOException {
        String authToken = /*sharedPreferences.getString(Constants.PREFTOKEN, "")*/dbQueries.readLoginID(1).getPref_token();
        String topicId = sharedPreferences.getString(Constants.TOPICID, "");

        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.HIPSTO_BASE_URL + "topic-settings/topic_id").newBuilder();

        String url = urlBuilder.build().toString();

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .header("Authorization", authToken)
                .header("Accept", "application/json")
                .url(url)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {

            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                LanguageList.clear();
                final String myResponse = response.body().string();
                Log.e("res", ">" + myResponse);
                try {
                    JSONArray json = new JSONArray(myResponse);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject jsonObject = json.getJSONObject(i);
                        TopicLangResponse topicLangResponse = new TopicLangResponse(
                                jsonObject.getString("code"),
                                jsonObject.getString("name"),
                                jsonObject.getString("native"));
                        LanguageList.add(topicLangResponse);
                        allLanguageList.add(topicLangResponse.getName());
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            /*Default News Stream*/
                            if (!(dbQueries.readSettingLang().size() > 0)) {
                                if (LanguageList.size() > 0) {
                                    if(LanguageList.get(0).getCode().equals("") || LanguageList.get(0).getName().equals("") || LanguageList.get(0).getNative().equals("")|| LanguageList.get(0).getCode()!= null || LanguageList.get(0).getName()!= null || LanguageList.get(0).getNative()!= null ){
                                        TopicLangResponse sample = new TopicLangResponse("en","English","English");
                                        dbQueries.insertSettingLang(sample);
                                    }else{
                                        dbQueries.insertSettingLang(LanguageList.get(0));
                                    }

                                }
                            }
                            editor.putString(Constants.ALLLANGUAGE, allLanguageList.toString()).apply();

                            startActivity(new Intent(SplashScreenActivity.this, HomeActivity.class)

                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finish();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        });
    }

    @Override
    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
